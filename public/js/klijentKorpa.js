const DOSTAVA = 250;
$korpaInclude = $('#korpaInclude');

function number_format_javascript(n, decimalFields ,decimalSeparator, thousandSeparator) {
    var parts=n.toString().split(".");
    var value =  parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousandSeparator);
    if(decimalFields > 0){
        value += (parts[1] ? decimalSeparator + parts[1].substr(0,decimalFields) : decimalSeparator + '00');
    }
    return value;
}

function postaviUkupnuCenuPorudzbine(total){
    $('#total-cena').html(number_format_javascript(total, 0, ',', '.') + " din");
    $('#total-cena-dostava').html(number_format_javascript(parseFloat(total) + DOSTAVA, 0, ',', '.') + " din");
}

function dodajUKorpu(idProizvod, kolicina){
    $.post( "/dodaj_u_korpu?id_proizvod=" + idProizvod + "&kolicina="  + kolicina  , function( data ) {
        otvoriDialog();

        if(document.getElementById('korpaInclude') !== undefined) {
            $korpaInclude.html(data.html);
        }
    }).fail(function(jqXHR, ajaxOptions, thrownError)
    {
        alert('server nije dostupan...');
    });
}

// da bi se azurirala korpa u Controlleru potreban je rowId
function azurirajKolicinuUKorpi(rowIdKorpa, idProizvod, kolicina) {
    if (parseInt(kolicina) > 0){
        $('#qttotal-stavka-' + rowIdKorpa).html(kolicina);
        $.post("/azuriraj_kolicinu_u_korpi?rowId=" + rowIdKorpa +"&id_proizvod=" + idProizvod + "&kolicina=" + kolicina, function( data ) {
            //ISPRAVI SUBTOTAL I TAX NE RADI
            $('#stavka-total-' + rowIdKorpa).html(number_format_javascript(data.stavkaTotal, 0,  ',', '.')+ " din");
            postaviUkupnuCenuPorudzbine(data.total);
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
            alert('server nije dostupan...');
        });
    }

}

//zove se iz dijaloga kao linkUspesno
function obrisiIzKorpe(rowIdKorpa, idProizvod){
    $.post("/obrisi_iz_korpe?rowId=" + rowIdKorpa +"&id_proizvod=" + idProizvod, function( data ){
        let brojElemenata = parseInt(data.brojElemenata);

        if(document.getElementById('korpaPrikaz') !== undefined) {
            $('#stavka-proizvod-' + rowIdKorpa).remove();
            if(brojElemenata == 0){
                $('#korpaPrikaz').html('<h3>Korpa je prazna.</h3>');
            } else{
                postaviUkupnuCenuPorudzbine(data.total);
            }
        }
        if(document.getElementById('korpaInclude') !== undefined){
            $korpaInclude.html(data.html);
        }

        postaviUkupnuCenuPorudzbine(data.total);

        zatvoriDialogSaId(rowIdKorpa);
    }).fail(function (jqXHR, ajaxOptions, thrownError) {
        alert('server nije dostupan...');
    });
}

function ukloniVaucer(rowIdVaucer, idVaucer){
    $.post("/ukloni_vaucer_iz_korpe?rowId=" + rowIdVaucer +"&id_vaucer=" + idVaucer, function( data ) {
        $('#stavka-vaucer-'+rowIdVaucer).remove();

        $('#tabela-vauceri').remove();
        $('#tabele-div').append(data.html);

        postaviUkupnuCenuPorudzbine(data.total);

        zatvoriDialogSaId('vaucer-' + rowIdVaucer);
    }).fail(function (jqXHR, ajaxOptions, thrownError) {
        alert('server nije dostupan...');
    });
}

function primeniKupon(){
    let kod = document.getElementById('kod').value;

    if(kod !== ''){
        $.post("/primeni_kupon_vaucer?kod=" + kod, function( data ) {
            let color = 'color:red;';

            if(data.primenjen) {
                color = 'color:limegreen';

                if(data.kupon_ili_vaucer == 'kupon') {
                    $.each(data.stavke, function (index, element) {
                        if (data.primenjenKupon[element.rowId] !== undefined) {
                            let kupon = data.primenjenKupon[element.rowId];
                            $('#stavka-total-' + element.rowId).html(number_format_javascript(element.subtotal + element.tax*element.qty, 0, ',', '.') + " din");
                            $('#kupon-stavka-' + element.rowId).html(kupon.kod + ' (' + kupon.popust * 100 + '%)');
                        }
                    });
                } else if(data.kupon_ili_vaucer == 'vaucer'){
                    $('#tabela-vauceri').remove();
                    $('#tabele-div').append(data.html);
                }

                postaviUkupnuCenuPorudzbine(data.total);
            }

            document.getElementById('kupon-text').style = color;
            $('#kupon-text').html(data.poruka);
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
            alert('server nije dostupan...');
        });
    } else{
        $('#kupon-text').html('Kod ne sme biti prazan.');
    }
}
