function prikaziAktivne(){
    $('#blokirani-korisnici-tabela').css('display', 'none');
    $('#aktivni-korisnici-tabela').css('display', 'block');
    $('#blokirani-korisnici-tabela_wrapper').css('display', 'none');
    $('#aktivni-korisnici-tabela_wrapper').css('display', 'block');

    $('#kupci-title').html('Aktivni korisnici');
}

function prikaziBlokirane(){
    $('#blokirani-korisnici-tabela').css('display', 'block');
    $('#aktivni-korisnici-tabela').css('display', 'none');
    $('#blokirani-korisnici-tabela_wrapper').css('display', 'block');
    $('#aktivni-korisnici-tabela_wrapper').css('display', 'none');

    $('#proizvodi-avail-title').html('Blokirani korisnici');
}