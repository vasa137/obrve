function prikaziDostupne(){
    $('#tabela-brendovi-obrisani').css('display', 'none');
    $('#tabela-brendovi-aktivni').css('display', 'block');
    $('#tabela-brendovi-obrisani_wrapper').css('display', 'none');
    $('#tabela-brendovi-aktivni_wrapper').css('display', 'block');

    $('#brendovi-title').html('Dostupni brendovi');
}

function prikaziNedostupne(){
    $('#tabela-brendovi-obrisani').css('display', 'block');
    $('#tabela-brendovi-aktivni').css('display', 'none');
    $('#tabela-brendovi-obrisani_wrapper').css('display', 'block');
    $('#tabela-brendovi-aktivni_wrapper').css('display', 'none');

    $('#brendovi-title').html('Obrisani brendovi');
}