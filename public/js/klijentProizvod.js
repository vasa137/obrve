function inicijalizujSlike(){
    var slikkfor = $('.slider-for');
    if(slikkfor.length > 0){
        slikkfor.slick({
            slidesToShow: 1,
            speed: 500,
            arrows: false,
            fade: true,
            asNavFor: '.product-deails-thumb'
        });
    }
    var productDetailsThumb = $('.product-deails-thumb');

    if(productDetailsThumb.length > 0){

        productDetailsThumb.slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            speed: 500,
            asNavFor: '.slider-for',
            dots: false,
            focusOnSelect: true,
            slide: 'li',
            autoplay: true,
            arrows: false
        });

    }


    $('#product-details-slider').slickLightbox();
}

// product Details page item ++/--
$(document).on('click', '.qtplus', function () {
    var total = $('.qttotal').text();
    total++;
    $('.qttotal').text(total);
});
$(document).on('click', '.qtminus', function () {
    var total = $('.qttotal').text();
    total--;
    if (total > 0) {
        $('.qttotal').text(total);
    }
});