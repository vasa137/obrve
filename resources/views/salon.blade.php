@extends('layout')

@section('title')
 - Tretmani
@stop

@section('main')
<div class="about-header">
<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <div class="heading-table">
        <div class="heading-table-cell">
          <h2 class="heading-text">MOA TRETMANI</h2>   
           <div class="header-menu">
             <ul class="list-inline">
                <li ><a href="/">NASLOVNA /</a></li>
                <li><span class="active">MOA TRETMANI</span></li>
             </ul>
           </div>
          </div>
        </div>
      </div><!-- col-md-12 -->
    </div><!-- .row -->
  </div><!-- .container -->
</div><!-- .about-header -->



<!-- Section-Start --> 
<section class="bg-light-alt top-deals-area padding-large-bottom"><!-- top-deals -->
  <div class="container wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
    <div class="title text-center">
      <h2>MOA TRETMANI</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p>Posetite MOA salone u Beogradu ili Novom Sadu i uživajte u prijatnom ambijentu i profesionalnoj usluzi. </p>    </div><!-- tittle -->
    <div class="container top-deals"><!-- .top-deals -->
      

      <div class="col-md-12">
      <table  align="center" width="100%">
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Iscrtavanje obrva</p> <h3 style="float: right"   ></p>
          </td>
        </tr>

        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Senčenje obrva</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Veliko osveženje</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Malo osveženje</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Korekcija obrva</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Trajna šminka usana</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Trajna šminka eyeliner-a</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Korekcija trajne šminke usana i eyeliner-a</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Laserski tretman skidanja prvi</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Laserski tretman skidanja sledeći</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Laserski tretman Carbon black</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Microneedling lice</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Microneedling strije</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Microneedling kosa</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Plazma pen kapci - nehirurško podizanje</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Plazma pen nazolabijalne bore</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Plazma pen mimične bore</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Plazma pen lifting usana</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Šminkanje</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >BB Glow</p> <h3 style="float: right"   ></p>
          </td>
        </tr>
        <tr style=" border-bottom: 1px solid black;">
          <td width="100%">
            <h3 style="float: left"  >Ultrazvučni tretman lica</p> <h3 style="float: right"   ></p>
          </td>
        </tr>

      </table>
      </div>

    </div><!-- .row -->
  </div><!-- .container -->
</section>
<!--  Section-End -->



<!-- Section-Start --> <!-- working-hour-area-style2 -->
<section class="working-hour-area working-hour-area-style2 overlay dark-overlay padding-large-top-bottom">
  <div class="container">
    <div class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>MOA BEAUTY SALON</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p>Posetite MOA Beauty salone u Beogradu ili Novom Sadu i uživajte u prijatnom ambijentu i profesionalnoj usluzi. </p>
    </div><!-- tittle -->
    <div class="working-week text-center">
      <div class="working-day-outer">
        <div class="working-day wow zoomIn" data-wow-duration=".5s" data-wow-delay=".2s">
          <span class="day color-text">Pon</span>
          <span class="time">9<sup>00</sup> - 17<sup>00</sup></span>
        </div>
      </div>
      <div class="working-day-outer">
        <div class="working-day wow zoomIn" data-wow-duration=".5s" data-wow-delay=".4s">
          <span class="day color-text">Uto</span>
          <span class="time">9<sup>00</sup> - 17<sup>00</sup></span>
        </div>
      </div>
      <div class="working-day-outer">
        <div class="working-day wow zoomIn" data-wow-duration=".5s" data-wow-delay=".6s">
          <span class="day color-text">Sre</span>
          <span class="time">9<sup>00</sup> - 17<sup>00</sup></span>
        </div>
      </div>
      <div class="working-day-outer">
        <div class="working-day wow zoomIn" data-wow-duration=".5s" data-wow-delay=".8s">
          <span class="day color-text">Čet</span>
          <span class="time">9<sup>00</sup> - 17<sup>00</sup></span>
        </div>
      </div>
      <div class="working-day-outer">
        <div class="working-day wow zoomIn" data-wow-duration=".5s" data-wow-delay="1s">
          <span class="day color-text">Pet</span>
          <span class="time">9<sup>00</sup> - 17<sup>00</sup></span>
        </div>
      </div>
      <div class="working-day-outer">
        <div class="working-day wow zoomIn" data-wow-duration=".5s" data-wow-delay="1.2s">
          <span class="day color-text">Sub</span>
          <span class="time">9<sup>00</sup> - 17<sup>00</sup></span>
        </div>
      </div>
      <div class="working-day-outer">
        <div class="working-day wow zoomIn" data-wow-duration=".5s" data-wow-delay="1.4s">
          <span class="day color-text">Ned</span>
          <span class="time">9<sup>00</sup> - 17<sup>00</sup></span>
        </div>
      </div>
    </div><!-- .row -->
    <div class="row text-center">
      <div class="queries-call wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
        <h4 class="color-text"><a class="color-text" href="/kontakt">ZAKAŽITE VAŠ TERMIN</a></h4>
        
      </div>
    </div>
  </div><!-- .container -->
</section>
<!--  Section-End -->

  <div class="contact-us-area padding-large-top">
    <div class="container">
      <div class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
        <h2>Beograd</h2>
        <img style="height:40px;" src="/images/d1.png">
        <p>Kontaktirajte nas za sve potrebne informacije, zakazivanje tretmana i edukacija.</p>
      </div><!-- tittle -->
      <div class="row">
        <div class="contact-information">
          <div class="col-md-4 col-sm-4">
            <div class="contact-us-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
              <div class="contact-icon">
                <a href="#"><i class="pe-7s-map-marker"></i></a>
              </div>
              <h5>Adresa</h5>                      
               <a href="#">Bulevar Oslobodjenja 239</a>
               <a href="#">Beograd</a>
              
            </div>
          </div><!-- .col-md-4 -->
          <div class="col-md-4 col-sm-4">
            <div class="contact-us-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
              <div class="contact-icon">
                <a href="tel:063-888-63-63"><i class="pe-7s-call"></i></a>
              </div>
              <h5>Telefon</h5>                      
               <a href="tel:063-888-63-63">063-888-63-63</a>
              
            </div>
          </div><!-- .col-md-4 -->
          <div class="col-md-4 col-sm-4">
            <div class="contact-us-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
              <div class="contact-icon">
                <a href="mailto:info@moa-academy.com"><i class="pe-7s-mail"></i></a>
              </div>
              <h5>E-mail</h5>  
               <a href="mailto:info@moa-academy.com">info@moa-academy.com</a>
              
            </div>
          </div><!-- .col-md-4 -->
        </div><!-- .contact-information -->
      </div><!-- .row -->
    </div><!-- .container -->
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2832.552870309662!2d20.474571515405554!3d44.769532879098875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a71491d8da3cf%3A0x73dbba8198d4a6f8!2sMOA+Academy!5e0!3m2!1sen!2srs!4v1565386721103!5m2!1sen!2srs" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div><!-- .contact-us-area -->

  <div class="contact-us-area padding-large-top">
    <div class="container">
      <div class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
        <h2>NOVI SAD</h2>
        <img style="height:40px;" src="/images/d1.png">
        <p>Kontaktirajte nas za sve potrebne informacije, zakazivanje tretmana i edukacija.</p>
      </div><!-- tittle -->
      <div class="row">
        <div class="contact-information">
          <div class="col-md-4 col-sm-4">
            <div class="contact-us-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
              <div class="contact-icon">
                <a href="#"><i class="pe-7s-map-marker"></i></a>
              </div>
              <h5>Adresa</h5>                      
               <a href="#">Vardarska 1c</a>
               <a href="#">Novi Sad</a>
              
            </div>
          </div><!-- .col-md-4 -->
          <div class="col-md-4 col-sm-4">
            <div class="contact-us-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
              <div class="contact-icon">
                <a href="tel:063-888-63-63"><i class="pe-7s-call"></i></a>
              </div>
              <h5>Telefon</h5>                      
               <a href="tel:069-122-00-92">069-122-00-92</a>
              
            </div>
          </div><!-- .col-md-4 -->
          <div class="col-md-4 col-sm-4">
            <div class="contact-us-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
              <div class="contact-icon">
                <a href="mailto:info@moa-academy.com"><i class="pe-7s-mail"></i></a>
              </div>
              <h5>E-mail</h5>  
               <a href="mailto:info@moa-academy.com">info@moa-academy.com</a>
              
            </div>
          </div><!-- .col-md-4 -->
        </div><!-- .contact-information -->
      </div><!-- .row -->
    </div><!-- .container -->
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2809.299916248468!2d19.817002415386032!3d45.24172767909888!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475b10254488738b%3A0x2d17d5e5d1ff59e2!2sVardarska%201c%2C%20Novi%20Sad%2021000!5e0!3m2!1sen!2srs!4v1568580029010!5m2!1sen!2srs" width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    
  </div><!-- .contact-us-area -->
@stop