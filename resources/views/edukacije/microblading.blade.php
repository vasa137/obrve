@extends('layout')

@section('title')
 - Microblading Edukacija
@stop

@section('main')
<div class="about-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="heading-table">
          <div class="heading-table-cell">
            <h2 class="heading-text">MICROBLADING</h2>   
             <div class="header-menu">
               <ul class="list-inline">
                  <li ><a href="/">NASLOVNA /</a></li>
                  <li ><a href="/edukacije">EDUKACIJE /</a></li>
                  <li><span class="active">MICROBLADING</span></li>
               </ul>
             </div>
            </div>
          </div>
        </div><!-- col-md-12 -->
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .about-header -->

  <div class="service-main-content padding-small-bottom">
 <div class="container">
   <div class="row" style="padding-top: 30px;">
     <div class="title col-md-8 service-content wow fadeIn"  data-wow-duration="1s" data-wow-delay=".5s">
       <div class="service-single-image-holder">
         <img src="{{asset('images/edukacije/brows.jpg')}}" alt="image">
       </div>
       <h3>MOA MICROBLADING</h3>
       <div class="saparator1"></div>
       <p>Svim studentima obezbedjujemo detaljan uvid u svet estetike I svo potrebno znanje za dalji rad.
Obuka podrazumeva teoretski deo,rad na zivim  modelima, i podrsku mastera u narednom periodu,kao i dolazak na akademiju gde bi uz pomoc mastera mogli da usavrsavate tehniku microbladinga na zivim modelima i na taj nacin postanete spremni za dalji rad,a mi sigurni da cete pruzati vrhunsku uslugu buducim klijentima.


        </p>

     </div>
     <div class="col-md-4 service-sidebar wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
      <div class="widget">
        <a href="/kontakt">
        <img width="90%" src="{{asset('images/edukacije/zakazi-microblading.jpg')}}">
        </a>
       </div>
       <div class="widget" style="width: 90%;">
          <div class="heare-catagory">
             <ul>
                <li>
                <a href="/edukacije/microblading">Microblading<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
                <li>
                <a href="/edukacije/makeup">Make Up<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
                <li>
                <a href="/edukacije/pmu">Permanent Make Up<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
                <li>
                <a href="/edukacije/microblading">Microblading<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
             </ul>
          </div>
       </div>
       
     </div>
   </div>
 </div>
</div> 

<!-- Section-Start --> 
<section class="bg-light-alt padding-small-top">
  <div class="container">
    <div class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>MOA Bazna obuka obuhvata:</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p style="text-align: justify; text-justify: inter-word;">
       Obuhvata osnove microblading vestine, prenosenju teorije, prakticne primene tretmana i osnovnih pravila u radu. <br>
        Trajanje obuke je dva dana-prvog dana je teorijski deo i vezba na simulaciji koze, dok je drugog dana rad na zivom modelu. <br>
    Microblading je vestina, koja se stice samom vezbom. Tako da je najbitnije tokom prvog dana da se savlada kako da se pravilno vezba. Tokom cele obuke, ucimo kako se izvodi tetman, koliko traje, formiranje oblika u skladu sa proporcijama,formiranje simetrije, rad na kozi sa vec postojecom trajnom sminkom, upoznavanje sa problemima koze, bojama koje se koriste, vaznost samog posttretmanskog toka i slucajevi u kojima se microbladig tehnika ne izvodi.<br> 
<br>


Tokom drugog dana radi se na zivom modelu, uz nasu pomoc inadzor. Tu podrazumevamo pripremu klijenta, iscrtavanje oblika prema proporcijama lica, merenje simetrije, pravilno zatezanje koze, iscrtavanje dlacica, punjenje dlacica bojom. 
<br><br> 




      </p>
    </div><!-- tittle -->

  </div> 
</section>
<!--  Section-End -->


<!-- Section-Start --><!-- client-say-Area-Style2  -->
<section class="client-say-area-style2 overlay dark-overlay padding-small-top-bottom">
  <div style="z-index: 4; position:relative;" class="container">
     <div  class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>Saradnja nakon obuke</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p  > <!--style="text-align: justify; text-justify: inter-word;" -->
        Ukoliko zelite da nakon zavrsene obuke dolazite i posmatrate dok mi izvodimo tretmane, imate mogucnost da u dogovoru sa nama zakazete Vas dolazak. Ti dolasci su ukjuceni u cenu same obuke kao i dolasci na vezbu koje smo spomenuli na pocetku. 
        <br><br>
        Nakon obuke nastavlja se sa svakodnevnom komunikacijom, gde saljete Vase radove. 
        <br><br>

        To Vam daje mogucnost da u bilo kom momentu postavljate pitanja, razmenjujete iskustva i resavate probleme na koje cete nailaziti tokom vezbe. 
        Nakon završetka obuke dobija se sertifikat o učešću. 
        <br><br>
        <strong>
        Sertifikat o stecenoj vestini dobijate kada savladate nakon obuke sve kompletno. 
        </strong>

        <br><br>
        <strong>
        Svi polaznici Moa Academy dobijaju starter kit bez obzira da li je pocetni ili profesionalni kurs u pitanju sa svim potrebnim materijalima neophodnim za vežbanje i dalji rad.
        </strong>

      </p>
    </div><!-- tittle -->
  </div><!-- .container -->
</section><!-- .client-say-area-style2 -->
<!--  Section-End -->     

<!-- Section-Start --> 
<section class="bg-light-alt padding-small-top">
  <div class="container">
    <div class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>ZA ONE KOJI ŽELE VIŠE </h2>
       <img style="height:40px;" src="/images/d1.png">
      <p style="text-align: center; text-center: inter-word;">
       
        <br><br>
        Pored Osnovnog kursa imamo i kurseve za vec postojece umetnike u microblading tehnici kao i sve koji zele da se dalje usavršavaju u svom poslu:
        <strong>
        <br><br>
        Perfection obuka
        <br><br>
        Branching obuka
        <br><br>
        Shading obuka
        <br><br>
      </strong>
      Edukacija u oblasti mikropigmentacije odrzava se u nasem studiju u Bulevaru oslobodjenja 239,Beograd i u Vardarskoj 1c Novi Sad . Nudimo vam grupne i individualne obuke.
      <br><br>
      </p>
    </div><!-- tittle -->
  </div>

  <div class="book-now overlay dark-overlay">
    <div class="container">
      <div class="row">
          <div class="col-md-3">
            <div class="logo">
              <img style="height: 50px;" src="{{asset('images/logo-beli.png')}}" alt="logo">
            </div>
          </div>
          <div class="col-md-9 text-center">
            <h4>Dodatne informacije i zakazivanje:<strong><a style="color: white;" href="mailto:dunjaopacic@moa-academy.com"> dunjaopacic@moa-academy.com</a></strong> </h4>
          </div>
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .book-now -->
</section>


<section><!-- our-protfolio-area -->
  <div class="our-protfolio-area protfolio-style2 padding-medium-top-bottom">  
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <img src="{{asset('images/edukacije/microblading/1.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
            <img src="{{asset('images/edukacije/microblading/4.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
            <img src="{{asset('images/edukacije/microblading/2.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".95s">
            <img src="{{asset('images/edukacije/microblading/33.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
      </div><!-- .row -->   
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-12 no-padding text-center">
              <div class="protfolio-single-item six-column wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
                <img src="{{asset('images/edukacije/microblading/5.jpg')}}" alt="images">
                 <!-- .protfolio-content-inner -->
              </div><!-- .protfolio-single-item -->
            </div><!-- .col-md-12 -->
          </div><!-- .row -->   
        </div><!-- .col-md-6  -->
        <div class="col-md-6">
          <div class="row">                    
            <div class="col-md-6 col-sm-6 no-padding text-center">
              <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
                <img src="{{asset('images/edukacije/microblading/6.jpg')}}" alt="images">
                 <!-- .protfolio-content-inner -->
              </div><!-- .protfolio-single-item -->
            </div><!-- .col-md-6 -->         
            <div class="col-md-6 col-sm-6 no-padding text-center">
              <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
                <img src="{{asset('images/edukacije/microblading/8.jpg')}}" alt="images">
                 <!-- .protfolio-content-inner -->
              </div><!-- .protfolio-single-item -->
            </div><!-- .col-md-6 -->  
            
            
          </div><!-- .row -->   
        </div><!-- .col-md-6  -->
      </div><!-- .row -->   
    </div><!-- .container -->
  </div><!-- .our-protfolio-area -->
</section>
 <!--  Section-End -->
@stop




       