@extends('layout')

@section('title')
 - Permanent Make Up Edukacija
@stop

@section('main')
<div class="about-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="heading-table">
          <div class="heading-table-cell">
            <h2 class="heading-text">PERMANENT MAKE UP</h2>   
             <div class="header-menu">
               <ul class="list-inline">
                  <li ><a href="/">NASLOVNA /</a></li>
                  <li ><a href="/edukacije">EDUKACIJE /</a></li>
                  <li><span class="active">PERMANENT MAKE UP</span></li>
               </ul>
             </div>
            </div>
          </div>
        </div><!-- col-md-12 -->
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .about-header -->

  <div class="service-main-content padding-small-bottom">
 <div class="container">
   <div class="row" style="padding-top: 30px;">
     <div class="title col-md-8 service-content wow fadeIn"  data-wow-duration="1s" data-wow-delay=".5s">
       <div class="service-single-image-holder">
         <img src="{{asset('images/edukacije/pmu-pozadina.jpg')}}" alt="image">
       </div>
       <h3>MOA PERMANENT MAKE UP</h3>
       <div class="saparator1"></div>
       <p>MOA PMU je jedinstven koncept obuke permanent make up-a.
        <BR>
        Predstavlja internacionalu platformu za razmenu znanja i veština u permanent make up industriji.
        <BR>
        U svoje obuke implementiramo inovacije iz sveta trajne šminke, a kroz interaktivan rad omogućavamo svakom polazniku siguran ulazak u biznis sektor ove grane estetike.

        </p>

     </div>
     <div class="col-md-4 service-sidebar wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
      <div class="widget">
        <a href="/kontakt">
        <img width="90%" src="{{asset('images/edukacije/zakazi-pmu.jpg')}}">
        </a>
       </div>
       <div class="widget" style="width: 90%;">
          <div class="heare-catagory">
             <ul>
                <li>
                <a href="/edukacije/microblading">Microblading<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
                <li>
                <a href="/edukacije/makeup">Make Up<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
                <li>
                <a href="/edukacije/pmu">Permanent Make Up<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
                <li>
                <a href="/edukacije/microneedling">Microneedling i plasma pen<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
             </ul>
          </div>
       </div>
       
     </div>
   </div>
 </div>
</div> 

<!-- Section-Start --> 
<section class="bg-light-alt padding-small-top">
  <div class="container">
    <div class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>Zašto MOA PMU?</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p > <!-- style="text-align: justify; text-justify: inter-word;" -->
        
      -Pažnju usmeravamo na praktičan rad i preciznost
      <br><br> 
      -Uviđamo prednost rada u malim grupama kao i individualne potrebe polaznika
      <br><br> 
      - Bavimo se Vašim ličnim razvojem
      <br><br> 
      -Učimo Vas kako da negujete kontinuiran odnos sa klijentima
      <br><br> 
      -Zajednički cilj su nam uspešni rezultati
      <br><br> 
      <STRONG>MOA PMU koncept sastoji se iz 3 MODULA:
        <br><br>MOA BASIC
        <br><br>MOA PERFECTION
        <br><br>MOA MASTER
      </STRONG>
      </p>
    </div><!-- tittle -->
    
  </div> 
</section>
<!--  Section-End -->


<!-- Section-Start --><!-- client-say-Area-Style2  -->
<section class="client-say-area-style2 overlay dark-overlay padding-small-top-bottom">
  <div style="z-index: 4; position:relative;" class="container">
     <div  class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>MOA BASIC</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p> <!--style="text-align: justify; text-justify: inter-word;"" -->
        <strong>1.dan- Teorijski deo</strong><br><br>
          Uvodno upoznavanje, 
          Upoznavanje sa MOA PMU konceptom, 
          Odgovornosti permanentnog make up-a, 
          Higijena i priprema radnog mesta, 
          Indikacije i kontraindikacije, 
          Posttretmanska nega, 
          Dokumentacija (fotodokumentacija), 
          Teorija boja (pigmenti koji se primenjuju), 
          Objašnjenje pribora za rad, 
          Upoznavanje sa mašinicom, 
          Tehnika crtanja (oči i usne), 
          Tehnike zatezanja kože, 
          Rad na lateksu (simulacija kože), 
          Evaluacija (rezime dana)
          <br><br>
        <strong>2.dan- Praktični deo- Eyeliner</strong><br><br>
          Rad na lateksu, 
          Tehnike crtanja, 
          Tehnike zatezanje kože, 
          Rad na živom modelu, 
          Evaluacija
          <br><br>
        <strong>3.dan- Praktični deo- Usne</strong><br><br>
        Rad na lateksu, 
        Tehnike crtanja, 
        Kontura i senčenje usana, 
        Tehnike zatezanje kože, 
        Rad na živom modelu, 
        Evaluacija
        <br><br><br>
        Na našim edukacijama imate obezbeđen sav neophodan materijal.<br>
Nakon završene edukacije dobijate <strong>MOA sertifikat o učešću.</strong><br>
Takođe, dobijate i <strong>MOA Starter Kit</strong> za Vaš samostalan rad.<br>
Narednih <strong>6 meseci</strong> pratimo Vaš rad pružajući Vam <strong>stalnu online podršku</strong>.<br>
Nakon nekolicine uspešnih radova na živim modelima, kada sta uspešno savladali tehniku iscrtavanja
eyeliner-a i usana, dobijate <strong>sertifikat MOA PMU Artist (MOA Permanent make up Artist).</strong><br>
      </p>
    </div><!-- tittle -->
  </div><!-- .container -->
</section><!-- .client-say-area-style2 -->
<!--  Section-End -->     

<!-- Section-Start --> 
<section class="bg-light-alt padding-small-top">
  <div class="container">
    <div class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>MOA PERFECTION </h2>
       <img style="height:40px;" src="/images/d1.png">
      <p > <!--style="text-align: center; text-center: inter-word;"-->
       
        
        Čestitamo, postali ste MOA PMU Artist i sad je vreme za korak dalje u Vašoj karijeri.
        Perfection stepen edukacije omogućava Vam profesionalno usavršavanje na polju permanentnog
        make up-a.
        <br>
        Namenjen je artistima sa iskustvom u radu sa klijentima.
        <br>
        <strong>Po završetku, svaki polaznik polaže završni ispit i dobija setifikat o položenoj obuci.</strong>

      </p>
      <br><br>
      <h2>MOA MASTER </h2>
       <img style="height:40px;" src="/images/d1.png">
      <p> <!--style="text-align: center; text-center: inter-word;"-->
        Svim polaznicima MOA edukacija omogućavamo učešće u najvišem modulu MOA koncepta.
        <br>
        <strong>Aktivnim i praktičnim usvajanjem znanja stičete zvanje MOA MASTER.</strong>
        <br>
        Uspešnim savladavanjem ovog stepena učesnicima nudimo saradnju u vidu uključivanja u naš tim.
        Mogućnost da postanete MOA Edukatori budućim generacijama MAKEOVER ACADEMY studenata
        predstavlja vrhunac Vašeg napretka.
        <br>
        Dobrodošli u tim koji stvara uspeh.<br>
        <strong>
          Tu smo da zajedno pretvaramo planove u dela.
        </strong>
      </p>
    </div><!-- tittle -->
  </div>
  <div class="book-now overlay dark-overlay">
    <div class="container">
      <div class="row">
          <div class="col-md-3">
            <div class="logo">
              <img style="height: 50px;" src="{{asset('images/logo-beli.png')}}" alt="logo">
            </div>
          </div>
          <div class="col-md-9 text-center">
            <h4>Dodatne informacije i zakazivanje:<strong><a style="color: white;" href="mailto:jelenajocic@moa-academy.com"> 
jelenajocic@moa-academy.com</a></strong> </h4>
          </div>
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .book-now -->
</section>

<section><!-- our-protfolio-area -->
  <div class="our-protfolio-area protfolio-style2 padding-medium-top-bottom">  
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-12 no-padding text-center">
          
        </div><!-- .col-md-3 -->
        <div class="col-md-4 col-sm-12 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".95s">
            <img src="{{asset('images/222.jpeg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
      </div><!-- .row -->   

    </div><!-- .container -->
  </div><!-- .our-protfolio-area -->
</section>

<!--  Section-End -->
@stop