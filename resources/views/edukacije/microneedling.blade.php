@extends('layout')

@section('title')
 - Microneedling Edukacija
@stop

@section('main')
<div class="about-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="heading-table">
          <div class="heading-table-cell">
            <h2 class="heading-text">MICRONEEDLING I PLASMA PEN</h2>   
             <div class="header-menu">
               <ul class="list-inline">
                  <li ><a href="/">NASLOVNA /</a></li>
                  <li ><a href="/edukacije">EDUKACIJE /</a></li>
                  <li><span class="active">MICRONEEDLING I PLASMA PEN</span></li>
               </ul>
             </div>
            </div>
          </div>
        </div><!-- col-md-12 -->
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .about-header -->

  <div class="service-main-content padding-small-bottom">
 <div class="container">
   <div class="row" style="padding-top: 30px;">
     <div class="title col-md-8 service-content wow fadeIn"  data-wow-duration="1s" data-wow-delay=".5s">
       <div class="service-single-image-holder">
         <img src="{{asset('images/edukacije/face.jpg')}}" alt="image">
       </div>
       <h3>MOA MICRONEEDLING I PLASMA PEN</h3>
       <div class="saparator1"></div>
       <p><strong>Microneedling</strong>  tretman podstiče proizvodnju kolagena i povećava prokrvljenost kože čime se postižu rezultati poboljšanja izgleda kože i tonusa kože te ona postaje zdravija i sjajnija. Ova tehnika je pronašla široku primenu kod uklanjanja bora, ožiljaka nastalih od akni, uklanjanja strija, pigmentacija kao i lečenja stanja alopecije.<br><br>

        <strong>Plasma Pen</strong> tretman је revolucionarna neinvazivna metoda za savršeno podmlađivanje lica i tela. Omogućava postizanje rezultata uporedivih sa rezultatima estetske hirurgije.  Koristi se za nehirurško podizanje očnih kapaka, zatezanje kože vrata, uklanjanje bora na licu i strija; zatim za uklanjanje ožiljaka od akni kao i postoperativnih ožiljaka. 

        </p>

     </div>
     <div class="col-md-4 service-sidebar wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
      <div class="widget">
        <a href="/kontakt">
        <img width="90%" src="{{asset('images/edukacije/zakazi-microneedling.jpg')}}">
        </a>
       </div>
       <div class="widget" style="width: 90%;">
          <div class="heare-catagory">
             <ul>
                <li>
                <a href="/edukacije/microblading">Microblading<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
                <li>
                <a href="/edukacije/makeup">Make Up<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
                <li>
                <a href="/edukacije/pmu">Permanent Make Up<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
                <li>
                <a href="/edukacije/microneedling">Microneedling i plasma pen<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
             </ul>
          </div>
       </div>
       
     </div>
   </div>
 </div>
</div> 



<!-- Section-Start --><!-- client-say-Area-Style2  -->
<section class="client-say-area-style2 overlay dark-overlay padding-small-top-bottom">
  <div style="z-index: 4; position:relative;" class="container">
     <div  class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>Kome je namenjena edukacija?</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p style="text-align: justify; text-justify: inter-word;"> <!-- -->
        Za sve polaznike koji imaju želju da se upoznaju sa microneedling i plasma pen tehnikama, MOA Academy ima zadatak širenja znanja i stručnog osposobljavanja kroz teorijski deo (anatomija i fiziologija lica, osnovna i specijalna dermatologija, primena i delovanje tretmana) i praktičan rad na modelima uz nadzor edukatora koji  je specijalizovan za tretmane lica i tela.
        <br><br> 
        Edukacije su namenjene za potpune početnike ali i za kontinuirani razvoj kozmetičara.
        <br><br> 
        <strong>Sav potreban materijal obezbeđen je u toku i posle edukacija.</strong>


      </p>
    </div><!-- tittle -->
  </div><!-- .container -->
</section><!-- .client-say-area-style2 -->
<!--  Section-End -->     

<!-- Section-Start --> 
<section class="bg-light-alt padding-small-top">
  <div class="container">
    <div class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>Koilko traje edukacija? </h2>
       <img style="height:40px;" src="/images/d1.png">
      <p style="text-align: center; text-center: inter-word;">
       
        <br><br>
        Trajanje jedne edukacije je dva dana, a nakon toga sprovodi se online edukacija u trajanju od šest meseci. 
        <br><br>
        Pružamo znanje i savete o potrebnom materijalu i profesionalnom alatu, preparatima i opremi, te mogućnost kupovine istih. Edukacije mogu biti grupne ili individualne, sve zavisi od vašeg izbora.  
        <br><br>
        <strong>
        Uz stečeno znanje i veštine, nakon završene obuke dobija se sertifikat o učešću, a nakon uspešno položenog završnog ispita sertifikat MOA Academy.
        <br><br>
        Mi smo uvek tu za Vas kao podrška u daljem radu,edukacijama, opremi ili novostima iz sveta kozmetike. </strong>
      </p>
    </div><!-- tittle -->
  </div>
  <div class="book-now overlay dark-overlay">
    <div class="container">
      <div class="row">
          <div class="col-md-3">
            <div class="logo">
              <img style="height: 50px;" src="{{asset('images/logo-beli.png')}}" alt="logo">
            </div>
          </div>
          <div class="col-md-9 text-center">
            <h4>Dodatne informacije i zakazivanje:<strong><a style="color: white;" href="mailto:lidijatesic@moa-academy.com"> lidijatesic@moa-academy.com</a></strong> </h4>
          </div>
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .book-now -->
</section>


<section><!-- our-protfolio-area -->
  <div class="our-protfolio-area protfolio-style2 padding-medium-top-bottom">  
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-12 no-padding text-center">
          
        </div><!-- .col-md-3 -->
        <div class="col-md-4 col-sm-12 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".95s">
            <img src="{{asset('images/111.jpeg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
      </div><!-- .row -->   

    </div><!-- .container -->
  </div><!-- .our-protfolio-area -->
</section>
 <!--  Section-End -->
<!--  Section-End -->
<!--  Section-End -->
@stop