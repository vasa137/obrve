@extends('layout')

@section('title')
 - Make Up Edukacija
@stop

@section('main')
<div class="about-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="heading-table">
          <div class="heading-table-cell">
            <h2 class="heading-text">MAKE UP</h2>   
             <div class="header-menu">
               <ul class="list-inline">
                  <li ><a href="/">NASLOVNA /</a></li>
                  <li ><a href="/edukacije">EDUKACIJE /</a></li>
                  <li><span class="active">MAKE UP</span></li>
               </ul>
             </div>
            </div>
          </div>
        </div><!-- col-md-12 -->
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .about-header -->

  <div class="service-main-content padding-large-bottom">
 <div class="container">
   <div class="row" style="padding-top: 30px;">
     <div class="title col-md-8 service-content wow fadeIn"  data-wow-duration="1s" data-wow-delay=".5s">
       <div class="service-single-image-holder">
         <img src="{{asset('images/edukacije/makeup.jpg')}}" alt="image">
       </div>
       <h3>MOA MAKE UP</h3>
       <div class="saparator1"></div>
       <p>Obuhvata kompletnu šminkersku edukaciju u vidu bazne obuke za sve koji žele da postanu priznati profesionalni šminkeri kao i različite tipove obuka za usavršavanje profesionalnih šminkera</p>

     </div>
     <div class="col-md-4 service-sidebar wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
      <div class="widget">
        <a href="/kontakt">
        <img width="90%" src="{{asset('images/edukacije/zakazi-make-up.jpg')}}">
        </a>
       </div>
       <div class="widget" style="width: 90%;">
          <div class="heare-catagory">
             <ul>
                <li>
                <a href="/edukacije/makeup">Microblading<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
                <li>
                <a href="/edukacije/makeup">Make Up<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
                <li>
                <a href="/edukacije/pmu">Permanent Make Up<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
                <li>
                <a href="/edukacije/makeup">Microblading<span class="pull-right">
                  <i class="fa fa-angle-right"></i></span></a>
                </li>
             </ul>
          </div>
       </div>       
     </div>
   </div>
 </div>
</div> 

<!-- Section-Start --> 
<section class="bg-light-alt padding-small-top">
  <div class="container">
    <div class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>MOA Bazna obuka obuhvata:</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p style="text-align: justify; text-justify: inter-word;">
        Mesec dana intenzivne edukacije u vidu teorijskog i praktičnog obučavanja. 
        Obuke su predvidene kao grupne ili individualne po vašem izboru.
        <br><br>
        Teorijski deo obuke podrazumeva upoznavanje sa kožom, osnovama dermatologije. Upoznavanje sa alatom, dekorativnom kozmetikom i njenim sastavom. 
        <br><br>
        Praktični deo obuke podrazumeva rad na modelima uz vodstvo mentora. 
        <br><br>
        Šminka i potreban materijal za rad tokom obuke su obezbedjeni. 
        <br><br> 
        Tokom ove bazne obuke polaznici imaju mogućnost konsultacija za razvoj ličnog marketing.
        <br><br>
        Svi polaznoci nakon obuke dobijaju <strong>MOA Kit.</strong>
        <br><br>
        Polaznici dobijanu sertifikat o pohadjanju obuke, zatim  nakon zavrešene obuke polažu za <strong>završni MOA Academy sertifikat</strong> o položenoj obuci. 

      </p>
    </div><!-- tittle -->
    <div class="row text-center">
      <div class="col-md-12">
        <div class="fun-fact-image-holder wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
          <img src="{{asset('images/fun-fact-image.png')}}" alt="image">
        </div>
      </div>
    </div>
  </div> 
</section>
<!--  Section-End -->


<!-- Section-Start --><!-- client-say-Area-Style2  -->
<section class="client-say-area-style2 overlay dark-overlay padding-small-top-bottom">
  <div style="z-index: 4; position:relative;" class="container">
     <div  class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>MOA PERFECTION</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p  > <!--style="text-align: justify; text-justify: inter-word;" -->
        <strong>Predstavlja intenzivno usavršavanje za profesionalne šminkere. </strong>
        <br><br>
        Polaznik  usavršava odabranu oblast uz pomoć edukatora.
        <br><br>
        Perfection traje dva dana sa intenzivnim i interaktivnim radom na modelima.
        <br><br>
        Svaki polaznik dobija <strong>sertifikat o učešću.</strong>
        <br><br>
        Perfection je predvidjen kao individualno usavršavanje za šminkere koji su već upoznati sa osnovama i imaju praksu u radu sa klijentima.
        <br><br>
        Nakon završene obuke, polaznik polaže završni ispit kada <strong>dobija MOA Academy sertifikat o položenoj obuci.</strong>
        <br><br>

      </p>
    </div><!-- tittle -->
  </div><!-- .container -->
</section><!-- .client-say-area-style2 -->
<!--  Section-End -->     

<!-- Section-Start --> 
<section class="bg-light-alt padding-small-top">
  <div class="container">
    <div class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>MOA MASTER</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p style="text-align: justify; text-justify: inter-word;">
       <strong>
        <br><br>
        Je stepen usavršavanja predvidjen za sve članove MOA edukacija. Predstvlja mogućnost dalje saradnje i prenošenje stečenog znanja novim MOA generacijama u vidi eduktora.
        <br><br>
        MASTER traje  dve dana i podrzumeva rad od više kombinovanih šminkerskih stilova.
        <br><br>
        Svaki polaznik dobija sertifikat o učešću.
        <br><br>
        Nakon završene obuke profesionalni šminker polaže ispit i dobija MOA Academy sertifikat o položenoj obuci.
        <br><br>
        Nakon položenog ispita profesionalni  šminkeri uz tutorstvo edukatora stiču praksu i postaju MAKEOVER ACADEMY edukatori.
        <br><br>
      </strong>
      </p>
    </div><!-- tittle -->
  </div>
  <div class="book-now overlay dark-overlay">
            <div class="container">
              <div class="row">
                  <div class="col-md-3">
                    <div class="logo">
                      <img style="height: 50px;" src="{{asset('images/logo-beli.png')}}" alt="logo">
                    </div>
                  </div>
                  <div class="col-md-9 text-center">
                    <h4>Za sve ostale informacije obratite se na mail: <a style="color: white;" href="mailto:kristinauhelji@moa-academy.com"> kristinauhelji@moa-academy.com</a></h4>
                  </div>
              </div><!-- .row -->
            </div><!-- .container -->
          </div><!-- .book-now -->
</section>


<section><!-- our-protfolio-area -->
  <div class="our-protfolio-area protfolio-style2 padding-medium-top-bottom">  
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <img src="{{asset('images/edukacije/makeup/1.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
            <img src="{{asset('images/edukacije/makeup/2.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
            <img src="{{asset('images/edukacije/makeup/3.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".95s">
            <img src="{{asset('images/edukacije/makeup/4.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
      </div><!-- .row -->   
      
      <div class="row">
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <img src="{{asset('images/edukacije/makeup/5.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
            <img src="{{asset('images/edukacije/makeup/6.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
            <img src="{{asset('images/edukacije/makeup/7.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".95s">
            <img src="{{asset('images/edukacije/makeup/8.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
      </div><!-- .row -->    
      <div class="row">
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <img src="{{asset('images/edukacije/makeup/9.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
            <img src="{{asset('images/edukacije/makeup/10.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
            <img src="{{asset('images/edukacije/makeup/11.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".95s">
            <img src="{{asset('images/edukacije/makeup/12.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
      </div><!-- .row -->  
      <div class="row">
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <img src="{{asset('images/edukacije/makeup/13.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
            <img src="{{asset('images/edukacije/makeup/14.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
            <img src="{{asset('images/edukacije/makeup/15.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".95s">
            <img src="{{asset('images/edukacije/makeup/16.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
      </div><!-- .row -->
      <div class="row">
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <img src="{{asset('images/edukacije/makeup/17.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
            <img src="{{asset('images/edukacije/makeup/18.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
            <img src="{{asset('images/edukacije/makeup/19.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".95s">
            <img src="{{asset('images/edukacije/makeup/20.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
      </div><!-- .row --> 
      <div class="row">
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <img src="{{asset('images/edukacije/makeup/21.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
            <img src="{{asset('images/edukacije/makeup/22.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
            <img src="{{asset('images/edukacije/makeup/23.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".95s">
            <img src="{{asset('images/edukacije/makeup/24.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
      </div><!-- .row -->
      <div class="row">
        <div class="col-md-3 col-sm-6 no-padding text-center">
          <div class="protfolio-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <img src="{{asset('images/edukacije/makeup/25.jpg')}}" alt="images">
             <!-- .protfolio-content-inner -->
          </div><!-- .protfolio-single-item -->
        </div><!-- .col-md-3 -->
       
      </div><!-- .row --> 
    </div><!-- .container -->
  </div><!-- .our-protfolio-area -->
</section>
 <!--  Section-End -->
<!--  Section-End -->
@stop