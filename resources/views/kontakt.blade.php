@extends('layout')

@section('title')
 - Kontakt
@stop


@section('scriptsBottom')
  <script src='https://www.google.com/recaptcha/api.js' async defer></script>
@endsection

@section('main')
<div class="about-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="heading-table">
          <div class="heading-table-cell">
            <h2 class="heading-text">KONTAKT</h2>   
             <div class="header-menu">
               <ul class="list-inline">
                  <li ><a href="/">NASLOVNA /</a></li>
                   <li><span class="active">KONTAKT</span></li>
               </ul>
             </div>
            </div>
          </div>
        </div><!-- col-md-12 -->
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .about-header -->

  <div class="contact-us-area padding-large-top">
    <div class="container">
      <div class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
        <h2>Beograd</h2>
        <img style="height:40px;" src="/images/d1.png">
        <p>Kontaktirajte nas za sve potrebne informacije, zakazivanje tretmana i edukacija.</p>
      </div><!-- tittle -->
      <div class="row">
        <div class="contact-information">
          <div class="col-md-4 col-sm-4">
            <div class="contact-us-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
              <div class="contact-icon">
                <a href="#"><i class="pe-7s-map-marker"></i></a>
              </div>
              <h5>Adresa</h5>                      
               <a href="#">Bulevar Oslobodjenja 239</a>
               <a href="#">Beograd</a>
              
            </div>
          </div><!-- .col-md-4 -->
          <div class="col-md-4 col-sm-4">
            <div class="contact-us-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
              <div class="contact-icon">
                <a href="tel:+381 63-888-63-63"><i class="pe-7s-call"></i></a>
              </div>
              <h5>Telefon</h5>                      
               <a href="tel:+381 63-888-63-63">+381 063-888-63-63</a>
              
            </div>
          </div><!-- .col-md-4 -->
          <div class="col-md-4 col-sm-4">
            <div class="contact-us-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
              <div class="contact-icon">
                <a href="mailto:info@moa-academy.com"><i class="pe-7s-mail"></i></a>
              </div>
              <h5>E-mail</h5>  
               <a href="mailto:info@moa-academy.com">info@moa-academy.com</a>
              
            </div>
          </div><!-- .col-md-4 -->
        </div><!-- .contact-information -->
      </div><!-- .row -->
    </div><!-- .container -->
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2832.552870309662!2d20.474571515405554!3d44.769532879098875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a71491d8da3cf%3A0x73dbba8198d4a6f8!2sMOA+Academy!5e0!3m2!1sen!2srs!4v1565386721103!5m2!1sen!2srs" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div><!-- .contact-us-area -->

  <div class="contact-us-area padding-large-top">
    <div class="container">
      <div class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
        <h2>NOVI SAD</h2>
        <img style="height:40px;" src="/images/d1.png">
        <p>Kontaktirajte nas za sve potrebne informacije, zakazivanje tretmana i edukacija.</p>
      </div><!-- tittle -->
      <div class="row">
        <div class="contact-information">
          <div class="col-md-4 col-sm-4">
            <div class="contact-us-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
              <div class="contact-icon">
                <a href="#"><i class="pe-7s-map-marker"></i></a>
              </div>
              <h5>Adresa</h5>                      
               <a href="#">Vardarska 1c</a>
               <a href="#">Novi Sad</a>
              
            </div>
          </div><!-- .col-md-4 -->
          <div class="col-md-4 col-sm-4">
            <div class="contact-us-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
              <div class="contact-icon">
                <a href="tel:+381 63-888-63-63"><i class="pe-7s-call"></i></a>
              </div>
              <h5>Telefon</h5>                      
               <a href="tel:+381 69-122-00-92">+381 069-122-00-92</a>
              
            </div>
          </div><!-- .col-md-4 -->
          <div class="col-md-4 col-sm-4">
            <div class="contact-us-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
              <div class="contact-icon">
                <a href="mailto:info@moa-academy.com"><i class="pe-7s-mail"></i></a>
              </div>
              <h5>E-mail</h5>  
               <a href="mailto:info@moa-academy.com">info@moa-academy.com</a>
              
            </div>
          </div><!-- .col-md-4 -->
        </div><!-- .contact-information -->
      </div><!-- .row -->
    </div><!-- .container -->
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2809.299916248468!2d19.817002415386032!3d45.24172767909888!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475b10254488738b%3A0x2d17d5e5d1ff59e2!2sVardarska%201c%2C%20Novi%20Sad%2021000!5e0!3m2!1sen!2srs!4v1568580029010!5m2!1sen!2srs" width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    
  </div><!-- .contact-us-area -->


  <section class="padding-small-bottom bg-light-alt "><!-- appoinment-area -->
  <div class="appoinment-area">
    <div class="container">
      <div class="row">
        <div class="col-md-12 wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
          <form action="/kontaktiraj" id="kontakt_forma" method="POST">
            {{csrf_field()}}
            <div class="appoinment-contact-form">
              <div class="row">
                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="perch-user"></i></div>
                      <input type="text" class="form-control" placeholder="Ime i prezime" required name="ime_prezime">
                  </div><!-- .input-group -->
                </div><!-- .col-md-6  -->                       
                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="pe-7s-mail"></i></div>
                      <input type="email" class="form-control" placeholder="E-mail" required name="mail">
                  </div><!-- .input-group -->
                </div><!-- .col-md-6  --> 

                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="pe-7s-call"></i></div>
                      <input type="text" class="form-control" placeholder="Telefon" name="telefon">
                  </div><!-- .input-group -->
                </div><!-- .col-md-6  --> 
              </div><!-- .row -->                      

              <select name="opcija" class="select-option">
                 <option selected value="Samo se informišem">Samo se informišem</option>
                 
                 <option value="Microblading Edukacija">Microblading Edukacija</option>
                 <option value="Microneedling i Plasma Pen Edukacija">Microneedling i Plasma Pen Edukacija</option>
                 <option value="Permanent Makeup Edukacija">Permanent Makeup Edukacija</option>
                 <option value="Makeup Edukacija">Makeup Edukacija</option>

                 <option value="Zakazivanje tretmana u salonu">Zakazivanje tretmana u salonu</option>
              </select>

              <div class="input-group">
                <div class="input-group-addon input-group-addon2"><i class="perch-comment-alt"></i></div>
                  <textarea class="form-control" name="poruka" placeholder="Vaša poruka..."></textarea>
              </div><!-- .input-group -->
              <div class="input-group">
                  <div style="width:100%;" class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}" id="captcha">
                    <div class="g-recaptcha" data-theme="light" data-sitekey="{{config('app.re_cap_site')}}" style="overflow:hidden; display:flex; justify-content:center;"></div>
                    @if ($errors->has('g-recaptcha-response'))
                      <br/>
                      <span class="invalid-feedback" role="alert" style="display:block;">
                                                        <strong>Morate potvrditi da niste robot.</strong>
                                                    </span>
                    @endif
                  </div>
                </div>
              <div class="row text-center">
                <div class="col-md-12">
                <input type="submit" class="btn btn-default" value="Pošalji">
                </div>
              </div>

            </div><!-- .appoinment-contact-form -->
          </form><!-- form -->

        </div>
        
      </div><!-- .row -->
    </div><!-- .container -->
  </div>
</section>
   
@stop