@extends('layout')


@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/klijentProizvodi.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/popupDialog.css')}}"/>
@endsection

@section('scriptsBottom')
    <script src="{{asset('js/popupDialog.js')}}"></script>
    <script src="{{asset('js/klijentKorpa.js')}}"></script>
    <script src="{{asset('js/prodavnica.js')}}"></script>
@endsection


@section('main')
@include('include.popupDialog', ['poruka' => 'Uspešno ste dodali proizvod u korpu. <br/> Da li želite da pregledate korpu?', 'linkUspesno' => '/korpa'])

 <div class="about-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="heading-table">
          <div class="heading-table-cell">
            <h2 class="heading-text">PRODAVNICA</h2>   
             <div class="header-menu">
               <ul class="list-inline">
                  <li ><a href="/">NASLOVNA /</a></li>
                   <li><span class="active">PRODAVNICA</span></li>
               </ul>
             </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

 <div class="shop-main-content padding-large-top-bottom"><!-- .shop-main-content Start--> 
   <div class="container">
     <div class="row">
        <div class="col-md-3 sidebar-wrap">
         <div class="sidebar-wrap-content wow fadeIn" data-wow-duration=".5s" data-wow-delay=".5s">


             <form id="filterForma">
            <div class="widget">
              <p>KATEGORIJE</p>
              <ul class="categories">
                  @foreach($stabloKategorija as $kategorija)
                      <li>
                          <input type="checkbox" onchange="filtriraj();" style="cursor:pointer;" id="checkbox-kategorija-{{$kategorija->id}}" name="kategorije[]" value="{{$kategorija->id}}"/>
                          <label style="cursor:pointer;" for="checkbox-kategorija-{{$kategorija->id}}">{{$kategorija->naziv}}</label>
                      </li>
                  @endforeach
              </ul>
            </div><!-- .widget -->  
            
            <div class="widget">
              <p>BRENDOVI</p>
              <ul class="categories top-brand">
                  @foreach($brendovi as $brend)
                      <li>
                          <input onchange="filtriraj();" style="cursor:pointer;" id="checkbox-brend-{{$brend->id}}" type="checkbox" name="brendovi[]" value="{{$brend->id}}"/>
                          <label style="cursor:pointer;" for="checkbox-brend-{{$brend->id}}">{{$brend->naziv}}</label>
                      </li>
                  @endforeach
              </ul>
            </div><!-- .widget -->
             </form>
            <div class="widget">
              <p>AKCIJA</p>
              <div class="discount-offer text-center">
                <div class="discount-offer-inner">
                  <h5>get flat</h5>
                  <span>15%</span>
                  <h5>offer for fresh arrivals</h5>
                </div>
              </div>
            </div><!-- .widget -->

             @if(count($najprodavanijiProizvodi) > 0)
            <div class="widget shop-recent-posts"><!-- widget-recent-post -->
              <p>ISTAKNUTI PROIZVODI</p>
              <div class="recent-posts">
                 <ul>
                     @foreach($najprodavanijiProizvodi as $proizvod)
                    <li>
                       <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}">
                          <div class="recent-posts-image">
                              @if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike .'.jpg')))
                                  <img class="img-circle" src="/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg" alt="<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>">
                              @endif
                          </div>
                          {{$proizvod->naziv}}

                           @if($proizvod->na_popustu)
                               <br/><del>{{number_format($proizvod->cena, 0, ',', '.')}} din</del>  <span class="post-meta color-text">{{number_format($proizvod->cena_popust, 0, ',', '.')}} din</span>
                           @else
                               <span class="post-meta color-text">{{number_format($proizvod->cena, 0, ',', '.')}} din</span>
                           @endif
                       </a>
                    </li>
                     @endforeach
                     <br/>
                 </ul>
              </div>
            </div><!-- .widget -->
                 @endif
          </div><!-- .sidebar-wrap-content -->
       </div><!-- .col-md-3 .sidebar-wrap -->



       <div class="col-md-9 content-wrap wow fadeIn" data-wow-duration=".5s" data-wow-delay=".5s">
           <form id="sortirajForma">
               <select onchange="filtriraj()" name="sort">
                   <option hidden value="null">Sortiraj po</option>
                   <option value="cena-asc">Ceni rastuće</option>
                   <option value="cena-desc">Ceni opadajuće</option>
                   <option value="naziv-asc">Naziv rastuće</option>
                   <option value="naziv-desc">Naziv opadajuće</option>
               </select>
           </form>
        <!-- .huge-collection 
        <div class="huge-collection overlay dark-overlay">
          <div class="huge-collection-content text-center">
            <h3>HUGE COLLECTION FOR</h3>
            <span class="color-text">2017</span>
          </div>
        </div>-->

            <div class="row" id="proizvodiInclude">
                @include('include.listaProizvoda')
            </div><!-- .row -->
           <div class="ajax-load" ></div>
       </div><!-- .col-md-9 .content-wrap -->

     </div><!-- .row -->
   </div><!-- .container -->
 </div> <!-- .shop-main-content End--> 


@stop