<!doctype html>
<html class="no-js" lang="">
    
<!-- Mirrored from themeperch.net/html/scissors/index2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 May 2019 13:19:53 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>MOA Academy</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">

        <link href='https://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}">
        <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
        <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/selectize.bootstrap3.css')}}">
        <link rel="stylesheet" href="{{asset('fonts/flaticon.css')}}">
        <link rel="stylesheet" href="{{asset('css/pe-icon-7-stroke.css')}}">
        <link rel="stylesheet" href="{{asset('css/perch.css')}}">
        <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
        <link rel="stylesheet" href="{{asset('css/unite-gallery.css')}}">
        <link rel="stylesheet" href="{{asset('css/jquery-ui-1.10.3.custom.css')}}">
        <link rel="stylesheet" href="{{asset('css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('css/menu.css')}}">
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
        <link rel="stylesheet" href="{{asset('css/color.css')}}">
        <link rel="stylesheet" href="{{asset('css/responsive.css')}}">

        <link rel="stylesheet" href="{{asset('assets/js/plugins/slick/slick-theme.css')}}"/>

        @yield('scriptsTop')
    </head>
    <body>

  

        <!-- Start-Header -->
        <header>
          <div class="bg-light-alt">
              <div class="container">
                  <div class="row text-center">
                          <a href="/"><img src="{{asset('images/logo.png')}}" style="width: 85%" alt="MOA academy"></a>
                  </div><!-- .row -->
                  <br>
                  <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-3 ">
                          <a href="viber://chat?number=+381638886363"><img src="{{asset('images/kontakt-ikonice/viber.png')}}" rel="nofollow" width="90%" alt="MOA academy"></a>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-3 ">
                          <a href="https://api.whatsapp.com/send?phone=+381638886363"><img src="{{asset('images/kontakt-ikonice/wa.png')}}" rel="nofollow" width="90%" alt="MOA academy"></a>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-3 ">
                          <a href="http://m.me/dunjabakicphibrows" rel="nofollow"><img src="{{asset('images/kontakt-ikonice/msg.png')}}" width="90%" alt="MOA academy"></a>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-3 ">
                          <a href="mailto:info@moa-academy.com"><img src="{{asset('images/kontakt-ikonice/mail.png')}}" rel="nofollow" width="90%" alt="MOA academy"></a>
                    </div>

                  </div><!-- .row -->
                   <br><br>
                   <div class="container">
                        <div class="row text-center">
                          <a style="width: 100%" href="tel:+381638886363" class="btn btn-default">CALL US: (+381) 063-888-63-63</a>
                        </div>
                        <br>
                         <div class="row text-center">
                          <a style="width: 100%" href="/" class="btn btn-default">visit our website</a>
                        </div>

                   </div>
                   

                  <br><br>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2832.552870309662!2d20.474571515405554!3d44.769532879098875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a71491d8da3cf%3A0x73dbba8198d4a6f8!2sMOA+Academy!5e0!3m2!1sen!2srs!4v1565386721103!5m2!1sen!2srs" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div><!-- .Container -->
          </div><!-- topbar -->
          @yield('slider')
        </header>     
        <!-- End-Header -->


        <!-- Footer-Start -->
        <footer class="dark-bg">
            <div class="container">
              <div class="row footer-style2">
                <div class="col-md-4 col-sm-4">
                  <div class="widget">
                    <div class="logo"><a href="/"><h1><img src="{{asset('images/logo-beli.png')}}"  alt="logo"></h1></a></div>              
                    <div class="social-icon-circle">
                      <a href="https://www.facebook.com/moaacademydunja"><i class="fa fa-facebook"></i></a>
                      <a href="https://www.instagram.com/moa.academy/"><i class="fa fa-instagram"></i></a>
                     </div>
                  </div><!-- .widget -->
                </div><!-- .col-md-4 -->
                
              </div><!-- .row -->
              <div class="cpoyright-bar text-center">
                <p class="copyright">MOA &copy; Copyrights 2019. All Rights Reserved</p>
              </div><!-- .cpoyright-bar -->
            </div><!-- .container -->
        </footer>
        <!-- Footer-End -->
       

        <script src="http://maps.google.com/maps/api/js?sensor=false&amp;libraries=geometry&amp;v=3.7"></script>
        <script src="{{asset('code.jquery.com/jquery-1.12.3.min.js')}}"></script>
        <script src="{{asset('js/jquery-1.12.3.min.js')}}" ></script>
        <script src="{{asset('js/vendor/modernizr-2.8.3.min.js')}}" ></script>
        <script src="{{asset('js/plugins.js')}}" ></script>
        <script src="{{asset('js/bootstrap.min.js')}}" ></script>
        <script src="{{asset('js/selectize.min.js')}}" ></script>
        <script src="{{asset('js/owl.carousel.min.js')}}" ></script>
        <script src="{{asset('cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js')}}"></script>
        <script src="{{asset('js/jquery.counterup.min.js')}}" ></script>
        <script src="{{asset('js/jquery.magnific-popup.min.js')}}" ></script>
        <script src="{{asset('js/unitegallery.min.js')}}" ></script>
        <script src="{{asset('js/ug-theme-compact.js')}}" ></script>
        <script src="{{asset('js/jquery-ui-1.10.3.custom.min.js')}}" ></script>
        <script src="{{asset('js/wow.min.js')}}" ></script>
        <script src="{{asset('js/menu.js')}}" ></script>
        <script src="{{asset('js/main.js')}}" ></script>
        <script src="{{asset('js/slick.min.js')}}"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>

        @yield('scriptsBottom')
    </body>

<!-- Mirrored from themeperch.net/html/scissors/index2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 May 2019 13:23:43 GMT -->
</html>
