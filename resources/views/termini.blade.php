@extends('layout')


@section('main')
<div class="about-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="heading-table">
          <div class="heading-table-cell">
            <h2 class="heading-text">TERMINI EDUKACIJA</h2>   
             <div class="header-menu">
               <ul class="list-inline">
                  <li ><a href="/">NASLOVNA /</a></li>
                  <li><span class="active">TERMINI EDUKACIJA</span></li>
               </ul>
             </div>
            </div>
          </div>
        </div><!-- col-md-12 -->
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .about-header -->


<!-- Section-Start --> 
<section class="padding-large-top-bottom"><!-- Fresh news -->
  <div class="container">
    <div class="title text-center title-margin-bottom-medium wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">
      <h2>TERMINI EDUKACIJA</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p>Iskoristite priliku i postanite deo MOA tima! Izaberite neku od naših edukacija i postanite sertifikovani profesionalac i deo MOA Academy tima! </p>
    </div><!-- tittle -->
      <div class="row our-work-list our-work-padding-bottom text-center">
    <div class="col-md-3 col-sm-6 col-xs-12 our-work-single-item wow fadeInLeft" data-wow-duration=".25s" data-wow-delay=".5s">
      <div class="work-image-holder">
        <img src="images/edukacije/edukacija-1.jpg" alt="image">
      </div>
      <div class="our-work-details-outer">
        <div class="our-work-details-outer">
        <div class="our-work-details">
          <div class="our-work-details-icon">
            <i class="fa fa-info"></i>
          </div>
          <h4><a href="#">BEOGRAD 10.10.2019.</a></h4>
          <div class="saparator2"></div>
          <p>Ovde moze biti adresa, kontatk ili neki drugi bitan info.</p>
        </div>              
      </div>              
      </div>              
    </div><!-- .col-md-3 -->
    <div class="col-md-3 col-sm-6 col-xs-12 our-work-single-item wow fadeInLeft" data-wow-duration=".45s" data-wow-delay=".5s">
      <div class="work-image-holder">
        <img src="images/edukacije/edukacija-2.jpg" alt="image">
      </div>
      <div class="our-work-details-outer">
        <div class="our-work-details">
          <div class="our-work-details-icon">
            <i class="fa fa-info"></i>
          </div>
          <h4><a href="#">BEOGRAD 10.10.2019.</a></h4>
          <div class="saparator2"></div>
          <p>Ovde moze biti adresa, kontatk ili neki drugi bitan info.</p>
        </div>              
      </div>              
    </div><!-- .col-md-3 -->
    <div class="col-md-3 col-sm-6 col-xs-12 our-work-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
      <div class="work-image-holder">
        <img src="images/edukacije/edukacija-3.jpg" alt="image">
      </div>
      <div class="our-work-details-outer">
        <div class="our-work-details">
          <div class="our-work-details-icon">
            <i class="fa fa-info"></i>
          </div>
          <h4><a href="#">BEOGRAD 10.10.2019.</a></h4>
          <div class="saparator2"></div>
          <p>Ovde moze biti adresa, kontatk ili neki drugi bitan info.</p>
        </div>              
      </div>              
    </div><!-- .col-md-3 -->
    <div class="col-md-3 col-sm-6 col-xs-12 our-work-single-item wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
      <div class="work-image-holder">
        <img src="images/edukacije/edukacija-4.jpg" alt="image">
      </div>
      <div class="our-work-details-outer">
        <div class="our-work-details">
          <div class="our-work-details-icon">
            <i class="fa fa-info"></i>
          </div>
          <h4><a href="#">BEOGRAD 10.10.2019.</a></h4>
          <div class="saparator2"></div>
          <p>Ovde moze biti adresa, kontatk ili neki drugi bitan info.</p>
        </div>              
      </div>              
    </div><!-- .col-md-3 -->
  </div><!-- .row -->

  <div class="row our-work-list text-center">
    <div class="col-md-3 col-sm-6 col-xs-12 our-work-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
      <div class="work-image-holder">
        <img src="images/edukacije/edukacija-4.jpg" alt="image">
      </div>
      <div class="our-work-details-outer">
        <div class="our-work-details">
          <div class="our-work-details-icon">
            <i class="fa fa-info"></i>
          </div>
          <h4><a href="#">BEOGRAD 10.10.2019.</a></h4>
          <div class="saparator2"></div>
          <p>Ovde moze biti adresa, kontatk ili neki drugi bitan info.</p>
        </div>               
      </div>              
    </div><!-- .col-md-3 -->
    <div class="col-md-3 col-sm-6 col-xs-12 our-work-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
      <div class="work-image-holder">
        <img src="images/edukacije/edukacija-3.jpg" alt="image">
      </div>
      <div class="our-work-details-outer">
        <div class="our-work-details">
          <div class="our-work-details-icon">
            <i class="fa fa-info"></i>
          </div>
          <h4><a href="#">BEOGRAD 10.10.2019.</a></h4>
          <div class="saparator2"></div>
          <p>Ovde moze biti adresa, kontatk ili neki drugi bitan info.</p>
        </div>              
      </div>              
    </div><!-- .col-md-3 -->
    <div class="col-md-3 col-sm-6 col-xs-12 our-work-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
      <div class="work-image-holder">
        <img src="images/edukacije/edukacija-2.jpg" alt="image">
      </div>
      <div class="our-work-details-outer">
        <div class="our-work-details">
          <div class="our-work-details-icon">
            <i class="fa fa-info"></i>
          </div>
          <h4><a href="#">BEOGRAD 10.10.2019.</a></h4>
          <div class="saparator2"></div>
          <p>Ovde moze biti adresa, kontatk ili neki drugi bitan info.</p>
        </div>              
      </div>              
    </div><!-- .col-md-3 -->
    <div class="col-md-3 col-sm-6 col-xs-12 our-work-single-item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
      <div class="work-image-holder">
        <img src="images/edukacije/edukacija-1.jpg" alt="image">
      </div>
      <div class="our-work-details-outer">
        <div class="our-work-details">
          <div class="our-work-details-icon">
            <i class="fa fa-info"></i>
          </div>
          <h4><a href="#">BEOGRAD 10.10.2019.</a></h4>
          <div class="saparator2"></div>
          <p>Ovde moze biti adresa, kontatk ili neki drugi bitan info.</p>
        </div>              
      </div>              
    </div><!-- .col-md-3 -->
  </div><!-- .row -->
  </div><!-- .container -->
</section>
<!--  Section-End -->
@stop