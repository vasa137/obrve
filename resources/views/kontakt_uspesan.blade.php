@extends('layout')


@section('title')
 - Kontakt uspešan
@stop

@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/klijentProizvodi.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/popupDialog.css')}}"/>
@endsection

@section('scriptsBottom')
    <script src="{{asset('js/popupDialog.js')}}"></script>
    <script src="{{asset('js/klijentKorpa.js')}}"></script>
    <script src="{{asset('js/prodavnica.js')}}"></script>
@endsection


@section('main')
@include('include.popupDialog', ['poruka' => 'Uspešno ste dodali proizvod u korpu. <br/> Da li želite da pregledate korpu?', 'linkUspesno' => '/korpa'])


@section('slider')
<div class="header-slider">
    <div class="slider-item-index1 overlay dark-overlay">
      <div class="slider-content">
        <h1 class="color-text">USPЕŠNO STE NAS KONTAKTIRALI</h1>
        <img src="images/logo-beli.png" alt="icon">
        <h2 class="color-text"><a style="color:white;" href="/">Nazad na početnu stranu</a></h2>

      </div>
    </div><!-- slider-item-index1 -->

    <div class="slider-item-index3 overlay dark-overlay">
      <div class="slider-content">
        <h1 class="color-text">USPЕŠNO STE NAS KONTAKTIRALI</h1>
        <img src="images/logo-beli.png" alt="icon">
        <h2 class="color-text"><a style="color:white;" href="/">Nazad na početnu stranu</a></h2>

      </div>
    </div><!-- slider-item-index3 -->
  </div><!-- header-slider -->
  @stop


@stop