<!doctype html>
<html class="no-js" lang="">
    
<!-- Mirrored from themeperch.net/html/scissors/index2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 May 2019 13:19:53 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>MOA @yield('title')</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">


        <link href='https://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}">
        <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
        <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/selectize.bootstrap3.css')}}">
        <link rel="stylesheet" href="{{asset('fonts/flaticon.css')}}">
        <link rel="stylesheet" href="{{asset('css/pe-icon-7-stroke.css')}}">
        <link rel="stylesheet" href="{{asset('css/perch.css')}}">
        <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
        <link rel="stylesheet" href="{{asset('css/unite-gallery.css')}}">
        <link rel="stylesheet" href="{{asset('css/jquery-ui-1.10.3.custom.css')}}">
        <link rel="stylesheet" href="{{asset('css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('css/menu.css')}}">
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
        <link rel="stylesheet" href="{{asset('css/color.css')}}">
        <link rel="stylesheet" href="{{asset('css/responsive.css')}}">

        <link rel="stylesheet" href="{{asset('assets/js/plugins/slick/slick-theme.css')}}"/>

        @yield('scriptsTop')

        <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1292948074209997');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1292948074209997&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117964354-9"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-117964354-9');
		</script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159459225-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
 
  gtag('config', 'UA-159459225-1');
</script>
    </head>
    <body>

  

        <!-- Start-Header -->
        <header>
          <div class="topbar bg-light-alt">
              <div class="container">
                  <div class="row">
                      <div class="col-md-2 col-sm-2">
                          <h1 class="logo"><a href="/"><img src="{{asset('images/logo.png')}}" alt="MOA academy"></a></h1>
                      </div><!-- .col-md-3 -->
                      <div class="col-md-10 col-sm-10">
                        <div class="row">
                          <div class="col-md-2 col-sm-2 contact-info-padding">
                              <div class="contact-info-inner call-us">
                                <a href="tel:+381 63-888-63-63">
                                  <p class="color-text">POZOVITE NAS</p>
                                  <h5>063-888-63-63</h5>
                                </a>
                              </div>
                          </div><!-- .col-md-3 -->
                          <div class="col-md-4 col-sm-4 contact-info-padding">
                              <div class="contact-info-inner">
                                  <div class="info-icon img-circle">
                                      <i class="glyphicon glyphicon-home"></i>
                                  </div>
                                  <a href="#">
                                    <p class="color-text">Bulevar oslobođenja 239</p>
                                    <p>Beograd</p>
                                  </a>
                              </div>
                          </div><!-- .col-md-3 -->
                          <div class="col-md-4 col-sm-4 contact-info-padding contact-info-inner">
                              <div class="contact-info-inner">
                                  <div class="info-icon  img-circle">
                                      <i class="pe-7s-mail"></i>
                                  </div>
                                  <a href="mailto:info@moa-academy.com">
                                    <p class="color-text">E-mail</p>
                                    <p>info@moa-academy.com</p>
                                  </a>
                              </div>
                          </div><!-- .col-md-3 -->                          
                          <div class="col-md-2 col-sm-2 text-right">
                              <div class="contact-info-inner">
                                  <div class="social-icon-circle">
                                      <a href="https://www.facebook.com/moaacademydunja" target="_blank" rel="nofollow"><i class="fa fa-facebook"></i></a>
                                      <a href="https://www.instagram.com/moa.academy/"><i class="fa fa-instagram" target="_blank" rel="nofollow"></i></a>
                                   </div>
                              </div>
                          </div><!-- .col-md-3 -->
                        </div> 
                      </div><!-- .col-md-9 -->
                  </div><!-- .row -->
              </div><!-- .Container -->
          </div><!-- topbar -->
           

          <nav class="header-nav sticky-header dark-bg">
            <div class="container main-menu">
                <div class="row">
                  <div class="col-md-10">           
                    <div class="nav-menu cssmenu menu-line-off" id="header-menu-wrap">  
                      <ul class="menu list-inline">
                          <li><a href="/" class="active">Naslovna</a>
                            
                          </li><!-- li 
                          <li><a href="/o-nama">O Nama</a>
                              
                          </li>
                          -->
                          <li><a href="/prodavnica">Prodavnica</a>
                            
                          </li><!-- li -->
                          <li><a href="/tretmani">MOA Tretmani</a>
                            
                          </li><!-- li -->
                          <li><a href="/edukacije">Edukacije</a>
                              <ul>
                                 <li>
                                  <a href="/edukacije/microblading">MICROBLADING</a>
                                </li>
                                <li>
                                  <a href="/edukacije/microneedling">MICRONEEDLING I PLASMA PEN</a>
                                </li>
                                <li>
                                  <a href="/edukacije/pmu">PERMANENT MAKE UP</a>
                                </li>
                                <li>
                                  <a href="/edukacije/makeup">MAKE UP</a>
                                </li>
                                <!--
                                <li>
                                  <a href="/edukacije/body">BODY</a>
                                </li>
                                <li>
                                  <a href="/edukacije/face">FACE</a>
                                </li>
                                <li>
                                  <a href="/edukacije/lips">LIPS</a>
                                </li>
                                <li>
                                  <a href="/edukacije/hair">HAIR</a>
                                </li>
                                <li>
                                  <a href="/edukacije/eyes">EYES</a>
                                </li>
                                <li>
                                  <a href="/edukacije/laser">LASER</a>
                                </li>
                                -->
                              </ul>
                          </li> 
                          <li><a href="/kontakt">KONTAKT</a>
                          </li>
                          <li><a href="/akcija-edukacije">AKCIJA !</a>
                          </li>
                      
                          
                      </ul>
                    </div><!-- #header-menu-wrap -->
                   </div>
                   <div class="col-md-2">
                    
                    
                     <a href="http://en.moa-academy.com">
                          <img style="height: 30px; padding-top: 10px;" src="{{asset('/images/eng.png')}}">
                     </a>
                     <a href="http://esp.moa-academy.com">
                          <img style="height: 30px; padding-top: 10px;" src="{{asset('/images/esp.png')}}">
                     </a>
                     <a href="http://moa-academy.com/">
                          <img style="height: 30px; padding-top: 10px;" src="{{asset('/images/srb.png')}}">
                     </a>
                   
                   </div>
                </div>
            </div><!-- .container -->          
          </nav><!-- .header-nav -->
          @yield('slider')
        </header>     
        <!-- End-Header -->


        @yield('main')

        <!-- Footer-Start -->
        <footer class="dark-bg">
            <div class="container">
              <div class="row footer-style2">
                <div class="col-md-4 col-sm-4">
                  <div class="widget">
                    <div class="logo"><a href="/"><h1><img src="{{asset('images/logo-beli.png')}}" alt="logo"></h1></a></div>
                    <p></p>                  
                    <div class="social-icon-circle">
                      <a href="https://www.facebook.com/moaacademydunja" target="_blank" rel="nofollow"><i class="fa fa-facebook"></i></a>
                      <a href="https://www.instagram.com/moa.academy/"><i class="fa fa-instagram" target="_blank" rel="nofollow"></i></a>             
                     </div>
                  </div><!-- .widget -->
                </div><!-- .col-md-4 -->
                <div class="col-md-8 col-sm-8">
                  <div class="row">                    
                    <div class="col-md-6 col-sm-6">
                      <div class="widget">
                        <h4>EDUKACIJE</h4>
                        <div class="flickr-feed">
                            
                          <img width="120" height="120" src="{{asset('images/edukacije/brows-2.jpg')}}" alt="image">
                          <img width="120" height="120" src="{{asset('images/edukacije/makeup-2.jpg')}}" alt="image">
                          <img width="120" height="120" src="{{asset('images/edukacije/pmu.jpg')}}" alt="image">
                          <img width="120" height="120" src="{{asset('images/edukacije/face-2.jpg')}}" alt="image">
                          
                        </div>
                      </div><!-- .widget -->
                    </div> <!--               
                    <div class="col-md-0 col-sm-0">
                      <div class="widget">
                        <h4>Vaucer</h4>
                        <img src="images/gift.png" alt="image">
                      </div> 
                    </div> -->
                    <div class="col-md-6 col-sm-6">
                      <div class="widget">
                        <h4>INFORMACIJE</h4>
                        <h4><a style="color: white;" href="tel:+381 63-888-63-63"><i class="fa fa-phone"></i>+381 63-888-63-63</a></h4>
                        <h4><a style="color: white;" href="mailto:info@moa-academy.com"><i class="fa fa-envelope"></i>info@moa-academy.com</a></h4>
                      </div><!-- .widget -->
                    </div><!-- .col-md-4 -->
                  </div><!-- .row -->
                </div><!-- .col-md-8 -->
              </div><!-- .row -->
              <div class="cpoyright-bar text-center">
                <p class="copyright">MOA &copy; Copyrights 2019. All Rights Reserved</p>
              </div><!-- .cpoyright-bar -->
            </div><!-- .container -->
        </footer>
        <!-- Footer-End -->
       

        <script src="http://maps.google.com/maps/api/js?sensor=false&amp;libraries=geometry&amp;v=3.7"></script>
        <script src="{{asset('code.jquery.com/jquery-1.12.3.min.js')}}"></script>
        <script src="{{asset('js/jquery-1.12.3.min.js')}}" ></script>
        <script src="{{asset('js/vendor/modernizr-2.8.3.min.js')}}" ></script>
        <script src="{{asset('js/plugins.js')}}" ></script>
        <script src="{{asset('js/bootstrap.min.js')}}" ></script>
        <script src="{{asset('js/selectize.min.js')}}" ></script>
        <script src="{{asset('js/owl.carousel.min.js')}}" ></script>
        <script src="{{asset('cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js')}}"></script>
        <script src="{{asset('js/jquery.counterup.min.js')}}" ></script>
        <script src="{{asset('js/jquery.magnific-popup.min.js')}}" ></script>
        <script src="{{asset('js/unitegallery.min.js')}}" ></script>
        <script src="{{asset('js/ug-theme-compact.js')}}" ></script>
        <script src="{{asset('js/jquery-ui-1.10.3.custom.min.js')}}" ></script>
        <script src="{{asset('js/wow.min.js')}}" ></script>
        <script src="{{asset('js/menu.js')}}" ></script>
        <script src="{{asset('js/main.js')}}" ></script>
        <script src="{{asset('js/slick.min.js')}}"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>

        @yield('scriptsBottom')
    </body>

<!-- Mirrored from themeperch.net/html/scissors/index2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 May 2019 13:23:43 GMT -->
</html>
