@extends('layout')


@section('slider')


<div class="header-slider">
    

    <div class="slider-item-index3 overlay dark-overlay">
      <div class="slider-content">
        <h1 class="color-text"></h1>
        
           <img src="images/logo-beli.png" alt="icon">
        
		
        <h2 class="color-text"></h2>

      </div>
    </div>

    <div class="slider-item-index1 overlay dark-overlay">
      <div class="slider-content">
        <h1 class="color-text"></h1>
        <img src="images/logo-beli.png" alt="icon">
        <h2 class="color-text"></h2>
      </div>
    </div><!-- slider-item-index1 -->
  </div><!-- header-slider -->
  @stop

@section('main')


<!-- Section-Start --> 
<section class="padding-small-top"><!-- service-area -->
 <div class="service-area"><!-- .service-area -->
    <div class="title text-center title-margin-bottom-small">
      <h2>Zašto MOA?</h2>
       <img style="height:40px;" src="/images/d1.png">
       <!--
      <p>Ovde mozemo napisati neke vazne karakteristike, prednosti...</p>
    -->
    </div><!-- tittle -->
    <div class="row">
      <div class="col-md-4 text-right">
        <div class="service-details about-us-details service-icon-left  wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
          <ul>
            <li>
              <div class="service-icon about-us-icon pull-right">
                <i class="fa fa-check"></i>
              </div>
              <h4><br>Ne pratimo trendove.Mi ističemo lepotu!</h4>
              
            </li>
            <li>
              <div class="service-icon about-us-icon pull-right">
                <i class="fa fa-check"></i>
              </div>
              <h4><br>Mentori MOA tima su Vam uvek na raspolaganju!</h4>
              
            </li>
            <li>
              <div class="service-icon about-us-icon pull-right">
                <i class="fa fa-check"></i>
              </div>
              <h4><br>Posetite nas i uverite se u kvalitet naših usluga.</h4>
              <p></p>
            </li>
          </ul>
          <!-- ul -->
        </div><!-- .about-us-details -->
      </div><!-- .col-md-4 -->
      <div class="col-md-4 text-center">
        <div class="service-image-holder">
          <img src="images/dunja.png" alt="image">
        </div>
      </div><!-- .col-md-4 -->
      <div class="col-md-4">
        <div class="service-details about-us-details wow fadeInRight" data-wow-duration=".5s" data-wow-delay=".25s">
          <ul>
            <li>
              <div class="service-icon about-us-icon">
                <i class="fa fa-check"></i>
              </div>
              <h4><br>Kod nas je prirodna lepota uvek u trendu!</h4>
            </li>
            <li>
              <div class="service-icon about-us-icon">
                <i class="fa fa-check"></i>
              </div>
              <h4><br>Uz nas imate mogućnost konstantnog napredovanja!</h4>
              
            </li>
            <li>
              <div class="service-icon about-us-icon">
                <i class="fa fa-check"></i>
              </div>
              <h4><br>Svi članovi MOA tima imaju posebne pogodnosti!</h4>
              
            </li>
          </ul>
          <!-- ul -->
        </div><!-- .about-us-details -->
      </div><!-- .col-md-4 -->
    </div><!-- .row -->
 </div>
</section>
<!--  Section-End -->

<!-- Section-Start --> 
<section class=""><!-- .working-experience-area -->        
  <div class="book-now overlay dark-overlay">
    <div class="container">
      <div class="row">
          <div class="col-md-3">
            <div class="logo">
              <img  style="height: 50px;" src="images/logo-beli.png" alt="logo">
            </div>
          </div>
          <div class="col-md-7 text-center">
            <h4>ZAINTERESOVANI STE ZA INDIVIDUALNE EDUKACIJE?</h4>
          </div>
          <div class="col-md-2 text-center">
            <a href="/kontakt" class="btn btn-default">KONTAKTIRAJTE NAS</a>
          </div>
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .book-now -->
</section>
<!--  Section-End -->

<!-- Section-Start --> 
<section class="padding-large-top-bottom"><!-- Fresh news -->
  <div class="container">
    <div class="title text-center title-margin-bottom-large wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">
      <h2>MOA EDUKACIJE</h2>
       <img style="height:40px;" src="/images/d1.png">
       <p>Iskoristite priliku i postanite deo MOA tima! Izaberite neku od naših edukacija i postanite sertifikovani profesionalac i deo MOA Academy tima!  </p>
    </div><!-- tittle -->
    <div class="post-list">
      <div class="row">
        <div class="col-md-3 post-padding">
          <div class="blog-post-single-item bg-light-alt wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <div class="post-image-holder">
              <a href="/edukacije/microblading">
              <img src="{{asset('images/edukacije/brows-2.jpg')}}" alt="image">
                </a>
            </div>
            
            <div class="blog-post-content text-center">
              <a href="/edukacije/microblading">
                <h5 style="text-align:center;">MICROBLADING</h5>
            </a>
              <p>Zasniva se na osnovama microblading vestine, prenosenju teorije, prakticne primene tretmana i osnovnih pravila u radu. </p>
              <a href="/edukacije/microblading" class=" btn btn-default">DETALJNIJE</a>
              <br><br>
            </div>
          </div>
        </div><!-- .col-md-4 -->
        <div class="col-md-3 post-padding">
          <div class="blog-post-single-item bg-light-alt wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <div class="post-image-holder">
              <a href="/edukacije/makeup">
              <img src="{{asset('images/edukacije/makeup-2.jpg')}}" alt="image">
                </a>
            </div>
            
            <div class="blog-post-content text-center">
              <a href="/edukacije/makeup">
                <h5 style="text-align:center;">Makeup</h5>
            </a>
              <p>Obuhvata kompletnu šminkersku edukaciju u vidu bazne obuke za sve koji žele da postanu priznati profesionalni šminkeri kao i različite...</p>
              <a href="/edukacije/makeup" class=" btn btn-default">DETALJNIJE</a>
              <br><br>
            </div>
          </div>
        </div><!-- .col-md-4 -->
        <div class="col-md-3 post-padding">
          <div class="blog-post-single-item bg-light-alt wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <div class="post-image-holder">
              <a href="/edukacije/pmu">
              <img src="{{asset('images/edukacije/pmu.jpg')}}" alt="image">
                </a>
            </div>
            
            <div class="blog-post-content text-center">
              <a href="/edukacije/pmu">
                <h5 style="text-align:center;">PERMANENT MAKE UP</h5>
            </a>
              <p>MOA PMU je jedinstven koncept obuke permanent make up-a.Predstavlja internacionalu platformu za razmenu znanja i veština...</p>
              <a href="/edukacije/pmu" class=" btn btn-default">DETALJNIJE</a>
              <br><br>
            </div>
          </div>
        </div><!-- .col-md-4 -->
        <div class="col-md-3 post-padding">
          <div class="blog-post-single-item bg-light-alt wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <div class="post-image-holder">
              <a href="/edukacije/microneedling">
              <img src="{{asset('images/edukacije/face-2.jpg')}}" alt="image">
                </a>
            </div>
            
            <div class="blog-post-content text-center">
              <a href="/edukacije/microneedling">
                <h5 style="text-align:center;">MICRONEEDLING</h5>
            </a>
              <p>Microneedling tretman podstiče proizvodnju kolagena i povećava prokrvljenost kože čime se postižu rezultati poboljšanja...</p>
              <a href="/edukacije/microneedling" class=" btn btn-default">DETALJNIJE</a>
              <br><br>
            </div>
          </div>
        </div><!-- .col-md-4 -->
      </div><!-- .news-list -->
    </div><!-- .row -->
    <!--
    <div class="row text-center">
      <div class="col-md-12 wow zoomIn" data-wow-duration=".5s" data-wow-delay=".25s">
        <a href="/edukacije" class="btn btn-default">POGLEDAJTE SVE</a>
      </div>
    </div>
  -->
  </div><!-- .container -->
</section>
<!--  Section-End -->


<!-- Section-Start --> <!-- working-hour-area-style2 -->
<section class="working-hour-area working-hour-area-style2 overlay dark-overlay padding-large-top-bottom">
  <div class="container">
    <div class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>MOA BEAUTY SALON</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p>Posetite MOA Beauty salone u Beogradu ili Novom Sadu i uživajte u prijatnom ambijentu i profesionalnoj usluzi. </p>
    </div><!-- tittle -->
    <div class="working-week text-center">
      <div class="working-day-outer">
        <div class="working-day wow zoomIn" data-wow-duration=".5s" data-wow-delay=".2s">
          <span class="day color-text">Pon</span>
          <span class="time">9<sup>00</sup> - 17<sup>00</sup></span>
        </div>
      </div>
      <div class="working-day-outer">
        <div class="working-day wow zoomIn" data-wow-duration=".5s" data-wow-delay=".4s">
          <span class="day color-text">Uto</span>
          <span class="time">9<sup>00</sup> - 17<sup>00</sup></span>
        </div>
      </div>
      <div class="working-day-outer">
        <div class="working-day wow zoomIn" data-wow-duration=".5s" data-wow-delay=".6s">
          <span class="day color-text">Sre</span>
          <span class="time">9<sup>00</sup> - 17<sup>00</sup></span>
        </div>
      </div>
      <div class="working-day-outer">
        <div class="working-day wow zoomIn" data-wow-duration=".5s" data-wow-delay=".8s">
          <span class="day color-text">Čet</span>
          <span class="time">9<sup>00</sup> - 17<sup>00</sup></span>
        </div>
      </div>
      <div class="working-day-outer">
        <div class="working-day wow zoomIn" data-wow-duration=".5s" data-wow-delay="1s">
          <span class="day color-text">Pet</span>
          <span class="time">9<sup>00</sup> - 17<sup>00</sup></span>
        </div>
      </div>
      <div class="working-day-outer">
        <div class="working-day wow zoomIn" data-wow-duration=".5s" data-wow-delay="1.2s">
          <span class="day color-text">Sub</span>
          <span class="time">9<sup>00</sup> - 17<sup>00</sup></span>
        </div>
      </div>
      <div class="working-day-outer">
        <div class="working-day wow zoomIn" data-wow-duration=".5s" data-wow-delay="1.4s">
          <span class="day color-text">Ned</span>
          <span class="time">9<sup>00</sup> - 17<sup>00</sup></span>
        </div>
      </div>
    </div><!-- .row -->
    <div class="row text-center">
      <div class="queries-call wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
        <h4 class="color-text"><a class="color-text" href="/kontakt">ZAKAŽITE VAŠ TERMIN</a></h4>
        <h3><a style="color: white;" href="tel:063-888-63-63">063-888-63-63 </a></h3>
      </div>
    </div>
  </div><!-- .container -->
</section>
<!--  Section-End -->
<!-- 
<section class="padding-large-top-bottom">
  <div class="shoping-area">
    <div class="container">                    
      <div class="title text-center title-margin-bottom-medium wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
        <h2>IZDVAJAMO IZ PONUDE</h2>
         <img style="height:40px;" src="/images/d1.png">
        <p>Ovde se nalazi lista proizvoda koji su npr na akciji.</p>
      </div>
      <div class="row text-center">
        <div class="col-md-3 col-sm-6 shoping-single-item-inner new wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
          <div class="shoping-single-item display-flex">
            <div class="shoping-image-holder vertical-align-middle">
              <img src="images/arrival/arrival1.png" alt="image">
            </div>
          </div>
          <div class="item-name-price">                
            <span class="item-name">PROIZVOD 1</span>
            <h5><del>1,499</del> <span class="color-text">999 din</span></h5>
          </div>     
          <div class="shoping-hover">
            <a href="#" target="_blank">
              <span>PROIZVOD 1</span>
              <h4 class="color-text">999 din</h4>
            </a>
            
            <a href="#" class="btn btn-default">DODAJ U KORPU</a>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 shoping-single-item-inner new wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".45s">
          <div class="shoping-single-item display-flex">
            <div class="shoping-image-holder vertical-align-middle">
              <img src="images/arrival/arrival2.png" alt="image">
            </div>
          </div>
          <div class="item-name-price">                
            <span class="item-name">PROIZVOD 2</span>
            <h5> <span class="color-text">1,599 din</span></h5>
          </div>     
          <div class="shoping-hover">
            <a href="#" target="_blank">
              <span>PROIZVOD 2</span>
              <h4 class="color-text">999 din</h4>
            </a>
            
            <a href="#" class="btn btn-default">DODAJ U KORPU</a>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 shoping-single-item-inner new wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".7s">
          <div class="shoping-single-item display-flex">
            <div class="shoping-image-holder vertical-align-middle">
              <img src="images/arrival/arrival3.png" alt="image">
            </div>
          </div>
          <div class="item-name-price">                
            <span class="item-name">PROIZVOD 3</span>
            <h5><del>1,499</del> <span class="color-text">999 din</span></h5>
          </div>     
          <div class="shoping-hover">
            <a href="#" target="_blank">
              <span>PROIZVOD 3</span>
              <h4 class="color-text">999 din</h4>
            </a>
            
            <a href="#" class="btn btn-default">DODAJ U KORPU</a>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 shoping-single-item-inner new sold wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".9s">
          <div class="shoping-single-item display-flex">
            <div class="shoping-image-holder vertical-align-middle">
              <img src="images/arrival/arrival4.png" alt="image">
            </div>
          </div>
          <div class="item-name-price">                
            <span class="item-name">PROIZVOD 4</span>
            <h5><del>1,499</del> <span class="color-text">999 din</span></h5>
          </div>     
          <div class="shoping-hover">
            <a href="#" target="_blank">
              <span>PROIZVOD 4</span>
              <h4 class="color-text">999 din</h4>
            </a>
            
            <a href="#" class="btn btn-default">DODAJ U KORPU</a>
          </div>
        </div>
      </div>
      <div class="row text-center wow zoomIn" data-wow-duration=".5s" data-wow-delay=".25s">
        <a href="#" class="btn btn-default">KOMPLETNA PONUDA</a>
      </div>
    </div>
  </div> 
</section>
 -->



<!-- 
<section>
  <div class="our-work-area our-work-area-style2">            
    <div class="row">
      <div class="col-md-6 col-sm-6 single-work-item wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
        <div class="single-work-image-holder overlay dark-overlay">
          <img src="images/edukacije/edukacija-d1.jpg" alt="image">
        </div>
        <div class="work-icon">
         <a href="images/edukacije/edukacija-d1.jpg" class="full-size"> <i class="pe-7s-plus"></i></a>
        </div>
      </div>              
      <div class="col-md-6 col-sm-6 single-work-item wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
        <div class="single-work-image-holder overlay dark-overlay">
          <img src="images/edukacije/edukacija-d2.jpg" alt="image">
        </div>
        <div class="work-icon">
         <a href="images/edukacije/edukacija-d2.jpg" class="full-size"> <i class="pe-7s-plus"></i></a>
        </div>
      </div>              
      <div class="col-md-6 col-sm-6 single-work-item wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
        <div class="single-work-image-holder overlay dark-overlay">
          <img src="images/edukacije/edukacija-d3.jpg" alt="image">
        </div>
        <div class="work-icon">
         <a href="images/edukacije/edukacija-d3.jpg" class="full-size"> <i class="pe-7s-plus"></i></a>
        </div>
      </div>              
      <div class="col-md-6 col-sm-6 single-work-item wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
        <div class="single-work-image-holder overlay dark-overlay">
          <img src="images/edukacije/edukacija-d4.jpg" alt="image">
        </div>
        <div class="work-icon">
         <a href="images/edukacije/edukacija-d4.jpg" class="full-size"> <i class="pe-7s-plus"></i></a>
        </div>
      </div>
      <div class="work wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
        <h3>OBUKE</h3>
      </div>
    </div>
  </div>
</section>
-->


<!-- Section-Start --> 
<section class="padding-small-top"><!-- our-work-area -->
  <div class="our-work-area">            
    <div class="title text-center title-margin-bottom-large wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">
      <h2>Termini edukacija</h2>
       <img style="height:40px;" src="/images/d1.png">
       <p>Prijavite se za individualne edukacije. </p>
    </div><!-- tittle -->


  <br>

  <div class="row text-center">
      <div class="col-md-12 wow zoomIn" data-wow-duration=".5s" data-wow-delay=".25s">
        <a href="/kontakt" class="btn btn-default">ZAKAŽITE INDIVIDUALNU EDUKACIJU</a>
      </div>
    </div>
    <br>
  <br>

  </div><!-- .our-work-area -->
</section>
 <!--  Section-End -->


<!-- Section-Start --> 
<section class="padding-small-bottom bg-light-alt "><!-- appoinment-area -->
  <div class="appoinment-area">
    <div class="container">
      <div class="row">
        <div class="col-md-12 wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
          <div class="title text-center title-margin-bottom-large">
            <h2>KONTAKTIRAJTE NAS</h2>
             <img style="height:40px;" src="/images/d1.png">
            <p>Kontaktirajte nas za sve potrebne informacije, zakazivanje tretmana i edukacija.</p>
          </div><!-- tittle -->
          <form action="/kontaktiraj" id="kontakt_forma" method="POST">
            {{csrf_field()}}
            <div class="appoinment-contact-form">
              <div class="row">
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="perch-user"></i></div>
                      <input type="text" class="form-control" placeholder="Ime i prezime" required name="ime_prezime">
                  </div><!-- .input-group -->
                </div><!-- .col-md-6  -->                       
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="pe-7s-mail"></i></div>
                      <input type="email" class="form-control" placeholder="E-mail" required name="mail">
                  </div><!-- .input-group -->
                </div><!-- .col-md-6  --> 
              </div><!-- .row -->                      
              <div class="row">
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="pe-7s-call"></i></div>
                      <input type="text" class="form-control" placeholder="Telefon" name="telefon">
                  </div><!-- .input-group -->
                </div><!-- .col-md-6  -->                       
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-calendar-minus-o"></i></div>
                      <input type="text" class="form-control" placeholder="Datum" name="datum">
                  </div><!-- .input-group -->
                </div><!-- .col-md-6  --> 
              </div><!-- .row -->

              <select name="opcija" class="select-option">
                 <option selected value="Samo se informišem">Samo se informišem</option>
                 
                 <option value="Zakazivanje tretmana u salonu">Zakazivanje tretmana u salonu</option>

                 <option value="Edukacije">Edukacije</option>
              </select>

              <div class="input-group">
                <div class="input-group-addon input-group-addon2"><i class="perch-comment-alt"></i></div>
                  <textarea class="form-control" name="poruka" placeholder="Vaša poruka..."></textarea>
              </div><!-- .input-group -->
              <div class="row text-center">
                <div class="col-md-12">
                <input type="submit" class="btn btn-default" value="Pošalji">
                </div>
              </div>

            </div><!-- .appoinment-contact-form -->
          </form><!-- form -->

        </div>
        
      </div><!-- .row -->
    </div><!-- .container -->
  </div>
</section>
<!--  Section-End -->


@stop


