<!doctype html>
<html class="no-js" lang="">
    
<!-- Mirrored from themeperch.net/html/scissors/index2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 May 2019 13:19:53 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>MOA</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">

        <link href='https://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <link rel="stylesheet" href="css/selectize.bootstrap3.css">
        <link rel="stylesheet" href="fonts/flaticon.css">
        <link rel="stylesheet" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" href="css/perch.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/unite-gallery.css">
        <link rel="stylesheet" href="css/jquery-ui-1.10.3.custom.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/menu.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/color.css">
        <link rel="stylesheet" href="css/responsive.css">
    </head>
    <body>

  

        <!-- Start-Header -->
        <header>
           


          <div class="header-slider">
    <div class="slider-item-index1 overlay dark-overlay">
      <div class="slider-content">
        <h1 class="color-text">SAJT JE  FAZI IZRADE</h1>
        <h2 class="color-text"><a href="tel:063-888-63-63">063-888-63-63</a></h2>
        <img src="images/logo-beli.png" alt="icon">
        
      </div>
    </div><!-- slider-item-index1 -->

    <div class="slider-item-index3 overlay dark-overlay">
      <div class="slider-content">
        <h1 class="color-text">SAJT JE  FAZI IZRADE</h1>
        <h2 class="color-text"><a href="tel:063-888-63-63">063-888-63-63</a></h2>
        <img src="images/logo-beli.png" alt="icon">
        

      </div>
    </div><!-- slider-item-index3 -->
  </div><!-- header-slider -->
  </header>     
<!-- End-Header -->




       

    <script src="http://maps.google.com/maps/api/js?sensor=false&amp;libraries=geometry&amp;v=3.7"></script>
    <script src="code.jquery.com/jquery-1.12.3.min.js"></script>
    <script src="js/jquery-1.12.3.min.js"></script>
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/selectize.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script> 
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/unitegallery.min.js"></script>
    <script src="js/ug-theme-compact.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/menu.js"></script>
    <script src="js/main.js"></script>   
</body>

<!-- Mirrored from themeperch.net/html/scissors/index2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 May 2019 13:23:43 GMT -->
</html>
