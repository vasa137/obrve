@extends('layout')

@section('scriptsTop')
  <link rel="stylesheet" href="{{asset('css/popupDialog.css')}}"/>
@endsection

@section('scriptsBottom')
  <script src="{{asset('js/klijentKorpa.js')}}"></script>
  <script src="{{asset('js/popupDialog.js')}}"></script>
@endsection

@section('main')
<div class="about-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="heading-table">
          <div class="heading-table-cell">
            <h2 class="heading-text">KORPA</h2>   
             <div class="header-menu">
               <ul class="list-inline">
                  <li ><a href="/">NASLOVNA /</a></li>
                   <li><span class="active">KORPA</span></li>
               </ul>
             </div>
            </div>
          </div>
        </div><!-- col-md-12 -->
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .about-header -->
             
  <div class="cart-main-content padding-small-top-bottom">
    <div class="container wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">  
      <div class="row">
        <div class="col-md-12" id="korpaPrikaz">
          @if(count($stavke) == 0)
            <h3>Korpa je prazna.</h3>
          @else
          <div class="table-responsive" id="tabele-div">
            <table class="table table-bordered cart-table">
              <thead class="text-center">
                <tr>
                  <th> </th>
                  <th>Proizvod</th>
                  <th>Cena</th>
                  <th>Kupon</th>
                  <th>Kolicina</th>
                  <th>Ukupno</th>
                  <th> </th>
                </tr><!-- .tr -->
              </thead><!-- .thead -->
              <tbody id="tabela-korpa">
              @foreach($stavke as $stavka)
                <tr id="stavka-proizvod-{{$stavka->rowId}}">
                  @include('include.popupDialog', ['poruka' => 'Da li ste sigurni da želite da obrišete proizvod iz korpe?', 'linkUspesno' => 'javascript:obrisiIzKorpe(\''. $stavka->rowId . '\',\'' . $stavka->id .'\')', 'dialogId' => $stavka->rowId])

                  <td> 
                    <div class="cart-image">
                      @if(File::exists(public_path('/images/proizvodi/' .$stavka->id  . '/glavna/' . $stavka->nazivGlavneSlike . '.jpg')))
                        <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $stavka->nazivGlavneSlike))?>/{{$stavka->id}}">
                          <img height="160" width="160" src="{{asset('/images/proizvodi/'. $stavka->id .'/glavna/' . $stavka->nazivGlavneSlike . '.jpg')}}" alt="cart image">
                        </a>
                      @endif
                    </div>
                  </td><!-- .cart-image -->
                  <td>
                    <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $stavka->name))?>/{{$stavka->id}}">
                      <h4 class="title">{{$stavka->name}}</h4>
                    </a>
                  </td>
                  <td>
                    <div class="price">{{number_format($stavka->price, 0, ',', '.')}} din</div>
                  </td>
                  <td>
                    <div class="price" id="kupon-stavka-{{$stavka->rowId}}">@if($stavka->kupon != null) {{$stavka->kupon->kod}} ({{$stavka->kupon->popust*100}}%) @else / @endif</div>
                  </td>
                  <td>
                     <ul class="list-inline">
                       <li><a class="qtminus" href="javascript:azurirajKolicinuUKorpi('{!! $stavka->rowId !!}', '{!! $stavka->id!!}', parseInt($('#qttotal-stavka-{{$stavka->rowId}}').html()) - 1 )"><i class="pe-7s-less"></i></a></li>
                       <li><a id="qttotal-stavka-{{$stavka->rowId}}" class="qttotal">{{$stavka->qty}}</a></li>
                       <li><a class="qtplus" href="javascript:azurirajKolicinuUKorpi('{!! $stavka->rowId !!}', '{!! $stavka->id!!}', parseInt($('#qttotal-stavka-{{$stavka->rowId}}').html()) + 1 )"><i class="pe-7s-plus"></i></a></li>
                     </ul> 
                  </td>
                  <td class="color-text">
                    <div id="stavka-total-{{$stavka->rowId}}" class="price">
                      {{$stavka->total(0, ',', '.')}} din
                    </div>
                  </td>
                  <td><a href="javascript:otvoriDialogSaId('{!! $stavka->rowId!!}')" class="color-text"><i class="fa fa-times"></i></a></td>
                </tr><!-- .tr -->
                @endforeach
              </tbody> <!-- .tbody --> 
            </table><!-- .table -->
            <br/><br/>

            @if(count($stavkeVauceri) > 0)
              @include('include.korpaVauceri', ['stavkeVauceri', $stavkeVauceri])
            @endif
          </div><!-- .table-responsive -->
        </div><!-- .col-md-12 -->
      </div><!-- .row -->
      <div class="row">
        <div class="col-md-8">
          <div class="cart-form">
            <form>
              <a href="/prodavnica" class="btn shoping">Nazad na prodavnicu</a>
              <div class="coupon-code clearfix">
                <label>UNESITE KOD KUPONA</label>
                <input id="kod" type="text" class="form-control" placeholder="UNESITE KOD KUPONA">
                <input onclick="primeniKupon()" type="button" class="form-control apply-coupon" value="ISKORISTITE KUPON">
                <p id="kupon-text"></p>
              </div>
            </form><!-- form -->
          </div><!-- cart-form -->
        </div><!-- .col-md-8 -->
        <div class="col-md-4">
            <h3>UKUPNO</h3>
            <div class="row">
              <div class="total-cart">
                <div class="col-md-12 col-sm-12">
                  <ul class="sub-total">
                      <li class="text-uppercase">Cena<span id="total-cena">{{number_format($total,0 , ',', '.')}} din</span></li>
                  </ul>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">                      
                  <ul class="sub-total">
                    <li class="text-uppercase">Dostava<span>250 din</span></li> 
                  </ul>
                </div>
                
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="total-cart-price">
                  <ul>
                    <li>UKUPNO<span class="color-text" id="total-cena-dostava">{{number_format($total + 250,0 , ',', '.')}} din</span></li>
                  </ul>
                </div>                        
                <a href="/naplati" class="btn btn-default">ZAVRSITE KUPOVINU</a>
              </div>
            </div>
        </div><!-- .col-md-4 -->
        @endif
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .cart-main-content -->
   


@stop