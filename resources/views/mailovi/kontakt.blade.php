<!DOCTYPE html >
<html>

<head>
	<meta name="viewport" content="width=device-width" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<link rel="stylesheet" type="text/css" href="http://moa-academy.com/css/mailTemplate.css" />
	<link rel="stylesheet" type="text/css" href="http://moa-academy.com/css/bootstrap.min.css">

</head>

<body style="background-color: #777777;">

<!-- HEADER -->
<div class="container" style="padding-top:70px; padding-bottom: 70px;">

	<table class="head-wrap" bgcolor="#bb6dac">
		<tr>
			<td></td>
			<td class="header container">

				<div class="content">
					<table >
						<tr>
							<td>

							</td>

						</tr>
					</table>
				</div>

			</td>
			<td></td>
		</tr>
	</table><!-- /HEADER -->


	<!-- BODY -->
	<table class="body-wrap">
		<tr>
			<td></td>
			<td class="container" bgcolor="#FFFFFF">

				<div class="content">
					<table>
						<tr>
							<td>


								<!-- You may like to include a Hero Image -->
								<p><img src="http://moa-academy.com/images/logo.png" style="width: 200px;height:auto;" /></p>
								<!-- /Hero Image -->

								<br/>
								<h3>Poruka sa sajta <small>{{ date('d.m.Y - H:i', strtotime($vreme)) }}</small></h3>
								<h4>{{$ime_prezime}}</h4>
								<br>
								<p>{{$poruka}}</p>

								<br/>


								<hr>



								<!-- address detals -->
								<table class="columns" width="100%">
									<tr>
										<td>

											<!--- column 1 -->
											<table align="left" class="column">
												<tr>
													<td>
														<h5 class="">Podaci:</h5>
														<p class="">
															Ime i prezime: {{$ime_prezime}}<br/>
															E-mail: <a href="mailto:{{$mail}}">{{$mail}}</a><br/>
															Telefon: {{$telefon}}<br/>
															
															Info : {{$opcija}}
															<br/>
														</p>
														</p>
													</td>
												</tr>
											</table>
											<!-- /column 1 -->




											<span class="clear"></span>

										</td>
									</tr>
								</table>
								<!-- /address details -->

								<br/>

								<p style="text-align:center;">
									<a class="btn" href="mailto:{{$mail}}">Odgovorite na poruku &raquo;</a>
								</p>

								<br/>


							</td>
						</tr>
					</table>
				</div>

			</td>
			<td></td>
		</tr>
	</table>
	<!-- /BODY -->

	<!-- FOOTER -->
	<table class="footer-wrap" bgcolor="#222222">
		<tr>
			<td></td>
			<td class="container">

				<!-- content -->
				<div class="content">
					<table>
						<tr>
							<td align="center">
								<a href="/" target="_blank"><img src="http://moa-academy.com/images/logo-footer.png"  style="width:140px; height:auto" alt="/" /></a>
								<br/><br/>
								<p><strong><a href="mailto:info@moa-academy.com" style="color:#bb6dac;">info@moa-academy.com</a></strong></p>
							</td>
						</tr>
					</table>
				</div><!-- /content -->

			</td>
			<td></td>
		</tr>
	</table><!-- /FOOTER -->
</div>

</body>
</html>