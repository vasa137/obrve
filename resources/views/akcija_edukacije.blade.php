@extends('layout')

@section('title')
 - AKCIJA -20 %
@stop


@section('scriptsBottom')
  <script src='https://www.google.com/recaptcha/api.js' async defer></script>
@endsection

@section('main')
<img src="/images/moa-cover-radni.jpg" style="width: 100%">

<!-- Section-Start --> 
<section class="bg-light-alt padding-small-top">
  <div class="container">
    <div class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>MOA ACADEMY AKCIJA ! </h2>
       <img style="height:40px;" src="/images/d1.png">
      <p >
        Akcija se odnosi na sve MOA Academy edukacije. Ostvarite <strong>popust od 20% </strong>na bilo koju od edukacija iz naše ponude i postanite član MOA tima!<br>
         Svi polaznici edukacija od aprila postaju članovi <strong>MOA Academy ONLINE PLATFORME! </strong>
        <br>
        Akcija važi za uplate do 15.3.2020.
      </p>
    </div><!-- tittle -->

  </div> 
</section>
<!--  Section-End -->


<!-- Section-Start --><!-- client-say-Area-Style2  -->
<section class="client-say-area-style2 overlay dark-overlay padding-small-top">
  <div style="z-index: 4; position:relative;" class="container">
     <div  class="title text-center wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
      <h2>ŠTA JE MOA ACADEMY ONLINE PLATFORMA?</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p  > <!--style="text-align: justify; text-justify: inter-word;" -->
          Kao što smo Već rekli, svi polaznici MOA Academy edukacija u aprilu postaju članovi naše inovativne platforme koja će služiti za razmenu iskustava, komunikaciju sa Vašim mentorima, kao i dalje usavršavanje svakog od članova MOA tima.
          <br>
          Platforma će biti dostupna u vidu aplikacije za mobilne uređaje (Android i iOS).
          <br>
          Sve Vaše radove ćete moći putem jednog klika da pošaljete svim članovima MOA tima ili Vašem mentoru , koji Vam mogu dati sugestije za dalji rad.
          <br>
          Naravno, pored komunikacije putem platforme, svi polaznici MOA edukacija će imati podršku mentora kroz svakodnevnu direktnu komunikaciju.
      </p>
    </div><!-- tittle -->
  </div><!-- .container -->
</section><!-- .client-say-area-style2 -->
<!--  Section-End -->     



<section class=" padding-small-top"><!-- Fresh news -->
  <div class="container">
    <div class="title text-center  title-margin-bottom-medium wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">
      <h2>ZAKAZIVANJE</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p>Ovde možete zakazati Vaš termin edukacije ili dobiti više potrebnih informacija. </p>
    </div><!-- tittle -->
      <div class="appoinment-area">
    <div class="container">
      <div class="row">
        <div class="col-md-12 wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
          <form action="/kontaktiraj" id="kontakt_forma" method="POST">
            {{csrf_field()}}
            <div class="appoinment-contact-form">
              <div class="row">
                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="perch-user"></i></div>
                      <input type="text" class="form-control" placeholder="Ime i prezime" required name="ime_prezime">
                  </div><!-- .input-group -->
                </div><!-- .col-md-6  -->                       
                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="pe-7s-mail"></i></div>
                      <input type="email" class="form-control" placeholder="E-mail" required name="mail">
                  </div><!-- .input-group -->
                </div><!-- .col-md-6  --> 

                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="pe-7s-call"></i></div>
                      <input type="text" class="form-control" placeholder="Telefon" name="telefon">
                  </div><!-- .input-group -->
                </div><!-- .col-md-6  --> 
              </div><!-- .row -->                      

              <select name="opcija" class="select-option">
                 <option selected value="Samo se informišem">Samo se informišem</option>
                 
                 <option value="Microblading Edukacija">Microblading Edukacija</option>
                 <option value="Microneedling i Plasma Pen Edukacija">Microneedling i Plasma Pen Edukacija</option>
                 <option value="Permanent Makeup Edukacija">Permanent Makeup Edukacija</option>
                 <option value="Makeup Edukacija">Makeup Edukacija</option>

              </select>

              <div class="input-group">
                <div class="input-group-addon input-group-addon2"><i class="perch-comment-alt"></i></div>
                  <textarea class="form-control" name="poruka" placeholder="Vaša poruka..."></textarea>
              </div><!-- .input-group -->
              <div class="input-group">
                  <div style="width:100%;" class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}" id="captcha">
                    <div class="g-recaptcha" data-theme="light" data-sitekey="{{config('app.re_cap_site')}}" style="overflow:hidden; display:flex; justify-content:center;"></div>
                    @if ($errors->has('g-recaptcha-response'))
                      <br/>
                      <span class="invalid-feedback" role="alert" style="display:block;">
                                                        <strong>Morate potvrditi da niste robot.</strong>
                                                    </span>
                    @endif
                  </div>
                </div>
              <div class="row text-center">
                <div class="col-md-12">
                <input type="submit" class="btn btn-default" value="Pošalji">
                </div>
              </div>

            </div><!-- .appoinment-contact-form -->
          </form><!-- form -->

        </div>
        
      </div><!-- .row -->
    </div><!-- .container -->
  </div>



    <div class="title text-center padding-small-top title-margin-bottom-medium wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">
      <h2>MOA EDUKACIJE</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p>Ovde možete pronaći sve potrebne informacije o MOA Academy edukacijama. </p>
    </div><!-- tittle -->
    <div class="post-list">
      <div class="row">
        <div class="col-md-3 post-padding">
          <div class="blog-post-single-item bg-light-alt wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <div class="post-image-holder">
              <a href="/edukacije/microblading">
              <img src="{{asset('images/edukacije/brows-2.jpg')}}" alt="image">
                </a>
            </div>
            
            <div class="blog-post-content text-center">
              <a href="/edukacije/microblading">
                <h5 style="text-align:center;">MICROBLADING</h5>
            </a>
              
              <a href="/edukacije/microblading" class=" btn btn-default">DETALJNIJE</a>
              <br><br>
            </div>
          </div>
        </div><!-- .col-md-4 -->
        <div class="col-md-3 post-padding">
          <div class="blog-post-single-item bg-light-alt wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <div class="post-image-holder">
              <a href="/edukacije/makeup">
              <img src="{{asset('images/edukacije/makeup-2.jpg')}}" alt="image">
                </a>
            </div>
            
            <div class="blog-post-content text-center">
              <a href="/edukacije/makeup">
                <h5 style="text-align:center;">Makeup</h5>
            </a>
              
              <a href="/edukacije/makeup" class=" btn btn-default">DETALJNIJE</a>
              <br><br>
            </div>
          </div>
        </div><!-- .col-md-4 -->
        <div class="col-md-3 post-padding">
          <div class="blog-post-single-item bg-light-alt wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <div class="post-image-holder">
              <a href="/edukacije/pmu">
              <img src="{{asset('images/edukacije/pmu.jpg')}}" alt="image">
                </a>
            </div>
            
            <div class="blog-post-content text-center">
              <a href="/edukacije/pmu">
                <h5 style="text-align:center;">PERMANENT MAKEUP</h5>
            </a>
              
              <a href="/edukacije/pmu" class=" btn btn-default">DETALJNIJE</a>
              <br><br>
            </div>
          </div>
        </div><!-- .col-md-4 -->
        <div class="col-md-3 post-padding">
          <div class="blog-post-single-item bg-light-alt wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <div class="post-image-holder">
              <a href="/edukacije/microneedling">
              <img src="{{asset('images/edukacije/face-2.jpg')}}" alt="image">
                </a>
            </div>
            
            <div class="blog-post-content text-center">
              <a href="/edukacije/microneedling">
                <h5 style="text-align:center;">MICRONEEDLING</h5>
            </a>
              
              <a href="/edukacije/microneedling" class=" btn btn-default">DETALJNIJE</a>
              <br><br>
            </div>
          </div>
        </div><!-- .col-md-4 -->
      </div><!-- .news-list -->
    </div><!-- .row -->
  </div><!-- .container -->
</section>

@stop