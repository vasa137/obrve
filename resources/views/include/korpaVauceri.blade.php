<div id="tabela-vauceri">
<h5>Vaučeri</h5>

    <table class="table table-bordered cart-table">
        <thead class="text-center">
        <tr>
            <th></th>
            <th>Kod vaučera</th>
            <th>Iznos</th>
            <th> </th>
        </tr><!-- .tr -->
        </thead><!-- .thead -->
        <tbody>

        @foreach($stavkeVauceri as $stavkaVaucer)
            <tr id="stavka-vaucer-{{$stavkaVaucer->rowId}}">
                @include('include.popupDialog', ['poruka' => 'Da li ste sigurni da želite da uklonite vaučer?', 'linkUspesno' => 'javascript:ukloniVaucer(\''. $stavkaVaucer->rowId . '\',\'' . $stavkaVaucer->id .'\')', 'dialogId' => 'vaucer-' . $stavkaVaucer->rowId])

                <td>
                    <div class="content">
                        <h4 class="title">Vaučer</h4>
                    </div>
                </td>
                <td>
                    <div>{{$stavkaVaucer->name}}</div>
                </td>
                <td>
                    <div class="price">-{{number_format($stavkaVaucer->price, 2, ',', '.')}} din</div>
                </td>
                <td><a href="javascript:otvoriDialogSaId('vaucer-{!! $stavkaVaucer->rowId!!}')" class="color-text"><i class="fa fa-times"></i></a></td>
            </tr>
        @endforeach

        </tbody> <!-- .tbody -->
    </table><!-- .table -->
</div>