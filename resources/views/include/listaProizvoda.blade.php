
@foreach($proizvodi as $proizvod)
    <div class="col-md-4 col-sm-4 shoping-single-item-inner">
        <div class="shoping-single-item display-flex">
            <div class="shoping-image-holder vertical-align-middle">
                @if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike .'.jpg')))
                    <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}">
                        <img src="/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg" alt="<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>">
                    </a>
                @endif
            </div>
        </div>
        <div class="item-name-price">

            <span class="item-name">{{$proizvod->naziv}}</span>
            <h5>
            @if($proizvod->na_popustu)
                <del>{{number_format($proizvod->cena, 0, ',', '.')}} din</del> <span class="color-text">{{number_format($proizvod->cena_popust, 0, ',', '.')}} din</span>
            @else
                <span class="color-text">{{number_format($proizvod->cena, 0, ',', '.')}} din</span>
            @endif

            </h5>

        </div>
        <div class="shoping-hover">
            <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}" target="_blank">
                <span>{{$proizvod->naziv}}</span>
                @if($proizvod->na_popustu)
                    <del>{{number_format($proizvod->cena, 0, ',', '.')}} din</del> <h4 class="color-text">{{number_format($proizvod->cena_popust, 0, ',', '.')}} din</h4>
                @else
                    <h4 class="color-text">{{number_format($proizvod->cena, 0, ',', '.')}} din</h4>
                @endif

            </a>
            <br/>
            <a href="javascript:dodajUKorpu('{!! $proizvod->id !!}', '1')" class="btn btn-default">DODAJ U KORPU</a>
        </div> <!-- .shoping-hover -->
    </div><!-- .col-md-4 -->
@endforeach