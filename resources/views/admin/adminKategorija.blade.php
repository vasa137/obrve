@extends('admin.adminLayout')

@section('title')
    @if($izmena)
        Kategorija - {{$kategorija->naziv}}
    @else
        Novi kategorija
    @endif
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Admin</a>
    <a class="breadcrumb-item" href="/admin/kategorije">Kategorije</a>
    <span class="breadcrumb-item active">@if($izmena){{$kategorija->naziv}} @else Nova kategorija @endif</span>
@stop

@section('heder-h1')
@if($izmena){{$kategorija->naziv}} @else Nova kategorija @endif
@stop


@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/bootstrap-treeview.css')}}">
    <link rel="stylesheet" href="{{asset('assets/js/plugins/dropzonejs/dropzone.css')}}">
    <script src="{{asset('js/adminKategorija.js')}}"></script>
@stop

@section('scriptsBottom')
    <script src="{{asset('js/bootstrap-treeview.js')}}"></script>
    <script src="{{asset('js/izmenaKategorija.js')}}"></script>
    <script src="{{asset('assets/js/plugins/dropzonejs/dropzone.js')}}"></script>
    <script src="{{asset('js/dropzoneSlikeKategorije.js')}}"></script>

    @if($izmena and $kategorija->id_nad_kategorija != null)
        <script>inicijalizujKategorije('{!! addslashes(json_encode($stabloKategorija)) !!}', '{{$kategorija->id}}', '{{$kategorija->id_nad_kategorija}}');</script>
    @elseif($izmena)
        <script>inicijalizujKategorije('{!! addslashes(json_encode($stabloKategorija)) !!}', '{{$kategorija->id}}');</script>
    @else
        <script>inicijalizujKategorije('{!! addslashes(json_encode($stabloKategorija)) !!}');</script>
    @endif


@stop

@section('main')
    <div class="row gutters-tiny">
    @if($izmena)

        <!-- In Orders -->
            <div class="col-md-3 col-xl-3">
                <a class="block block-rounded block-link-shadow" >
                    <div class="block-content block-content-full block-sticky-options">
                        <div class="block-options">
                            <div class="block-options-item">
                                <i class="fa fa-shopping-basket fa-2x text-info"></i>
                            </div>
                        </div>
                        <div class="py-20 text-center">
                            <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{$kategorija->broj_proizvoda}}">0</div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">Različitih proizvoda</div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- END In Orders -->

            <!-- Stock -->
            <div class="col-md-3 col-xl-3">
                <a class="block block-rounded block-link-shadow" >
                    <div class="block-content block-content-full block-sticky-options">
                        <div class="block-options">
                            <div class="block-options-item">
                                <i class="si si-social-dropbox fa-2x text-warning"></i>
                            </div>
                        </div>
                        <div class="py-20 text-center">
                            <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{$kategorija->broj_brendova}}">0</div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">Različitih brendova</div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- END Stock -->
    @endif
    <!-- Stock -->
        <div class="col-md-3 col-xl-3">

            <a class="block block-rounded block-link-shadow" href="javascript:$('#forma-kategorija-submit-button').click()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="si si-settings fa-2x text-success"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Sačuvaj</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Stock -->

    @if($izmena)
        @if(!$kategorija->sakriven)
            <!-- Delete Product -->

                    <div class="col-md-3 col-xl-3">
                        @if(!$kategorija->ima_decu)
                        <form id="forma-obrisi-kategoriju" method="POST" action="/admin/obrisiKategoriju/{{$kategorija->id}}">
                            {{csrf_field()}}
                            <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-obrisi-kategoriju').submit();">
                                <div class="block-content block-content-full block-sticky-options">
                                    <div class="block-options">
                                        <div class="block-options-item">
                                            <i class="fa fa-trash fa-2x text-danger"></i>
                                        </div>
                                    </div>
                                    <div class="py-20 text-center">
                                        <div class="font-size-h2 font-w700 mb-0 text-danger">
                                            <i class="fa fa-times"></i>
                                        </div>
                                        <div class="font-size-sm font-w600 text-uppercase text-muted">Obriši kategoriju</div>
                                    </div>
                                </div>
                            </a>
                        </form>
                        @else
                            <div class="py-20 text-center">
                            <div class="font-size-sm font-w600 text-uppercase text-danger">Ova kategorija ima podkategorije, te nije moguće brisanje.</div>
                            </div>
                        @endif
                    </div>



            @else
                <div class="col-md-3 col-xl-3">
                    <form id="forma-restauriraj-kategoriju" method="POST" action="/admin/restaurirajKategoriju/{{$kategorija->id}}">
                        {{csrf_field()}}
                        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-restauriraj-kategoriju').submit();">
                            <div class="block-content block-content-full block-sticky-options">
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <i class="fa fa-lightbulb-o fa-2x text-warning"></i>
                                    </div>
                                </div>
                                <div class="py-20 text-center">
                                    <div class="font-size-h2 font-w700 mb-0 text-warning">
                                        <i class="fa fa-undo"></i>
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Restauriraj kategoriju</div>
                                </div>
                            </div>
                        </a>
                    </form>
                </div>
        @endif
    @endif
    <!-- END Delete Product -->
    </div>
    <!-- END Overview -->
    <form id="forma-kategorija" method="POST" @if($izmena) action="/admin/sacuvajKategoriju/{{$kategorija->id}}" @else action="/admin/sacuvajKategoriju/-1" @endif onsubmit="return formaKategorijaPoslata()">
    {{csrf_field()}}
    <!-- Update Product -->
        <h2 class="content-heading">Informacije o kategoriji</h2>
        <div class="row gutters-tiny">
            <!-- Basic Info -->
            <div class="col-md-7">
                <div class="block block-rounded block-themed">
                    <div class="block-header bg-gd-primary">
                        <h3 class="block-title">Informacije</h3>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="form-group row">
                            <label class="col-12" >Naziv</label>
                            <div class="col-12 input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="si si-info"></i>
                                </span>
                                </div>
                                <input id="naziv" oninput="promeniTekstUStablu()" maxlength="254" type="text" class="form-control" name="naziv" @if($izmena) value="{{$kategorija->naziv}}" @endif required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12">Opis</label>
                            <div class="col-12">
                                <!-- CKEditor (js-ckeditor id is initialized in Codebase() -> uiHelperCkeditor()) -->
                                <!-- For more info and examples you can check out http://ckeditor.com -->
                                <textarea maxlength="9999" class="form-control" name="opis" rows="8">@if($izmena){{$kategorija->opis}}@endif</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" for="example-select2">Prioritet</label>
                            <div class="col-12">
                                <!-- Select2 (.js-select2 class is initialized in Codebase() -> uiHelperSelect2()) -->
                                <!-- For more info and examples you can check out https://github.com/select2/select2 -->

                                <select id="prioritetSelect" class="js-select2 form-control" name="prioritet" style="width: 100%;" data-placeholder="Choose one..">
                                    <?php for($i = 0; $i < 30; $i++){ ?>
                                        <option value="{{$i}}" @if($izmena and $kategorija->prioritet == $i) selected @endif>{{$i}}</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>


                    </div>
                    <!-- Meta Data -->
                    <div class="block block-rounded block-themed">
                        <div class="block-header bg-gd-primary">
                            <h3 class="block-title">Meta Data</h3>

                        </div>
                        <div class="block-content">
                            <!-- Bootstrap Maxlength (.js-maxlength class is initialized in Codebase() -> uiHelperMaxlength()) -->
                            <!-- For more info and examples you can check out https://github.com/mimo84/bootstrap-maxlength -->
                            <div class="form-group row">
                                <label class="col-12">Title</label>
                                <div class="col-12">
                                    <input type="text" class="js-maxlength form-control" name="meta_title" maxlength="254" data-always-show="true" data-placement="top" @if($izmena) value="{{$kategorija->meta_title}}" @endif>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12">Description</label>
                                <div class="col-12">
                                    <textarea class="js-maxlength form-control" name="meta_desc" maxlength="999" data-always-show="true" data-placement="top" rows="3">@if($izmena){{$kategorija->meta_desc}}@endif</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Meta Data -->

                </div>
            </div>

            <!-- END Basic Info -->

            <!-- More Options -->
            <div class="col-md-5">
                <!-- Status -->
                <div class="block block-rounded block-themed">
                    <div class="block-header bg-gd-primary">
                        <h3 class="block-title">Slika</h3>

                    </div>

                @if($izmena and File::exists(public_path('/images/kategorije/temp/glavna.jpg')))
                    <!-- Existing Images -->
                        <div id="slike-glavna.jpg" class="row gutters-tiny items-push">
                            <div class="col-sm-12 col-xl-12">
                                <div class="options-container">
                                    <img class="img-fluid options-item" src="{{asset('images/kategorije/temp/glavna.jpg')}}" alt="">
                                    <div class="options-overlay bg-black-op-75">
                                        <div class="options-overlay-content">
                                            <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" href="javascript: obrisiSliku('glavna.jpg');">
                                                <i class="fa fa-times"></i> Obriši
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Existing Images -->
                    @endif
                    <div class="dropzone" id="glavna-dropzone-div" name="mainFileUploader">
                        <input type="hidden" id="glavna-dropzone" name="glavna" value="0"/>
                        <div class="fallback">
                            <input name="file" type="file" />
                        </div>
                    </div>



                </div>


                <div class="block block-rounded block-themed">
                    <div class="block-header bg-gd-primary">
                        <h3 class="block-title">Roditeljska kategorija</h3>
                    </div>
                    <div class="block-content block-content-full row">
                        <div class="col-sm-12">
                            @if(!$izmena or !$kategorija->sakriven)
                                <div id="treeview-selectable"></div>
                            @else
                                <div class="font-size-sm font-w600 text-uppercase text-primary text-center">Moguće je postaviti roditeljsku kategoriju tek nakon restauracije.</div>
                            @endif
                        </div>
                    </div>
                </div>



            </div>


        </div>
        <!-- END More Options -->

        <!-- END Update Product -->
        <input type="submit" id="forma-kategorija-submit-button" style="display:none"/>
    </form>
@stop