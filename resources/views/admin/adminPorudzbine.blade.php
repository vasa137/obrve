@extends('admin.adminLayout')

@section('title')
Porudžbine
@stop

@section('breadcrumbs')
<a class="breadcrumb-item" href="/">Admin</a>
<span class="breadcrumb-item active">Porudžbine</span>
@stop

@section('heder-h1')
Porudžbine
@stop


@section('heder-h2')
Trenutno imate<a class="text-primary-light link-effect"> {{$brojNovih}} novih porudžbina</a>.
@stop

@section('scriptsTop')
    <script src="{{asset('/js/adminPorudzbine.js')}}"></script>
@endsection
@section('scriptsBottom')
    <!-- Page JS Code -->
    <script src="{{asset('/js/tabelaPorudzbine.js')}}"></script>
@endsection

@section('main')
<div class="row gutters-tiny">
    <!-- Top Sellers -->
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-link-shadow" href="javascript:pretraga('nova')">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-warning fa-2x text-warning"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{$brojNovih}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">NOVIH</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Top Sellers -->
    <!-- All Products -->
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-link-shadow" href="javascript:pretraga('poslata')">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-truck fa-2x text-info"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{$brojPoslatih}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">POSLATIH</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END All Products -->
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-link-shadow" href="javascript:pretraga('kompletirana')">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="si si-check fa-2x text-success"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-success" data-toggle="countTo" data-to="{{$brojKompletiranih}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-success">KOMPLETIRANIH</div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-link-shadow"  href="javascript:pretraga('stornirana')">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-times fa-2x text-danger"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-danger" data-toggle="countTo" data-to="{{$brojStorniranih}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-danger">STORNIRANIH</div>
                </div>
            </div>
        </a>
    </div>

<!--
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-link-shadow">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="si si-basket fa-2x text-muted"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-muted" data-toggle="countTo" data-to="{{$ovogMeseca}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Porudžbina ovog meseca</div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-link-shadow">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-money fa-2x text-muted-light"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-muted" data-toggle="countTo" data-to="{{$prometMeseca}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Promet ovog meseca</div>
                </div>
            </div>
        </a>
    </div>

    -->
</div>
<!-- END Overview -->

<!-- Dynamic Table Full Pagination -->
<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Porudžbine</h3>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
        <table id="tabela-porudzbina" class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
                <tr>
                    <th class="text-center">Broj</th>
                    <th class="text-center">Datum</th>
                    <th class="d-none d-sm-table-cell">Ime i Prezime</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">E-Mail</th>
                    <th class="text-center" style="width: 15%;">Status</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Iznos</th>
                    <th class="text-center" style="width: 20%;">Akcija</th>
                </tr>
            </thead>
            <tbody>
                @foreach($porudzbine as $porudzbina)
                    <tr>
                        <td class="text-center">{{$porudzbina->id}}</td>
                        <td class="text-center">{{date_format($porudzbina->created_at, "Y-m-d H:i")}}</td>
                        <td class="font-w600">@if($porudzbina->id_user != null ) <a href="/admin/korisnik/{{$porudzbina->id_user}}">{{$porudzbina->kupac}}</a> @else {{$porudzbina->kupac}} @endif</td>
                        <td class="d-none d-sm-table-cell">{{$porudzbina->email}}</td>

                        <td class="d-none d-sm-table-cell">
                            @if($porudzbina->status == 'nova')
                                <span class="badge badge-warning">Nova</span>
                            @elseif($porudzbina->status == 'poslata')
                                <span class="badge badge-primary">Poslata</span>
                            @elseif($porudzbina->status == 'stornirana')
                                <span class="badge badge-danger">Stornirana</span>
                            @elseif($porudzbina->status == 'kompletirana')
                                <span class="badge badge-success">Kompletirana</span>
                            @endif
                        </td>
                        <td class="font-w600">{{number_format($porudzbina->iznos_popust, 2, ',', '.')}}</td>
                        <td class="text-center">
                            <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Pregled porudžbine" href="/admin/porudzbina/{{$porudzbina->id}}">
                                <i class="si si-basket-loaded"></i>
                            </a>

                            <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Faktura" href="/admin/faktura/{{$porudzbina->id}}">
                                <i class="fa fa-file"></i>
                            </a>

                            <form method="POST" style="display: inline;">
                                {{csrf_field()}}
                                @if($porudzbina->dostava and $porudzbina->status != 'poslata')
                                    <button type="submit" class="text-primary btn btn-sm btn-secondary" data-toggle="tooltip" title="Pošalji" formaction="/admin/statusPorudzbine/{{$porudzbina->id}}/poslata">
                                        <i class="fa fa-truck"></i>
                                    </button>
                                @endif

                                @if($porudzbina->status != 'kompletirana')
                                    <button type="submit" class="text-success btn btn-sm btn-secondary" data-toggle="tooltip" title="Kompletiraj - naplaćeno" formaction="/admin/statusPorudzbine/{{$porudzbina->id}}/kompletirana">
                                        <i class="fa fa-check "></i>
                                    </button>
                                @endif

                                @if($porudzbina->status != 'stornirana')
                                    <button type="submit" class="text-danger btn btn-sm btn-secondary" data-toggle="tooltip" title="Storniraj" formaction="/admin/statusPorudzbine/{{$porudzbina->id}}/stornirana">
                                        <i class="fa fa-times"></i>
                                    </button>
                                @endif
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END Dynamic Table Full Pagination -->
@stop