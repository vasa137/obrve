@extends('admin.adminLayout')

@section('title')
    @if($izmena)
        Brend - {{$brend->naziv}}
    @else
        Novi brend
    @endif
@stop

@section('heder-h1')
@if($izmena){{$brend->naziv}} @else Novi brend @endif
@stop





@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Admin</a>
    <a class="breadcrumb-item" href="/admin/brendovi">Brendovi</a>
    <span class="breadcrumb-item active">@if($izmena){{$brend->naziv}} @else Novi brend @endif</span>
@stop

@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('assets/js/plugins/dropzonejs/dropzone.css')}}">
    <script src="{{asset('js/adminBrend.js')}}"></script>
@stop

@section('scriptsBottom')
    <script src="{{asset('assets/js/plugins/dropzonejs/dropzone.js')}}"></script>
    <script src="{{asset('js/dropzoneSlikeBrendovi.js')}}"></script>
@stop

@section('main')
    <div class="row gutters-tiny">
    @if($izmena)

        <!-- In Orders -->
            <div class="col-md-3 col-xl-3">
                <a class="block block-rounded block-link-shadow" >
                    <div class="block-content block-content-full block-sticky-options">
                        <div class="block-options">
                            <div class="block-options-item">
                                <i class="fa fa-shopping-basket fa-2x text-info"></i>
                            </div>
                        </div>
                        <div class="py-20 text-center">
                            <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{$brend->broj_proizvoda}}">0</div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">Različitih proizvoda</div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- END In Orders -->

            <!-- Stock -->
            <div class="col-md-3 col-xl-3">
                <a class="block block-rounded block-link-shadow" >
                    <div class="block-content block-content-full block-sticky-options">
                        <div class="block-options">
                            <div class="block-options-item">
                                <i class="si si-social-dropbox fa-2x text-warning"></i>
                            </div>
                        </div>
                        <div class="py-20 text-center">
                            <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{$brend->broj_kategorija}}">0</div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">Različitih kategorija</div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- END Stock -->
    @endif
    <!-- Stock -->
        <div class="col-md-3 col-xl-3">

            <a class="block block-rounded block-link-shadow" href="javascript:$('#forma-brend-submit-button').click()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="si si-settings fa-2x text-success"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Sačuvaj</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Stock -->

    @if($izmena)
        @if(!$brend->sakriven)
            <!-- Delete Product -->
                <div class="col-md-3 col-xl-3">
                    <form id="forma-obrisi-brend" method="POST" action="/admin/obrisiBrend/{{$brend->id}}">
                        {{csrf_field()}}
                        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-obrisi-brend').submit();">
                            <div class="block-content block-content-full block-sticky-options">
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <i class="fa fa-trash fa-2x text-danger"></i>
                                    </div>
                                </div>
                                <div class="py-20 text-center">
                                    <div class="font-size-h2 font-w700 mb-0 text-danger">
                                        <i class="fa fa-times"></i>
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Obriši brend</div>
                                </div>
                            </div>
                        </a>
                    </form>
                </div>

            @else
                <div class="col-md-3 col-xl-3">
                    <form id="forma-restauriraj-brend" method="POST" action="/admin/restaurirajBrend/{{$brend->id}}">
                        {{csrf_field()}}
                        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-restauriraj-brend').submit();">
                            <div class="block-content block-content-full block-sticky-options">
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <i class="fa fa-lightbulb-o fa-2x text-warning"></i>
                                    </div>
                                </div>
                                <div class="py-20 text-center">
                                    <div class="font-size-h2 font-w700 mb-0 text-warning">
                                        <i class="fa fa-undo"></i>
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Restauriraj brend</div>
                                </div>
                            </div>
                        </a>
                    </form>
                </div>
        @endif
    @endif
    <!-- END Delete Product -->
    </div>
    <!-- END Overview -->
    <form id="forma-brend" method="POST" @if($izmena) action="/admin/sacuvajBrend/{{$brend->id}}" @else action="/admin/sacuvajBrend/-1" @endif>
    {{csrf_field()}}
    <!-- Update Product -->
        <h2 class="content-heading">Informacije o brendu</h2>
        <div class="row gutters-tiny">
            <!-- Basic Info -->
            <div class="col-md-7">
                <div class="block block-rounded block-themed">
                    <div class="block-header bg-gd-primary">
                        <h3 class="block-title">Informacije</h3>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="form-group row">
                            <label class="col-12" >Naziv</label>
                            <div class="col-12 input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="si si-info"></i>
                                </span>
                                </div>
                                <input maxlength="254" type="text" class="form-control" name="naziv" @if($izmena) value="{{$brend->naziv}}" @endif required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12">Opis</label>
                            <div class="col-12">
                                <!-- CKEditor (js-ckeditor id is initialized in Codebase() -> uiHelperCkeditor()) -->
                                <!-- For more info and examples you can check out http://ckeditor.com -->
                                <textarea maxlength="15999" class="form-control" name="opis" rows="8">@if($izmena){{$brend->opis}}@endif</textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- END Basic Info -->

            <!-- More Options -->
            <div class="col-md-5">
                <!-- Status -->
                <div class="block block-rounded block-themed">
                    <div class="block-header bg-gd-primary">
                        <h3 class="block-title">Slika</h3>

                    </div>

                    @if($izmena and File::exists(public_path('/images/brendovi/temp/glavna.jpg')))
                        <!-- Existing Images -->
                        <div id="slike-glavna.jpg" class="row gutters-tiny items-push">
                            <div class="col-sm-12 col-xl-12">
                                <div class="options-container">
                                    <img class="img-fluid options-item" src="{{asset('images/brendovi/temp/glavna.jpg')}}" alt="">
                                    <div class="options-overlay bg-black-op-75">
                                        <div class="options-overlay-content">
                                            <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" href="javascript: obrisiSliku('glavna.jpg');">
                                                <i class="fa fa-times"></i> Obriši
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Existing Images -->
                    @endif
                    <div class="dropzone" id="glavna-dropzone-div" name="mainFileUploader">
                        <input type="hidden" id="glavna-dropzone" name="glavna" value="0"/>
                        <div class="fallback">
                            <input name="file" type="file" />
                        </div>
                    </div>



                </div>
            </div>


        </div>
            <!-- END More Options -->

        <!-- END Update Product -->
        <input type="submit" id="forma-brend-submit-button" style="display:none"/>
    </form>
@stop