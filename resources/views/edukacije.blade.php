@extends('layout')

@section('title')
- Edukacije
@stop

@section('main')
<div class="about-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="heading-table">
          <div class="heading-table-cell">
            <h2 class="heading-text">EDUKACIJE</h2>   
             <div class="header-menu">
               <ul class="list-inline">
                  <li ><a href="/">NASLOVNA /</a></li>
                  <li><span class="active">EDUKACIJE</span></li>
               </ul>
             </div>
            </div>
          </div>
        </div><!-- col-md-12 -->
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .about-header -->


<!-- Section-Start --> 
<section class="padding-large-top-bottom"><!-- Fresh news -->
  <div class="container">
    <div class="title text-center title-margin-bottom-medium wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">
      <h2>MOA EDUKACIJE</h2>
       <img style="height:40px;" src="/images/d1.png">
      <p>Iskoristite priliku i postanite deo MOA tima! Izaberite neku od naših edukacija i postanite sertifikovani profesionalac i deo MOA Academy tima! </p>
    </div><!-- tittle -->
    <div class="post-list">
      <div class="row">
        <div class="col-md-4 post-padding">
          <div class="blog-post-single-item bg-light-alt wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <div class="post-image-holder">
            	<a href="/edukacije/microblading">
              <img src="{{asset('images/edukacije/brows-2.jpg')}}" alt="image">
              	</a>
            </div>
            
            <div class="blog-post-content text-center">
            	<a href="/edukacije/microblading">
              	<h5 style="text-align:center;">MICROBLADING</h5>
         		</a>
              <p>Microblading je vestina, koja se stice samom vezbom. Tako da je najbitnije tokom prvog dana da se savlada kako da se pravilno vezba. Tokom cele obuke, ucimo kako se izvodi tetman... </p>
              <a href="/edukacije/microblading" class=" btn btn-default">DETALJNIJE</a>
              <br><br>
            </div>
          </div>
        </div><!-- .col-md-4 -->
        <div class="col-md-4 post-padding">
          <div class="blog-post-single-item bg-light-alt wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <div class="post-image-holder">
            	<a href="/edukacije/makeup">
              <img src="{{asset('images/edukacije/makeup-2.jpg')}}" alt="image">
              	</a>
            </div>
            
            <div class="blog-post-content text-center">
            	<a href="/edukacije/makeup">
              	<h5 style="text-align:center;">Make up</h5>
         		</a>
              <p>Obuhvata kompletnu šminkersku edukaciju u vidu bazne obuke za sve koji žele da postanu priznati profesionalni šminkeri kao i različite tipove obuka za usavršavanje...</p>
              <a href="/edukacije/makeup" class=" btn btn-default">DETALJNIJE</a>
              <br><br>
            </div>
          </div>
        </div><!-- .col-md-4 -->
        <div class="col-md-4 post-padding">
          <div class="blog-post-single-item bg-light-alt wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <div class="post-image-holder">
            	<a href="/edukacije/pmu">
              <img src="{{asset('images/edukacije/pmu.jpg')}}" alt="image">
              	</a>
            </div>
            
            <div class="blog-post-content text-center">
            	<a href="/edukacije/pmu">
              	<h5 style="text-align:center;">PERMANENT MAKE UP</h5>
         		</a>
              <p>U svoje obuke implementiramo inovacije iz sveta trajne šminke, a kroz interaktivan rad omogućavamo svakom polazniku siguran ulazak u biznis sektor ove grane estetike...</p>
              <a href="/edukacije/pmu" class=" btn btn-default">DETALJNIJE</a>
              <br><br>
            </div>
          </div>
        </div><!-- .col-md-4 -->
        <div class="col-md-4 post-padding">
          <div class="blog-post-single-item bg-light-alt wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".25s">
            <div class="post-image-holder">
            	<a href="/edukacije/microneedling">
              <img src="{{asset('images/edukacije/face-2.jpg')}}" alt="image">
              	</a>
            </div>
            
            <div class="blog-post-content text-center">
            	<a href="/edukacije/microneedling">
              	<h5 style="text-align:center;">MICRONEEDLING</h5>
         		</a>
              <p>Tretman podstiče proizvodnju kolagena i povećava prokrvljenost kože čime se postižu rezultati poboljšanja izgleda kože i tonusa kože te ona postaje zdravija i sjajnija...</p>
              <a href="/edukacije/microneedling" class=" btn btn-default">DETALJNIJE</a>
              <br><br>
            </div>
          </div>
        </div><!-- .col-md-4 -->
      </div><!-- .news-list -->
    </div><!-- .row -->
  </div><!-- .container -->
</section>
<!--  Section-End -->
@stop