@extends('layout')

@section('scriptsTop')
  <link href="{{asset('css/slick-lightbox.css')}}" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('css/popupDialog.css')}}"/>
@endsection
@section('scriptsBottom')
  <script src="{{asset('js/slick-lightbox.js')}}"></script>
  <script src="{{asset('js/klijentKorpa.js')}}"></script>
  <script src="{{asset('js/popupDialog.js')}}"></script>
  <script src="{{asset('js/klijentProizvod.js')}}"></script>
  <script>
    inicijalizujSlike();
  </script>
@endsection

@section('main')
@include('include.popupDialog', ['poruka' => 'Uspešno ste dodali proizvod u korpu. Želite li da pregledate korpu?', 'linkUspesno' => '/korpa' ,'yesButtonText' => 'Da' ,'noButtonText' => 'Ne'])

  <div class="about-header">
<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <div class="heading-table">
        <div class="heading-table-cell">
          <h2 class="heading-text">{{$proizvod->naziv}}</h2>
           <div class="header-menu">
             <ul class="list-inline">
                <li ><a href="/">NASLOVNA /</a></li>
                 <li><span class="active">{{$proizvod->naziv}}</span></li>
             </ul>
           </div>
          </div>
        </div>
      </div><!-- col-md-12 -->
    </div><!-- .row -->
  </div><!-- .container -->
</div><!-- .about-header -->

<div class="shop-main-content  shop-single-main-content padding-large-top"><!-- .shop-main-content Start-->

<div class="container">
 <div class="row">
   <div class="col-md-12 content-wrap wow fadeIn" data-wow-duration=".5s" data-wow-delay=".5s">
    <div class="hair-dryer">
      <div class="row">
        <div class="product-details-content-area">
        <div class="col-md-5 col-sm-5">
          <div class="hair-dryer-image">
            <div class="hair-dryer-image-holder">
              <div class="product-details-slider slider-for" id="product-details-slider" data-slider-id="1">
                @if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')))
                  <div class="single-product-thumb">
                    <a href="/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg"><img class="img-fluid" src="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')}}" alt="{{$proizvod->nazivGlavneSlike}}"></a>
                  </div>
                @endif
                @foreach($proizvod->sveSlike as $slika)
                  <div class="single-product-thumb">
                    <a href="/images/proizvodi/{{$proizvod->id}}/sveSlike/{{$slika}}"><img class="img-fluid" src="{{asset('images/proizvodi/'. $proizvod->id .'/sveSlike/' . $slika)}}" alt="{{$proizvod->naziv}}"></a>
                  </div>
                @endforeach
              </div>
              <br/>



            </div>
          </div>
          <br/>
          <ul class="product-deails-thumb" data-slider-id="1">
            @if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')))
              <li class="owl-thumb-item">
                <img class="img-fluid" src="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')}}" alt="{{$proizvod->nazivGlavneSlike}}">
              </li>
            @endif
            @foreach($proizvod->sveSlike as $slika)
              <li class="owl-thumb-item">
                <img class="img-fluid" src="{{asset('images/proizvodi/'. $proizvod->id .'/sveSlike/' . $slika)}}" alt="{{$proizvod->naziv}}">
              </li>
            @endforeach
          </ul>
        </div><!-- .col-md-5 -->
        <div class="col-md-7 col-sm-7">
          <div class="hair-dryer-description">            
            <h4>{{$proizvod->naziv}}</h4>
            <span class=" hair-dryer-price color-text">
              @if($proizvod->na_popustu)
                <del style="color: #333333;">{{number_format($proizvod->cena, 0, ',', '.')}} din </del>
                {{number_format($proizvod->cena_popust, 0, ',', '.')}} din
              @else
                {{number_format($proizvod->cena, 0, ',', '.')}} din
              @endif
            </span>
            <p>{{$proizvod->kratak_opis}}</p>
            <ul class="quantity list-inline">
              <li>KOLICINA:</li>
              <li><i style="cursor:pointer;" class="qtminus pe-7s-less"></i></li>
              <li class="qttotal" id="kolicina">1</li>
              <li><i style="cursor:pointer;" class="qtplus pe-7s-plus"></i></li>
            </ul>
            <ul class="quantity list-inline">
              <li>KATEGORIJE:</li>
              @foreach($proizvod->kategorije as $kategorija)
                <span class="cat">{{$kategorija->naziv}}</span>
                @if($kategorija != $proizvod->kategorije[count($proizvod->kategorije) - 1])
                  |
                @endif
              @endforeach
            </ul>
            <div class="hair-add-to-cart clearfix">
              <a href="javascript:dodajUKorpu('{!! $proizvod->id !!}', $('#kolicina').html())" class="btn btn-default"> <i class="fa fa-cart-arrow-down"></i>DODAJ U KORPU</a>
              <!--
              <ul class="list-inline">
                <li><a href="#"> <i class="fa fa-envelope-o"></i></a></li>
                <li><a href="#"> <i class="fa fa-heart-o"></i></a></li>
              </ul>
              -->
            </div>
          </div>
        </div><!-- .col-md-7 -->
      </div><!-- .hair-dryer   -->
      </div>
    </div><!-- .row -->
    <div class="hair-dryer-tab">
      <ul class="list-inline hair-dryer-tab-list" role="tablist">
        <li class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">OPIS</a></li>
        <li><a href="#product-info" aria-controls="product-info" role="tab" data-toggle="tab">SPECIFIKACIJE</a></li>   
    	</ul><!-- .hair-dryer-tab-list -->
      
       <div class="tab-content">
          <div role="tabpanel" class="hair-dryer-tab-content tab-pane active" id="overview">
            <p>{{$proizvod->opis}}</p>
          </div><!-- .tabpanel -->
          <div role="tabpanel" class="hair-dryer-tab-content tab-pane" id="product-info">
            <div class="row">
              @if($proizvod->ima_specifikacije)
              <div class="col-md-2 col-sm-2 col-xs-5">
                <ul class="text-uppercase">
                    <?php for($i = 0; $i < count($proizvod->specifikacije); $i++) { ?>
                      <li>{{$proizvod->specifikacije[$i]->naziv}}:</li>
                    <?php } ?>
                </ul>
              </div>  <!-- .col-md-2   -->                    
              <div class="col-md-10 col-sm-10 col-xs-7">
                <ul class="text-uppercase">
                  <?php for($i = 0; $i < count($proizvod->specifikacije); $i++) { ?>
                  <li>{{$proizvod->specifikacije_tekst[$i]}}</li>
                  <?php } ?>
                </ul>
              </div><!-- .col-md-10 -->
              @endif
            </div><!-- .row -->

          </div><!-- .tabpanel -->
      </div><!-- tab-content -->
    </div> <!-- .hair-dryer-tab         --> 
    @if(count($slicniProizvodi) > 0)
    <h2>SLICNI PROIZVODI</h2>
    <div class="saparator1"></div>
    <div class="row">
      @foreach($slicniProizvodi as $proizvod)
        <div class="col-md-4 col-sm-4 shoping-single-item-inner">
          <div class="shoping-single-item display-flex">
            <div class="shoping-image-holder vertical-align-middle">
              @if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike .'.jpg')))
                <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}">
                  <img src="/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg" alt="<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>">
                </a>
              @endif
            </div>
          </div>
          <div class="item-name-price">

            <span class="item-name">{{$proizvod->naziv}}</span>
            <h5>
              @if($proizvod->na_popustu)
                <del>{{number_format($proizvod->cena, 0, ',', '.')}} din</del> <span class="color-text">{{number_format($proizvod->cena_popust, 0, ',', '.')}} din</span>
              @else
                <span class="color-text">{{number_format($proizvod->cena, 0, ',', '.')}} din</span>
              @endif

            </h5>

          </div>
          <div class="shoping-hover">
            <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}" target="_blank">
              <span>{{$proizvod->naziv}}</span>
              @if($proizvod->na_popustu)
                <del>{{number_format($proizvod->cena, 0, ',', '.')}} din</del> <h4 class="color-text">{{number_format($proizvod->cena_popust, 0, ',', '.')}} din</h4>
              @else
                <h4 class="color-text">{{number_format($proizvod->cena, 0, ',', '.')}} din</h4>
              @endif

            </a>
            <br/>
            <a href="javascript:dodajUKorpu('{!! $proizvod->id !!}', '1')" class="btn btn-default">DODAJ U KORPU</a>
          </div> <!-- .shoping-hover -->
        </div><!-- .col-md-4 -->
      @endforeach
      </div><!-- .row -->
    @endif
   </div><!-- .col-md-9 .content-wrap -->
 </div><!-- .row -->
</div><!-- .container -->
</div> <!-- .shop-main-content End--> 

          
@stop