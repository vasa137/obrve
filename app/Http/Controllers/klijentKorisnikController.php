<?php

namespace App\Http\Controllers;

use App\Kupon;
use App\Porudzbina;
use App\PorudzbinaVaucer;
use App\Proizvod;
use App\StavkaPorudzbina;
use App\Vaucer;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use Hash;
use Session;
use Carbon\Carbon;
use Mail;
use App\Traits\CaptchaTrait;
use Illuminate\Support\Facades\Validator;
class klijentKorisnikController extends Controller
{
    use CaptchaTrait;
    private function formirajPorudzbinu($porudzbina){
        $id = $porudzbina->id;

        $porudzbina->stavke = StavkaPorudzbina::dohvatiStavkeZaPorudzbinu($id);

        if($porudzbina->ima_vaucere){
            $porudzbinaVauceri = PorudzbinaVaucer::dohvatiVaucereZaPorudzbinu($id);

            $vauceri = [];

            foreach($porudzbinaVauceri as $porudzbinaVaucer){
                $vauceri [] = Vaucer::dohvatiSaId($porudzbinaVaucer->id_vaucer);
            }

            $porudzbina->vauceri = $vauceri;
        }

        foreach($porudzbina->stavke as $stavka){
            $stavka->proizvod = Proizvod::dohvatiSaId($stavka->id_proizvod);

            if($stavka->id_kupon != null){
                $stavka->kupon = Kupon::dohvatiSaId($stavka->id_kupon);
            }
        }

        return $porudzbina;
    }
    public function nalog(){
        if(Auth::check()){
            $user = Auth::user();

            if($user->admin){
                return redirect('/admin');
            } else{
                $user = Auth::user();

                $porudzbine = Porudzbina::dohvatiZaKupca($user->id);

                foreach($porudzbine as $porudzbina){
                    $this->formirajPorudzbinu($porudzbina);
                }

                return view('nalog', compact('user', 'porudzbine'));
            }
        } else{
            return redirect('/login');
        }
    }


    public function kontaktiraj(Request $request)
    {
        //$request['captcha'] = $this->captchaCheck();

        $request->validate([
            'ime_prezime' => 'required|string|max:254',
            'telefon' => 'required|string|max:19',
            'mail' => 'required|email|max:254',
            'g-recaptcha-response' => 'required',
            'captcha' => 'required|min:1',
        ]);


        

        $ime_prezime = $_POST['ime_prezime'];
        $telefon = $_POST['telefon'];
        $mail = $_POST['mail'];
        $poruka = $_POST['poruka'];
        //$datum = $_POST['datum'];
        $opcija = $_POST['opcija'];
        $vreme = Carbon::now();

        $vreme->tz='Europe/Belgrade';


         $data =[
           'ime_prezime' => $ime_prezime,
           'telefon' =>  $telefon,
           'poruka' => $poruka,
           'mail' => $mail,
           'vreme' => $vreme,
           //'datum' => $datum,
           'opcija' => $opcija
        ];


  //dd($data);
        //-----------------slanje maila-----------
        
         Mail::send('mailovi.kontakt', $data, function($message) use ($ime_prezime,$mail) {
         $message->to('info@moa-academy.com', 'Poruka sa sajta- '.$ime_prezime)->subject('Poruka sa sajta- '.$ime_prezime);
         $message->from('office@moa-academy.com' ,$ime_prezime.' - Poruka sa sajta');
        });
	
        //dd($data);


        //return view('mailovi.kontakt',compact('vreme','mail','poruka','telefon','ime_prezime','datum','opcija'));

     return redirect('/kontakt-uspesan');
    }


    public function promeni_licne_podatke(Request $request){
        $request->validate([
            'ime_prezime' => 'required|string|max:254',
            'telefon' => 'nullable|string|max:19',
            'grad' => 'nullable|string|max:254',
            'adresa' => 'nullable|string|max:254',
            'zip' => 'nullable|numeric|min:10000|max:99999',
        ]);

        $ime_prezime = $_POST['ime_prezime'];
        $telefon = $_POST['telefon'];
        $grad = $_POST['grad'];
        $adresa = $_POST['adresa'];
        $zip = $_POST['zip'];

        $user = Auth::user();

        $user->promeniLicnePodatke($ime_prezime, $telefon, $grad, $adresa, $zip);

        Session::flash('success', 'Uspešno ste promenili lične podatke.');
        return Redirect::back();
    }

    public function promeni_lozinku(Request $request){
        $request->validate([
            'password' =>  'required|string|min:6|confirmed|max:254'
        ]);

        $password = $_POST['password'];

        $user = Auth::user();

        if(!Hash::check( $password, $user->password)) {
            $hashPassword = Hash::make($password);

            $user->promeniLozinku($hashPassword);
            Session::flash('success', 'Uspešno ste promenili lozinku.');
            return Redirect::back();
        } else{
            return Redirect::back()->withErrors(['greska' => 'Postavili ste već postojeću lozinku.']);
        }
    }
}
