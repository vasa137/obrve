<?php

namespace App\Http\Controllers;

use App\Kupon;
use App\KuponKategorija;
use App\KuponKorisnik;
use App\KuponProizvod;
use App\Porudzbina;
use App\PorudzbinaVaucer;
use App\Proizvod;
use App\ProizvodKategorija;
use App\StavkaPorudzbina;
use App\Utility\Util;
use App\Vaucer;
use Gloudemans\Shoppingcart\Exceptions\InvalidRowIDException;
use Illuminate\Http\Request;
use Cart;
use Auth;
use Mail;
use Carbon\Carbon;

/*
 *
 * Napomena:
 *
 * Prilikom povratka iz ajax-a, ako se vrati pojedinacna stavka u korpi nece se poslati polja subtotal i tax,
 * tako da ako hocemo da vratimo pojednicanu stavku treba da posaljemo njenu total vrednost nezavisno, kao u
 * funkciji azuriraj_kolicinu_u_korpi.
 *
 * Cart::instance('korpa')->total(2, '.', '') Ovo pisemo da bismo dobili broj a ne string npr. 1,450.00 -> OVO JE STRING A NE BROJ
 *
 */

class klijentKorpaController extends Controller
{
    private function ukupnoKorpa(){
        $ukupno = Cart::instance('korpa')->total(2, '.', '') - Cart::instance('vauceri')->total(2, '.', '');

        if($ukupno < 0){
            $ukupno = 0;
        }

        return $ukupno;
    }

   public function pregled_korpe(){

       $stavke = Cart::instance('korpa')->content();
       $stavkeVauceri = Cart::instance('vauceri')->content();

       //uzmi broj
       $total = $this->ukupnoKorpa();

       if($total < 0){
           $total = 0;
       }

       return view('korpa', compact('stavke', 'total', 'stavkeVauceri'));
   }

   public function dodaj_u_korpu(Request $request){
        $id_proizvod = $request->query('id_proizvod');
        $kolicina = $request->query('kolicina');

        $proizvod = Proizvod::dohvatiSaId($id_proizvod);

        if($proizvod == null || $kolicina <= 0){
            abort(404);
        }

        if($proizvod->na_popustu){
            $cena = $proizvod->cena_popust;
        } else{
            $cena = $proizvod->cena;
        }

        $postojiStavka = false;

        $stavke = Cart::instance('korpa')->content();

        foreach($stavke as $st){
            if($st->id == $id_proizvod){
                $stavka = $st;
                $postojiStavka = true;
                break;
            }
        }

        // AKO STAVKA NE POSTOJI POGLEDAJ U TRENUTNOM SKUPU PRIMENJENIH KUPONA DA LI NEKI MOZE DA SE PRIMENI
        if(!$postojiStavka){
            $stavka = Cart::instance('korpa')->add($proizvod->id, $proizvod->naziv, $kolicina, $cena);

            $kuponi = Cart::instance('kuponi')->content();

            $stavka->setTaxRate(0);
            $stavka->kupon = null;

            // DODAJEMO POLJE $stavka->cena zbog tabele porudzbina u kojoj se pamti i puna cena proizvoda.
            // AKO PROIZVOD NIJE NA POPUSTU ovo polje ce imati vrednost kao i polje price
            if($proizvod->na_popustu){
                $stavka->cena = $proizvod->cena;
            } else{
                $stavka->cena = $stavka->price;
            }

            // DODAJEMO POLJE sifra proizvoda zbog tabele porudzbina
            $stavka->sifra = $proizvod->sifra;

            $maxPopust = 0;
            $kuponMax = null;

            foreach($kuponi as $k){
                $kupon = Kupon::dohvatiSaId($k->id);
                if($kupon->popust > $maxPopust && $this->vazi_kupon_za_stavku($stavka, $kupon)){
                    $maxPopust = $kupon->popust;
                    $kuponMax = $kupon;
                }
            }

            if($kuponMax != null){
                // taxRate je procenat popusta ako je -, odnosno procenat povecanja cene ako je +
                $stavka->setTaxRate(-$kuponMax->popust*100);
                $stavka->kupon = $kuponMax;
            }

            // za prikaz prilikom pregleda korpe
            $stavka->nazivGlavneSlike = Util::getInstance()->nazivGlavneSlike($proizvod);

        } else{
            Cart::instance('korpa')->update($stavka->rowId, $stavka->qty + $kolicina);
        }

      // $html = view('include.korpaNavbar')->render();

        return compact('html');
   }

   public function azuriraj_kolicinu_u_korpi(Request $request){
       try {
           $rowId = $request->query('rowId');
           $id_proizvod = $request->query('id_proizvod');
           $kolicina = $request->query('kolicina');

           /*provera za svaki slucaj*/
           $proizvod = Proizvod::dohvatiSaId($id_proizvod);

           if ($proizvod == null || $kolicina <= 0 || Cart::instance('korpa')->get($rowId)->id != $id_proizvod) {
               return abort(404);
           }
           /*provera za svaki slucaj*/

           Cart::instance('korpa')->update($rowId, $kolicina);

           $stavka = Cart::instance('korpa')->get($rowId);

           $total = $this->ukupnoKorpa();

           $stavkaTotal = $stavka->total(2, '.', '');
           return compact('stavka', 'total', 'stavkaTotal');
       }catch (InvalidRowIDException $exc){
           return abort(404);
       }
   }

   private function proveri_da_li_se_kupon_jos_koristi_u_stavkama($kupon){
       $stavke = Cart::instance('korpa')->content();

       $postoji = false;

       foreach($stavke as $stavka){
           if($stavka->kupon != null && $stavka->kupon->id == $kupon->id){
               $postoji = true;
               break;
           }
       }
       return $postoji;
   }

   private function izbaci_kupon_iz_upotrebe($kupon){
       $kuponi = Cart::instance('kuponi')->content();

       foreach($kuponi as $k){
           if($k->id == $kupon->id){
               Cart::instance('kuponi')->remove($k->rowId);
               break;
           }
       }
   }

   public function obrisi_iz_korpe(Request $request){
        try {
            $rowId = $request->query('rowId');
            $id_proizvod = $request->query('id_proizvod');

            /*provera za svaki slucaj*/
            $proizvod = Proizvod::dohvatiSaId($id_proizvod);

            if ($proizvod == null || Cart::instance('korpa')->get($rowId)->id != $id_proizvod) {
                return abort(404);
            }
            /*provera za svaki slucaj*/

            $kupon = Cart::instance('korpa')->get($rowId)->kupon;

            Cart::instance('korpa')->remove($rowId);

            if($kupon != null && !$this->proveri_da_li_se_kupon_jos_koristi_u_stavkama($kupon)){
                $this->izbaci_kupon_iz_upotrebe($kupon);
            }

            $brojElemenata = count(Cart::instance('korpa')->content());

            if($brojElemenata == 0){
                Cart::instance('vauceri')->destroy();
            }

           // $html = view('include.korpaNavbar')->render();

            $total = $this->ukupnoKorpa();

            // vracamo broj elemenata
            return compact('brojElemenata', 'html', 'total');
        }catch (InvalidRowIDException $exc){
            return abort(404);
        }
    }

    private function vazi_kupon_za_kupca($kupon){
        if(!Auth::check()) {
            return false;
        }

        $kuponKorisnici = KuponKorisnik::dohvatiKorisnikeZaKupon($kupon->id);

        $zaKupca = false;

        foreach ($kuponKorisnici as $kuponKorisnik) {
            if ($kuponKorisnik->id_user == Auth::user()->id) {
                $zaKupca = true;
                break;
            }
        }

        return $zaKupca;
    }

    private function vazi_kupon_za_kategorije($stavka, $kupon){
        $kuponKategorije = KuponKategorija::dohvatiKategorijeZaKupon($kupon->id);

        $proizvodKategorije = ProizvodKategorija::dohvatiKategorijeZaProizvod($stavka->id);

        $nizKategorijaProizvod = [];

        foreach($proizvodKategorije as $proizvodKategorija){
            $putanjaKategorijaNiz = Util::getInstance()->buildCategoryIdParentArray($proizvodKategorija->id_kategorija);
            $nizKategorijaProizvod = array_merge($nizKategorijaProizvod, $putanjaKategorijaNiz);
        }

        $nizKategorijaProizvod = array_unique($nizKategorijaProizvod);

        $zaKategoriju = false;

        foreach($kuponKategorije as $kuponKategorija){
            if(in_array($kuponKategorija->id_kategorija, $nizKategorijaProizvod)){
                $zaKategoriju = true;
                break;
            }
        }

        return $zaKategoriju;
    }

    private function vazi_kupon_za_specijalni_proizvod($stavka, $kupon){

        if(!$kupon->jos_proizvoda) {
            return false;
        } else{
            $kuponProizvodi = KuponProizvod::dohvatiProizvodeZaKupon($kupon->id);

            $zaProizvod = false;

            foreach($kuponProizvodi as $kuponProizvod){
                if($kuponProizvod->id_proizvod == $stavka->id){
                    $zaProizvod = true;
                    break;
                }
            }

            return $zaProizvod;
        }
    }

    private function vazi_kupon_za_stavku($stavka, $kupon){
        $proizvod = Proizvod::dohvatiSaId($stavka->id);

        if(!$kupon->za_sve_kupce) {
            $zaKupca = $this->vazi_kupon_za_kupca($kupon);

            if(!$zaKupca){
                return false;
            }
        }

        if($proizvod->na_popustu && !$kupon->za_snizene){
            if(!$this->vazi_kupon_za_specijalni_proizvod($stavka, $kupon)){
                return false;
            }
        }

        if(!$kupon->za_sve_kategorije){
            $zaKategoriju = $this->vazi_kupon_za_kategorije($stavka, $kupon);

            if(!$zaKategoriju){
                if(!$this->vazi_kupon_za_specijalni_proizvod($stavka, $kupon)){
                    return false;
                }
            }
        }

        return true;
    }

   public function primeni_kupon_vaucer(Request $request){
       $kod = $request->query('kod');

       $kupon = Kupon::dohvatiAktivanKuponSaKodom($kod);
       $vaucer = Vaucer::dohvatiAktivanSaKodom($kod);

       if($kupon != null){
           $pretragaKupon = Cart::instance('kuponi')->search(function ($cartItem, $rowId) use ($kupon) {
               return $cartItem->id === $kupon->id;
           })->first();

           $primenjen = false;
           // da li je kupon vec primenjen
           if($pretragaKupon != null){
               $poruka = "Kupon je već primenjen.";
                return compact('poruka', 'primenjen');
           } else{
               $total = $this->ukupnoKorpa();

               if($total > 0) {
                   $stavke = Cart::instance('korpa')->content();

                   $primenjenKupon = [];

                   foreach ($stavke as $stavka) {
                       if (!$this->vazi_kupon_za_stavku($stavka, $kupon)) {
                           continue;
                       }

                       $trenutniKupon = $stavka->kupon;

                       // STAVI KUPON U STAVKU AKO JE USLOV ZADOVOLJEN
                       if ($trenutniKupon == null || $trenutniKupon->popust <= $kupon->popust) {
                           $primenjen = true;
                           $stavka->setTaxRate(-$kupon->popust * 100);
                           $stavka->kupon = $kupon;

                           // SADRZE STAVKE ZA KOJI JE TRENUTNI KUPON PRIMENJEN, IAKO SVAKA STAVKA IMA POLJE KUPON, ONO SE NE MOZE POSLATI PUTEM JSON_ENCODE-A, A AJAX TAKO RADI
                           // TAKODJE KUPON SE NE SME UBACITI U OPCIJE KORPE, JER SE MENJA ROWID KAD SE OPCIJA PROMENI
                           $primenjenKupon[$stavka->rowId] = $kupon;

                           if ($trenutniKupon != null && !$this->proveri_da_li_se_kupon_jos_koristi_u_stavkama($trenutniKupon)) {
                               $this->izbaci_kupon_iz_upotrebe($trenutniKupon);
                           }
                       }
                   }

                   if ($primenjen) {
                       Cart::instance('kuponi')->add($kupon->id, $kupon->kod, 1, $kupon->popust);
                       $poruka = 'Kupon je uspešno primenjen.';
                   } else {
                       $poruka = 'Kupon se ne može primeniti ili ne daje bolji popust od već primenjenih.';
                   }

                   $total = $this->ukupnoKorpa();

                   $kupon_ili_vaucer = 'kupon';

                   return compact('kupon_ili_vaucer', 'poruka', 'primenjen', 'stavke', 'primenjenKupon', 'total');
               }
               else{
                   $primenjen = false;
                   $poruka = 'Kupon se ne može primeniti, jer je cena porudžbine već anulirana.';

                   return compact('primenjen', 'poruka');
               }
           }
       } else if($vaucer != null){
               $pretragaVaucer = Cart::instance('vauceri')->search(function ($cartItem, $rowId) use ($vaucer) {
                   return $cartItem->id === $vaucer->id;
               })->first();

               if($pretragaVaucer == null) {
                   $kupon_ili_vaucer = 'vaucer';

                   $total = $this->ukupnoKorpa();

                   if ($total > 0) {
                       $primenjen = true;

                       Cart::instance('vauceri')->add($vaucer->id, $vaucer->kod, 1, $vaucer->iznos)->setTaxRate(0);

                       $noviIznos = $total - $vaucer->iznos;

                       if ($noviIznos < 0) {
                           $total = 0;
                       } else {
                           $total = $noviIznos;
                       }

                       $poruka = "Vaučer je uspešno primenjen.";

                       $stavkeVauceri = Cart::instance('vauceri')->content();
                       $html = view('include.korpaVauceri', compact('stavkeVauceri'))->render();

                       return compact('kupon_ili_vaucer', 'primenjen', 'poruka', 'total', 'vaucer', 'html');
                   } else {
                       $primenjen = false;
                       $poruka = 'Vaučer se ne može primeniti, jer je cena porudžbine već anulirana.';

                       return compact('kupon_ili_vaucer', 'primenjen', 'poruka');
                   }
               } else{
                   $primenjen = false;
                   $poruka = "Vaučer je već primenjen.";
                   return compact('poruka', 'primenjen');
               }
       } else{
           $primenjen = false;
           $poruka = 'Ne postoje ni kupon ni vaučer sa datim kodom.';

           return compact('primenjen', 'poruka');
       }
   }

   public function ukloni_vaucer_iz_korpe(Request $request){
       try {
           $rowId = $request->query('rowId');
           $id_vaucer = $request->query('id_vaucer');

           /*provera za svaki slucaj*/
           $vaucer = Vaucer::dohvatiSaId($id_vaucer);

           if ($vaucer == null || Cart::instance('vauceri')->get($rowId)->id != $id_vaucer) {
               return abort(404);
           }
           /*provera za svaki slucaj*/

           Cart::instance('vauceri')->remove($rowId);

           $stavkeVauceri = Cart::instance('vauceri')->content();

           if(count($stavkeVauceri) == 0){
               $html = '';
           } else {
               $html = view('include.korpaVauceri', compact('stavkeVauceri'))->render();
           }

           $total = $this->ukupnoKorpa();

           return compact('total','html');
       }catch (InvalidRowIDException $exc){
           return abort(404);
       }
   }

   public function naplati(){
        $stavke = Cart::instance('korpa')->content();

        if(count($stavke) == 0){
            abort(404);
        }

        $total = $this->ukupnoKorpa();

        return view('naplati', compact('total'));
   }

   public function sacuvaj_porudzbinu(Request $request){
       $request->validate([
           'ime_prezime' => 'required|string|max:254',
           'telefon' => 'required|string|max:19',
           'grad' => 'required|string|max:254',
           'email' => 'required|email|max:254',
           'adresa' => 'required|string|max:254',
           'zip' => 'required|numeric|min:10000|max:99999',
           'napomena' => 'max:999',
           'uslovi' => 'required'
       ]);

       $stavke = Cart::instance('korpa')->content();

       if(count($stavke) == 0){
           abort(404);
       }

       $dostava_ime_prezime = $ime_prezime = $_POST['ime_prezime'];
       $dostava_telefon = $telefon = $_POST['telefon'];
       $dostava_grad = $grad = $_POST['grad'];
       $dostava_email = $email = $_POST['email'];
       $dostava_adresa = $adresa = $_POST['adresa'];
       $dostava_zip = $zip = $_POST['zip'];

       $napomena = $_POST['napomena'];

       $id_user = null;

       if(Auth::check()){
           $user = Auth::user();
           $id_user = $user->id;

           $ime_prezime  = $user->ime_prezime;
           $telefon = $user->telefon;
           $grad = $user->grad;
           $email = $user->email;
           $adresa = $user->adresa;
           $zip = $user->zip;
       }

       $porudzbina = new Porudzbina();

       $porudzbina->napuni($ime_prezime, $telefon, $grad, $email, $adresa, $zip, $dostava_ime_prezime, $dostava_telefon, $dostava_grad, $dostava_email, $dostava_adresa, $dostava_zip, $napomena, $id_user);

       foreach($stavke as $stavka){
           $stavkaPorudzbina = new StavkaPorudzbina();

           $id_kupon = null;

           if($stavka->kupon != null){
               $id_kupon = $stavka->kupon->id;
           }

           $stavkaPorudzbina->napuni($stavka->qty, $stavka->cena, $stavka->price, $stavka->sifra, $stavka->id, $porudzbina->id, $id_kupon);

           $proizvod = Proizvod::dohvatiSaId($stavka->id);

           $broj_komada  = $proizvod->br_komada - $stavka->qty;

           /*
           // AKO SE NE PRATI BROJ KOMADA NE DAJ U MINUS
           if($proizvod->lager != 'prati_broj_komada'){
               if($broj_komada < 0){
                   $broj_komada = 0;
               }
           }
           */

           $proizvod->azurirajBrojKomada($broj_komada);
       }

       $stavkaVauceri = Cart::instance('vauceri')->content();

       foreach($stavkaVauceri as $stavkaVaucer){
           $porudzbinaVaucer = new PorudzbinaVaucer();

           $porudzbinaVaucer->napuni($porudzbina->id, $stavkaVaucer->id);
       }

        //info za mail
        $vreme = Carbon::now();
        $vreme->tz='Europe/Belgrade';
        $porudzbina=Porudzbina::dohvatiSaId($porudzbina->id); //dohvata nakon trigera iz baze
       $stavke = StavkaPorudzbina::dohvatiStavkeZaPorudzbinu($porudzbina->id);
       $kuponi = [];
       foreach ($stavke as $s)
       {  $proizvod = Proizvod::dohvatiSaId($s->id_proizvod);
           $proizvodi[$s->id] = $proizvod;
           $glavneSlike[$s->id_proizvod] =  Util::getInstance()->nazivGlavneSlike($proizvod);
           if($s->id_kupon)
           {
               $kuponi[$s->id_kupon] = Kupon::dohvatiSaId($s->id_kupon);



           }
       }

       $vauceri = [];

       if($porudzbina->ima_vaucere){
           $porudzbinaVauceri = PorudzbinaVaucer::dohvatiVaucereZaPorudzbinu($porudzbina->id);

           foreach($porudzbinaVauceri as $porudzbinaVaucer){
               $vauceri[] = Vaucer::dohvatiSaId($porudzbinaVaucer->id_vaucer);
           }
       }

       $data =[
           'stavke' => $stavke,
           'porudzbina' =>  $porudzbina,
           'vreme' => $vreme,
           'kuponi' =>$kuponi,
           'glavneSlike' => $glavneSlike,
           'vauceri' => $vauceri,
           'proizvodi' =>$proizvodi
       ];
        
        //-----------------slanje maila kupcu-----------
         Mail::send('mailovi.porudzbina', $data, function($message) use ($porudzbina) {
         $message->to('prodaja@essenceofbeauty.rs', 'Nova porudzbina - '.$porudzbina->id.' - '.$porudzbina->kupac)->subject('Nova porudzbina - '.$porudzbina->id.' - '.$porudzbina->kupac);
         $message->from('prodaja@essenceofbeauty.rs' ,'Nova porudzbina - '.$porudzbina->id.' - '.$porudzbina->kupac);
        });

        //-----------------slanje maila kupcu-----------
         Mail::send('mailovi.porudzbina', $data, function($message) use ($porudzbina) {
         $message->to($porudzbina->email, 'Potvrda porudžbine #'.$porudzbina->id)->subject('Potvrda porudžbine #'.$porudzbina->id);
         $message->from('prodaja@essenceofbeauty.rs' ,'ESSENCE OF BEAUTY');
        });

           
        //dd($stavke);
        
       //return view('mailovi.porudzbina',compact('vreme','porudzbina','stavke'));

       Cart::instance('korpa')->destroy();
       Cart::instance('kuponi')->destroy();
       Cart::instance('vauceri')->destroy();


       return redirect('/porudzbina-uspesna');
   }

}
