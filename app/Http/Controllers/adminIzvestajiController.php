<?php

namespace App\Http\Controllers;

use App\Izvestaj;
use App\KupacIzvestaj;
use App\Kupon;
use App\Porudzbina;
use App\PorudzbinaVaucer;
use App\StavkaPorudzbina;
use App\Vaucer;
use Illuminate\Http\Request;
use App\Proizvod;
use App\Period;
use Carbon\Carbon;
class adminIzvestajiController extends Controller
{



    public function izvestaji()
    {
    	return view('admin.adminIzvestaji');
    }


    public function dohvatiIzvestaj(){
        $datumOd = Carbon::parse($_GET['datumOd'])->format('Y-m-d');
        $datumDo = Carbon::parse($_GET['datumDo'])->format('Y-m-d');
        $interval = $_GET['interval'];
        $tipIzvestaja = $_GET['tipIzvestaja'];

        $periodi = [];

        for($datum = $datumOd; $datum <= $datumDo; ){

            $periodi[] = new Period($datum);

            // predji na sledeci datum
            $brDanaZaDodavanje = '';
            switch ($interval){
                case 'dan':
                    $brDanaZaDodavanje = ' +1 day';
                    break;
                case 'nedelja':
                    $brDanaZaDodavanje = ' +1 week';
                    break;
                case 'mesec':
                    $brDanaZaDodavanje = ' +1 month';
                    break;
                case 'godina':
                    $brDanaZaDodavanje = ' +1 year';
                    break;
            }

            $datum =  date('Y-m-d', strtotime($datum . $brDanaZaDodavanje));
        }

        for($i = 0; $i < count($periodi); $i++){
            $period = $periodi[$i];

            $datum1 = $period->getDatum1();

            if($i == count($periodi) - 1){
                $datum2 = date('Y-m-d', strtotime($datumDo . ' +1 day'));
            } else{
                $datum2 = $periodi[$i + 1]->getDatum1();
            }

            $period->setDatum2(date('Y-m-d', strtotime($datum2 . ' -1 day')));

            $izvestaji = [];

            switch ($tipIzvestaja) {
                case 'proizvodi':

                    $izvestajiInfo = StavkaPorudzbina::dohvatiIzvestajZaProizvode($datum1, $datum2);

                    foreach ($izvestajiInfo as $info) {
                        $izvestaji [] = new Izvestaj($info->naziv, $info->porudzbina, $info->komada, $info->razlika, $info->ukupno);
                    }
                     break;
                case 'gradovi':
                    $izvestajiInfo = StavkaPorudzbina::dohvatiIzvestajZaGradove($datum1, $datum2);
                    foreach ($izvestajiInfo as $info) {
                        $izvestaji [] = new Izvestaj($info->naziv, $info->porudzbina, $info->komada, $info->razlika, $info->ukupno);
                    }

                    break;
                case 'brendovi':
                    $izvestajiInfo = StavkaPorudzbina::dohvatiIzvestajZaBrendove($datum1, $datum2);
                    foreach ($izvestajiInfo as $info) {
                        $izvestaji [] = new Izvestaj($info->naziv, $info->porudzbina, $info->komada, $info->razlika, $info->ukupno);
                    }
                    break;
                case 'dobavljači':
                    $izvestajiInfo = StavkaPorudzbina::dohvatiIzvestajZaDobavljace($datum1, $datum2);
                    foreach ($izvestajiInfo as $info) {
                        $izvestaji [] = new Izvestaj($info->naziv, $info->porudzbina, $info->komada, $info->razlika, $info->ukupno);
                    }
                    break;
                case 'kategorija':
                    $izvestajiInfo = StavkaPorudzbina::dohvatiIzvestajZaKategorije($datum1, $datum2);
                    foreach ($izvestajiInfo as $info) {
                        $izvestaji [] = new Izvestaj($info->naziv, $info->porudzbina, $info->komada, $info->razlika, $info->ukupno);
                    }
                    break;
                case 'kupci':
                    $izvestajiInfo = StavkaPorudzbina::dohvatiIzvestajZaKupce($datum1, $datum2);
                    foreach ($izvestajiInfo as $info) {
                        $podaci = explode(',',$info->dodatno);
                        $kupacIzvestaj = new KupacIzvestaj($info->email, $podaci[0], $podaci[1], $podaci[2]);
                        $izvestaji [] = new Izvestaj($kupacIzvestaj, $info->porudzbina, $info->komada, $info->razlika, $info->ukupno);
                    }
                    break;
                case 'stavka':
                    $izvestajiInfo = StavkaPorudzbina::dohvatiIzvestajZaSveStavke($datum1, $datum2);

                    $porudzbine = [];

                    foreach ($izvestajiInfo as $info) {
                        $stavkaPorudzbina = StavkaPorudzbina::dohvatiSaId($info->id_stavka_porudzbina);

                        if(!isset($porudzbine[$stavkaPorudzbina->id_porudzbina])){
                            $porudzbine[$stavkaPorudzbina->id_porudzbina] = Porudzbina::dohvatiSaId($stavkaPorudzbina->id_porudzbina);
                        }

                        $stavkaPorudzbina->porudzbina = $porudzbine[$stavkaPorudzbina->id_porudzbina];

                        $stavkaPorudzbina->proizvod = Proizvod::dohvatiSaId($stavkaPorudzbina->id_proizvod);

                        if($stavkaPorudzbina->id_kupon != null){
                            $stavkaPorudzbina->kupon = Kupon::dohvatiSaId($stavkaPorudzbina->id_kupon);
                        }

                        $izvestaji [] = new Izvestaj($stavkaPorudzbina, 0, $info->komada, $info->razlika, $info->ukupno);
                    }

                    break;
                case 'kuponi':
                    $izvestajiInfo = StavkaPorudzbina::dohvatiIzvestajZaKupone($datum1, $datum2);
                    foreach ($izvestajiInfo as $info) {
                        if($info->id_kupon == 'Nema kupona'){
                            $kupon = null;
                        }else {
                            $kupon = Kupon::dohvatiSaId($info->id_kupon);
                            $kupon->ukupno_ustedjeno = $info->ukupno_ustedjeno;
                        }
                        $izvestaji [] = new Izvestaj($kupon, $info->porudzbina, $info->komada, $info->razlika, $info->ukupno);
                    }
                    break;
                case 'vaučeri':
                    $izvestajiInfo = PorudzbinaVaucer::dohvatiIzvestajZaVaucere($datum1, $datum2);
                    foreach ($izvestajiInfo as $info) {
                        $vaucer = Vaucer::dohvatiSaId($info->id_vaucer);
                        $vaucer->ukupno_ustedjeno = $info->ukupno_ustedjeno;

                        $izvestaji [] = new Izvestaj($vaucer, $info->porudzbina, 0, 0, 0);
                    }
                    break;
            }

            $period->setIzvestaji($izvestaji);
        }

        return view('admin.adminIzvestaji', compact('datumOd', 'datumDo', 'interval', 'tipIzvestaja', 'periodi'));
    }
}
