<?php

namespace App\Http\Controllers;

use App\Dobavljac;
use App\StavkaPorudzbina;
use Illuminate\Http\Request;
use App\Proizvod;
use Redirect;
class adminDobavljaciController extends Controller
{
    private function popuniInfoDobavljac($dobavljac){
        $dobavljacInfo = StavkaPorudzbina::dohvatiPrometOdDobavljaca($dobavljac->id);

        $dobavljac->broj_prodatih = $dobavljacInfo->broj_proizvoda;
        $dobavljac->promet = $dobavljacInfo->promet;
        $dobavljac->broj_na_stanju = Proizvod::dohvatiBrojProizvodaNaStanjuOdDobavljaca($dobavljac->id);
    }

	public function dobavljaci()
	{
		$dobavljaci = Dobavljac::dohvatiSveAktivne();

        $dobavljaciObrisani = Dobavljac::dohvatiSveObrisane();

        foreach($dobavljaci as $dobavljac){
            $this->popuniInfoDobavljac($dobavljac);
        }

        foreach($dobavljaciObrisani as $dobavljac){
            $this->popuniInfoDobavljac($dobavljac);
        }


		return view('admin.adminDobavljaci', compact('dobavljaci', 'dobavljaciObrisani'));
	}



	public function dobavljac($id)
	{
	    $izmena = false;

		if($id > 0){
		    $izmena = true;
        }

		if(!$izmena){
            return view('admin.adminDobavljac', compact('izmena'));
        } else{
            $dobavljac = Dobavljac::dohvatiSaId($id);

            if($dobavljac == null){
                abort(404);
            }

            $this->popuniInfoDobavljac($dobavljac);

            return view('admin.adminDobavljac', compact('izmena', 'dobavljac'));
        }


	}

	public function sacuvaj_dobavljaca($id){
	    $izmena = false;

	    if($id > 0){
	        $izmena = true;
        }

	    $naziv = $_POST['naziv'];
	    $sifra = $_POST['sifra'];
	    $adresa = $_POST['adresa'];
	    $grad = $_POST['grad'];
	    $zip = $_POST['zip'];
	    $opis = $_POST['opis'];
	    $broj_telefona =$_POST['broj_telefona'];
	    $broj_telefona2 = $_POST['broj_telefona2'];
	    $email = $_POST['email'];
	    $email2 = $_POST['email2'];
	    $fax = $_POST['fax'];
	    $PIB = $_POST['PIB'];
	    $maticni_broj = $_POST['maticni_broj'];

        if($izmena){
            $dobavljac = Dobavljac::dohvatiSaId($id);
        } else{
            $dobavljac = new Dobavljac();
        }

        $dobavljac->napuni($naziv, $sifra, $adresa, $grad, $zip, $opis, $broj_telefona, $broj_telefona2, $email, $email2, $fax, $PIB, $maticni_broj);

	    return redirect('/admin/dobavljac/' . $dobavljac->id);
	}

	public function obrisi_dobavljaca($id)
	{
		$dobavljac = Dobavljac::dohvatiSaId($id);

		$dobavljac->obrisi();

		return Redirect::back();
	}

    public function restauriraj_dobavljaca($id)
    {
        $dobavljac = Dobavljac::dohvatiSaId($id);

        $dobavljac->restauriraj();

        return Redirect::back();
    }

}
