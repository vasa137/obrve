<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 05-Apr-19
 * Time: 22:10
 */

namespace App\Http\Controllers;
use App\ProizvodTag;
use App\Tag;
use Redirect;
class adminTagoviController
{
    private function popuniTagInfo($tag){
        $tag->broj_proizvoda = ProizvodTag::dohvatiBrojProizvodaZaTag($tag->id);
    }

    public function tag($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        if(!$izmena){
            return view('admin.adminTag', compact('izmena'));
        } else{
            $tag = Tag::dohvatiSaId($id);

            if($tag == null){
                abort(404);
            }

            $this->popuniTagInfo($tag);

            return view('admin.adminTag', compact('izmena', 'tag'));
        }
    }

    public function tagovi(){
        $aktivniTagovi = Tag::dohvatiSveAktivne();
        $obrisaniTagovi = Tag::dohvatiSveObrisane();

        foreach($aktivniTagovi as $tag){
            $this->popuniTagInfo($tag);
        }

        foreach($obrisaniTagovi as $tag){
            $this->popuniTagInfo($tag);
        }

        return view('admin.adminTagovi', compact('aktivniTagovi', 'obrisaniTagovi'));
    }

    public function sacuvaj_tag($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $naziv = $_POST['naziv'];
        $opis = $_POST['opis'];

        $zaPunjenje = true;

        if($izmena){
            $tag = Tag::dohvatiSaId($id);

            if($tag->naziv == $naziv && $tag->opis == $opis ){
                $zaPunjenje = false;
            }

        } else{
            $tag = new Tag();
        }

        if($zaPunjenje) {
            $tag->napuni($naziv, $opis);
        }

        return redirect('/admin/tag/' . $tag->id);
    }

    public function obrisi_tag($id){
        $tag = Tag::dohvatiSaId($id);

        $tag->obrisi();

        return Redirect::back();
    }

    public function restauriraj_tag($id){
        $tag = Tag::dohvatiSaId($id);

        $tag->restauriraj();

        return Redirect::back();
    }
}