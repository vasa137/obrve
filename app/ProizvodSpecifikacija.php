<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProizvodSpecifikacija extends Model
{
    protected $table = 'proizvod_specifikacija';

    protected $fillable = ['id_proizvod', 'id_specifikacija', 'tekst'];

    public function napuni($id_proizvod, $id_specifikacija, $tekst){
        $this->id_proizvod = $id_proizvod;
        $this->id_specifikacija = $id_specifikacija;
        $this->tekst = $tekst;

        $this->save();
    }

    public static function dohvatiSpecifikacijeZaProizvod($id){
        return ProizvodSpecifikacija::where('id_proizvod', $id)->get();
    }

    public static function obrisiSpecifikacijeZaProizvod($id){
        ProizvodSpecifikacija::where('id_proizvod', $id)->delete();
    }

    public static function dohvatiBrojProizvodaZaSpecifikaciju($id){
        return DB::select("
            select IFNULL(COUNT(p.id), 0) as broj_proizvoda
            FROM proizvod_specifikacija ps, proizvod p
            WHERE ps.id_specifikacija = $id
            AND ps.id_proizvod = p.id
            AND p.sakriven = 0
        ")[0]->broj_proizvoda;
    }
}
