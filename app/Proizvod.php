<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Proizvod extends Model
{
    protected $table = 'proizvod';

    protected $fillable = ['sifra', 'naziv', 'opis', 'kratak_opis', 'cena', 'na_popustu', 'cena_popust', 'ima_opcije',
        'lager', 'br_komada', 'sakriven', 'id_brend', 'id_dobavljac', 'nabavna_cena', 'ima_specifikacije', 'ima_tagove','meta_title',
        'meta_tags', 'meta_desc'];

    protected $appends = ['brend', 'dobavljac', 'kategorije', 'specifikacije', 'specifikacije_tekst', 'grupe_opcija', 'niz_opcija', 'opcije', 'tagovi', 'opisi', 'sveSlike', 'opcijeSlike', 'broj_porucivanja', 'naziv_glavne_slike'];

    protected $brend;
    protected $dobavljac;
    protected $kategorije;
    protected $specifikacije;
    protected $specifikacije_tekst;
    protected $grupe_opcija;
    protected $niz_opcija;
    protected $opcije;
    protected $tagovi;
    protected $opisi;
    protected $sveSlike;
    protected $opcijeSlike;
    protected $broj_porucivanja;
    protected $naziv_glavne_slike = null;

    public function setBrendAttribute($brend){
        $this->brend = $brend;
    }

    public function getBrendAttribute(){
        return $this->brend;
    }

    public function setDobavljacAttribute($dobavljac){
        $this->dobavljac = $dobavljac;
    }

    public function getDobavljacAttribute(){
        return $this->dobavljac;
    }

    public function setKategorijeAttribute($kategorije){
        $this->kategorije = $kategorije;
    }

    public function getKategorijeAttribute(){
        return $this->kategorije;
    }

    public function setSpecifikacijeAttribute($specifikacije){
        $this->specifikacije = $specifikacije;
    }

    public function getSpecifikacijeAttribute(){
        return $this->specifikacije;
    }

    public function setSpecifikacijeTekstAttribute($specifikacijeTekst){
        $this->specifikacije_tekst = $specifikacijeTekst;
    }

    public function getSpecifikacijeTekstAttribute(){
        return $this->specifikacije_tekst;
    }

    public function setGrupeOpcijaAttribute($grupeOpcija){
        $this->grupe_opcija = $grupeOpcija;
    }

    public function getGrupeOpcijaAttribute(){
        return $this->grupe_opcija;
    }

    public function setNizOpcijaAttribute($nizOpcija){
        $this->niz_opcija = $nizOpcija;
    }

    public function getNizOpcijaAttribute(){
        return $this->niz_opcija;
    }

    public function setOpcijeAttribute($opcije){
        $this->opcije = $opcije;
    }

    public function getOpcijeAttribute(){
        return $this->opcije;
    }

    public function setTagoviAttribute($tagovi){
        $this->tagovi = $tagovi;
    }

    public function getTagoviAttribute(){
        return $this->tagovi;
    }

    public function setOpisiAttribute($opisi){
        $this->opisi = $opisi;
    }

    public function getOpisiAttribute(){
        return $this->opisi;
    }

    public function setSveSlikeAttribute($sveSlike){
        $this->sveSlike = $sveSlike;
    }

    public function getSveSlikeAttribute(){
        return $this->sveSlike;
    }

    public function setOpcijeSlikeAttribute($opcijeSlike){
        $this->opcijeSlike = $opcijeSlike;
    }

    public function getOpcijeSlikeAttribute(){
        return $this->opcijeSlike;
    }

    public function setBrojPorucivanjaAttribute($broj_porucivanja){
        $this->broj_porucivanja = $broj_porucivanja;
    }

    public function getBrojporucivanjaAttribute(){
        return $this->broj_porucivanja;
    }

    public function setNazivGlavneSlikeAttribute($naziv_glavne_slike){
        $this->naziv_glavne_slike = $naziv_glavne_slike;
    }

    public function getNazivGlavneSlikeAttribute(){
        return $this->naziv_glavne_slike;
    }

    public function napuni($sifra, $naziv, $opis, $kratak_opis, $cena, $na_popustu, $cena_popust, $ima_opcije, $lager,
                        $br_komada, $id_brend, $id_dobavljac, $nabavna_cena, $ima_specifikacije, $ima_tagove, $meta_title, $meta_tags, $meta_desc){
        $this->sifra = $sifra;
        $this->naziv = $naziv;
        $this->opis = $opis;
        $this->kratak_opis = $kratak_opis;
        $this->cena = $cena;
        $this->na_popustu = $na_popustu;
        $this->cena_popust = $cena_popust;
        $this->ima_opcije = $ima_opcije;
        $this->lager = $lager;
        $this->br_komada = $br_komada;
        $this->id_brend = $id_brend;
        $this->id_dobavljac =$id_dobavljac;
        $this->nabavna_cena = $nabavna_cena;
        $this->ima_specifikacije = $ima_specifikacije;
        $this->ima_tagove = $ima_tagove;
        $this->meta_title = $meta_title;
        $this->meta_tags = $meta_tags;
        $this->meta_desc = $meta_desc;

        $this->save();
    }

    public static function dohvatiSaId($id){
        return Proizvod::where('id', $id)->first();
    }

    public function obrisi(){
        $this->sakriven = 1;
        $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;
        $this->save();
    }

    public static function dohvatiSveAktivne(){
        return Proizvod::where('sakriven', 0)->get();
    }

    public static function dohvatiSveObrisane(){
        return Proizvod::where('sakriven', 1)->get();
    }

    public static function dohvatiBrojProizvodaNaStanjuOdDobavljaca($id){
        return DB::select("
            select IFNULL(SUM(p.br_komada), 0) as broj_proizvoda
            FROM proizvod p
            WHERE p.id_dobavljac = $id
            AND p.lager <> 'van_stanja'
            AND p.sakriven = 0
        ")[0]->broj_proizvoda;
    }
    public static function dohvatiBrojRazlicitihKategorijaZaBrend($id){
        return DB::select("
            select IFNULL(COUNT(distinct pk.id_kategorija), 0) as broj_kategorija
            FROM proizvod_kategorija pk, proizvod p
            WHERE pk.id_proizvod = p.id
            AND p.id_brend = $id
            AND p.sakriven = 0
        ")[0]->broj_kategorija;
    }


    public static function dohvatiBrojRazlicitihProizvodaZaBrend($id){
        return DB::select("
            select IFNULL(COUNT(p.id), 0) as broj_proizvoda
            FROM proizvod p
            WHERE p.id_brend = $id
            AND p.sakriven = 0
        ")[0]->broj_proizvoda;
    }

    public function azurirajBrojKomada($br_komada){
        $this->br_komada = $br_komada;

        $this->save();
    }

    public function nazivGlavneSlike(){
        if($this->naziv_glavne_slike == null){
            $proizvod_naziv_glavne_slike = '';

            if ($this->id_brend != null) {
                $brend = Brend::dohvatiSaId($this->id_brend);
                $proizvod_naziv_glavne_slike = $brend->naziv . '-';
            }

            $proizvod_naziv_glavne_slike .= $this->naziv . '-' . $this->sifra;

            $this->naziv_glavne_slike = $proizvod_naziv_glavne_slike;
        }

        return $this->naziv_glavne_slike;
    }

    public static function filtriraj($kategorijeZaFilter, $brendoviZaFilter, $sortBy, $redosled, $paginacija){
        $query = DB::table('proizvod')->where('sakriven', 0)->where(function($query)
        {
            $query->where('lager',  'na_stanju')
                ->orWhere(function($query)
                {
                    $query->where('lager',  'prati_broj_komada')
                        ->where('br_komada', '>', '0');
                });
        });
        if(!empty($kategorijeZaFilter)){
            $query->whereRaw('EXISTS(select pk.id_kategorija from proizvod_kategorija pk where pk.id_proizvod = proizvod.id and pk.id_kategorija IN (' . implode(',', $kategorijeZaFilter) . ') )');
        }

        if(!empty($brendoviZaFilter)){
            $query->whereNotNull('proizvod.id_brend');
            $query->whereIn('proizvod.id_brend', $brendoviZaFilter);
        }

        $query->select('proizvod.*');

        switch ($sortBy) {
            case 'naziv':
                $query->orderBy('proizvod.naziv', $redosled);
                break;
            case 'cena':
                $query->orderByRaw('IF(proizvod.na_popustu=1,proizvod.cena_popust,proizvod.cena) ' . $redosled);
                break;
            case 'created_at':
                $query->orderBy('proizvod.created_at', $redosled);
                break;
        }

        return $query->paginate($paginacija);
    }


    public static function dohvatiIzIstihKategorije($id, $kategorijeIds){
        return Proizvod::select('proizvod.*')->where('id', '<>', $id)->whereRaw('EXISTS(select pk.id_kategorija from proizvod_kategorija pk where pk.id_proizvod = proizvod.id and pk.id_kategorija IN (' . implode(',', $kategorijeIds) . ') )')->distinct('proizvod.id')->limit(6)->get();
    }


}
