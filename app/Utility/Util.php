<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 27-Feb-19
 * Time: 14:59
 */

namespace App\Utility;


use App\Brend;
use App\Kategorija;

class Util
{
    private static $instance = null;

    public static function getInstance(){
        if(Util::$instance == null){
            Util::$instance = new Util();
        }

        return Util::$instance;
    }


    public function buildCategoryTree($categories, $parentId){
        $branch = array();

        foreach($categories as $category){
            if($category->id_nad_kategorija == $parentId){
                $children = $this->buildCategoryTree($categories, $category->id);
                if($children){
                    $category->setChildrenAttribute($children);
                }
                $branch[] = $category;
            }
        }

        return $branch;
    }

    public function buildCategoryIdSubtreeArray($categories, $parentId, &$arr){
        $arr[] = $parentId;
        foreach($categories as $category){
            if($category->id_nad_kategorija == $parentId){
                $this->buildCategoryIdSubtreeArray($categories, $category->id, $arr);
            }
        }
    }

    public function buildCategoryIdParentArray($id){
        $arr = [];
        $kategorija = Kategorija::dohvatiSaId($id);

        while($kategorija != null){
            $arr[] = $kategorija->id;

            if($kategorija->id_nad_kategorija == null){
                $kategorija = null;
            } else{
                $kategorija = Kategorija::dohvatiSaId($kategorija->id_nad_kategorija);
            }
        }

        return $arr;
    }

    public function pokupiNizFajlova($direktorijum)
    {
        $fajlovi = [];
        foreach (scandir($direktorijum) as $file) {
            if ('.' === $file) continue;
            if ('..' === $file) continue;

            $fajlovi[] = $file;
        }

        return $fajlovi;
    }



    public function nazivGlavneSlike($proizvod){
        $proizvod_naziv_glavne_slike = '';

        if ($proizvod->id_brend != null) {
            $brend = Brend::dohvatiSaId($proizvod->id_brend);
            $proizvod_naziv_glavne_slike = $brend->naziv . '-';
        }

        $proizvod_naziv_glavne_slike .= $proizvod->naziv . '-' . $proizvod->sifra;

        return $proizvod_naziv_glavne_slike;
    }
}