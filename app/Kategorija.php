<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategorija extends Model
{
    protected $table = 'kategorija';

    protected $fillable = ['id_nad_kategorija', 'nivo', 'prioritet', 'naziv', 'opis', 'sakriven', 'meta_title', 'meta_desc'];

    // json_encode kodira samo vidljive atribute, sto children nije bez ove linije
    protected $visible = ['id','id_nad_kategorija', 'nivo', 'prioritet', 'naziv', 'opis', 'sakriven', 'meta_title', 'meta_desc', 'created_at', 'updated_at', 'children'];

    // dodatni atribut koji se ne nalazi u bazi
    protected $appends = ['children', 'broj_proizvoda', 'broj_brendova', 'ima_decu'];

    protected $children;

    private $broj_proizvoda;
    private $broj_brendova;
    private $ima_decu;

    public function setChildrenAttribute($children){
        $this->children = $children;
    }

    public function getChildrenAttribute(){
        return $this->children;
    }

    public function setBrojProizvodaAttribute($broj_proizvoda){
        $this->broj_proizvoda = $broj_proizvoda;
    }

    public function getBrojProizvodaAttribute(){
        return $this->broj_proizvoda;
    }

    public function setBrojBrendovaAttribute($broj_brendova){
        $this->broj_brendova = $broj_brendova;
    }

    public function getBrojBrendovaAttribute(){
        return $this->broj_brendova;
    }

    public function setImaDecu($ima_decu){
        $this->ima_decu = $ima_decu;
    }

    public function getImaDecu(){
        return $this->ima_decu;
    }

    public static function dohvatiSaId($id){
        return Kategorija::where('id', $id)->first();
    }

    public static function dohvatiSveAktivne(){
        return Kategorija::where('sakriven', 0)->get();
    }

    public static function dohvatiSveObrisane(){
        return Kategorija::where('sakriven', 1)->get();
    }

    public static function dohvatiNeposrednuDecu($id){
        return Kategorija::where('id_nad_kategorija', $id)->get();
    }

    public function napuni($naziv, $opis, $nivo, $prioritet, $id_nad_kategorija, $meta_title, $meta_desc){
        $this->naziv = $naziv;
        $this->opis = $opis;
        $this->id_nad_kategorija = $id_nad_kategorija;
        $this->nivo = $nivo;
        $this->prioritet = $prioritet;
        $this->meta_title = $meta_title;
        $this->meta_desc = $meta_desc;

        $this->save();
    }

    public function obrisi(){
        $this->id_nad_kategorija = null;
        $this->sakriven = 1;

        $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;
        $this->nivo = 0;

        $this->save();
    }

    public static function dohvatiAktivneKategorijeSortiranePoPrioritetu(){
        return Kategorija::where('sakriven', 0)->orderBy('prioritet', 'asc')->get();
    }

}
