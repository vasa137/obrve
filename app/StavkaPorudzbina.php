<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class StavkaPorudzbina extends Model
{
    protected $table = 'stavka_porudzbina';

    protected $fillable = ['kolicina', 'cena', 'cena_popust', 'sifra', 'id_proizvod', 'id_porudzbina', 'id_kupon', 'ima_opcije'];

    protected $appends = ['proizvod', 'opcije', 'kupon', 'porudzbina'];

    private $proizvod;
    private $opcije;
    private $kupon;
    private $porudzbina;

    public function setProizvodAttribute($proizvod){
        $this->proizvod = $proizvod;
    }

    public function getProizvodAttribute(){
        return $this->proizvod;
    }

    public function setOpcijeAttribute($opcije){
        $this->opcije = $opcije;
    }

    public function getOpcijeAttribute(){
        return $this->opcije;
    }

    public function setKuponAttribute($kupon){
        $this->kupon = $kupon;
    }

    public function getKuponAttribute(){
        return $this->kupon;
    }

    public function setPorudzbinaAttribute($porudzbina){
        $this->porudzbina = $porudzbina;
    }

    public function getPorudzbinaAttribute(){
        return $this->porudzbina;
    }

    public static function dohvatiSaId($id){
        return StavkaPorudzbina::where('id', $id)->first();
    }

    public static function dohvatiStavkeZaPorudzbinu($id){
        return StavkaPorudzbina::where('id_porudzbina', $id)->get();
    }

    public static function dohvatiBrojProizvodaZaPorudzbinu($id){
        return DB::select("
            select SUM(kolicina) as broj_proizvoda
            FROM stavka_porudzbina
            WHERE id_porudzbina = $id
        ")[0]->broj_proizvoda;
    }

    public static function dohvatiPrometOdDobavljaca($id){
        return DB::select("
            select IFNULL(SUM(sp.kolicina), 0)  as broj_proizvoda, IFNULL(SUM(sp.ukupno_popust), 0.00)  as promet
            FROM stavka_porudzbina sp, proizvod p, porudzbina por
            WHERE sp.id_proizvod = p.id
            AND sp.id_porudzbina = por.id
            AND por.status <> 'stornirana'
            AND p.id_dobavljac = $id
          
        ")[0];
    }

    public static function dohvatiBrojNarucenihProizvodaSaId($id){
        return DB::select("
            select IFNULL(SUM(sp.kolicina), 0)  as broj_proizvoda
            FROM stavka_porudzbina sp, porudzbina p
            WHERE sp.id_porudzbina = p.id
            AND p.status <> 'stornirana'
            AND sp.id_proizvod = $id
          
        ")[0]->broj_proizvoda;
    }

    //obavezno navodnici kod datumOd i datumDo
    public static function dohvatiIzvestajZaProizvode($datumOd, $datumDo){
        return DB::select("
           select pro.naziv as naziv, IFNULL(COUNT(distinct p.id), 0) as porudzbina, IFNULL(SUM(sp.ukupno_popust), 0) as ukupno, IFNULL(SUM(sp.kolicina), 0) as komada,
           IFNULL(SUM(IF(sp.ima_opcije = 0, 
           sp.ukupno_popust - sp.kolicina*pro.nabavna_cena, 
           sp.ukupno_popust - (SELECT sp.kolicina*po.nabavna_cena from proizvod_opcija po where po.id_opcija IN (SELECT so.id_opcija FROM stavka_opcija so WHERE so.id_stavka_porudzbina = sp.id) and po.id_proizvod = pro.id)) ), 0) as razlika
           from stavka_porudzbina sp, porudzbina p, proizvod pro
           where sp.id_porudzbina = p.id
           and sp.id_proizvod = pro.id
           and p.status <> 'stornirana'
           and DATE(p.created_at) >= '$datumOd'
           and DATE(p.created_at) < '$datumDo'
           group by pro.id, pro.naziv
        ");
    }

    public static function dohvatiIzvestajZaGradove($datumOd, $datumDo){
        return DB::select("
           select LOWER(TRIM(p.grad)) as naziv, IFNULL(COUNT(distinct p.id), 0) as porudzbina, IFNULL(SUM(sp.ukupno_popust), 0) as ukupno, IFNULL(SUM(sp.kolicina), 0) as komada,
           IFNULL(SUM(IF(sp.ima_opcije = 0, 
           sp.ukupno_popust - sp.kolicina*pro.nabavna_cena, 
           sp.ukupno_popust - (SELECT sp.kolicina*po.nabavna_cena from proizvod_opcija po where po.id_opcija IN (SELECT so.id_opcija FROM stavka_opcija so WHERE so.id_stavka_porudzbina = sp.id) and po.id_proizvod = pro.id)) ), 0) as razlika
           from stavka_porudzbina sp, porudzbina p, proizvod pro
           where sp.id_porudzbina = p.id
           and sp.id_proizvod = pro.id
           and p.status <> 'stornirana'
           and DATE(p.created_at) >= '$datumOd'
           and DATE(p.created_at) < '$datumDo'
           group by LOWER(TRIM(p.grad))
        ");
    }

    public static function dohvatiIzvestajZaBrendove($datumOd, $datumDo){
        return DB::select("
           select IF(pro.id_brend IS NULL, 'Nema brenda', (SELECT b.naziv from brend b where b.id = pro.id_brend)) as naziv, IFNULL(COUNT(distinct p.id), 0) as porudzbina, IFNULL(SUM(sp.ukupno_popust), 0) as ukupno, IFNULL(SUM(sp.kolicina), 0) as komada,
           IFNULL(SUM(IF(sp.ima_opcije = 0, 
           sp.ukupno_popust - sp.kolicina*pro.nabavna_cena, 
           sp.ukupno_popust - (SELECT sp.kolicina*po.nabavna_cena from proizvod_opcija po where po.id_opcija IN (SELECT so.id_opcija FROM stavka_opcija so WHERE so.id_stavka_porudzbina = sp.id) and po.id_proizvod = pro.id)) ), 0) as razlika
           from stavka_porudzbina sp, porudzbina p, proizvod pro
           where sp.id_porudzbina = p.id
           and sp.id_proizvod = pro.id
           and p.status <> 'stornirana'
           and DATE(p.created_at) >= '$datumOd'
           and DATE(p.created_at) < '$datumDo'
           group by pro.id_brend
        ");
    }

    public static function dohvatiIzvestajZaDobavljace($datumOd, $datumDo){
        return DB::select("
           select IF(pro.id_dobavljac IS NULL, 'Nema dobavljača', (SELECT do.naziv from dobavljac do where do.id = pro.id_dobavljac)) as naziv, IFNULL(COUNT(distinct p.id), 0) as porudzbina, IFNULL(SUM(sp.ukupno_popust), 0) as ukupno, IFNULL(SUM(sp.kolicina), 0) as komada,
           IFNULL(SUM(IF(sp.ima_opcije = 0, 
           sp.ukupno_popust - sp.kolicina*pro.nabavna_cena, 
           sp.ukupno_popust - (SELECT sp.kolicina*po.nabavna_cena from proizvod_opcija po where po.id_opcija IN (SELECT so.id_opcija FROM stavka_opcija so WHERE so.id_stavka_porudzbina = sp.id) and po.id_proizvod = pro.id)) ), 0) as razlika
           from stavka_porudzbina sp, porudzbina p, proizvod pro
           where sp.id_porudzbina = p.id
           and sp.id_proizvod = pro.id
           and p.status <> 'stornirana'
           and DATE(p.created_at) >= '$datumOd'
           and DATE(p.created_at) < '$datumDo'
           group by pro.id_dobavljac
        ");
    }

    public static function dohvatiIzvestajZaKategorije($datumOd, $datumDo){
        return DB::select("
           select kat.naziv as naziv, IFNULL(COUNT(distinct p.id), 0) as porudzbina, IFNULL(SUM(sp.ukupno_popust), 0) as ukupno, IFNULL(SUM(sp.kolicina), 0) as komada,
           IFNULL(SUM(IF(sp.ima_opcije = 0, 
           sp.ukupno_popust - sp.kolicina*pro.nabavna_cena, 
           sp.ukupno_popust - (SELECT sp.kolicina*po.nabavna_cena from proizvod_opcija po where po.id_opcija IN (SELECT so.id_opcija FROM stavka_opcija so WHERE so.id_stavka_porudzbina = sp.id) and po.id_proizvod = pro.id)) ), 0) as razlika
           from stavka_porudzbina sp, porudzbina p, proizvod pro, proizvod_kategorija pkat, kategorija kat
           where sp.id_porudzbina = p.id
           and sp.id_proizvod = pro.id
           and p.status <> 'stornirana'
           and DATE(p.created_at) >= '$datumOd'
           and DATE(p.created_at) < '$datumDo'
           and pkat.id_proizvod = pro.id
           and pkat.id_kategorija = kat.id
           group by kat.id, kat.naziv
           
           UNION
           
           select 'Nema kategorije' as naziv, IFNULL(COUNT(distinct p.id), 0) as porudzbina, IFNULL(SUM(sp.ukupno_popust), 0) as ukupno, IFNULL(SUM(sp.kolicina), 0) as komada,
           IFNULL(SUM(IF(sp.ima_opcije = 0, 
           sp.ukupno_popust - sp.kolicina*pro.nabavna_cena, 
           sp.ukupno_popust - (SELECT sp.kolicina*po.nabavna_cena from proizvod_opcija po where po.id_opcija IN (SELECT so.id_opcija FROM stavka_opcija so WHERE so.id_stavka_porudzbina = sp.id) and po.id_proizvod = pro.id)) ), 0) as razlika
           from stavka_porudzbina sp, porudzbina p, proizvod pro
           where sp.id_porudzbina = p.id
           and sp.id_proizvod = pro.id
           and p.status <> 'stornirana'
           and DATE(p.created_at) >= '$datumOd'
           and DATE(p.created_at) < '$datumDo'
           and NOT EXISTS(select pkat.id_kategorija from proizvod_kategorija pkat where pkat.id_proizvod = pro.id )
           having porudzbina > 0
        ");
    }

    public static function dohvatiIzvestajZaKupce($datumOd, $datumDo){
        return DB::select("
           select p.email as email, (SELECT CONCAT(poru.kupac, ',', poru.grad, ',', poru.telefon) from porudzbina poru where poru.email = p.email LIMIT 1 ) as dodatno , IFNULL(COUNT(distinct p.id), 0) as porudzbina, IFNULL(SUM(sp.ukupno_popust), 0) as ukupno, IFNULL(SUM(sp.kolicina), 0) as komada,
           IFNULL(SUM(IF(sp.ima_opcije = 0, 
           sp.ukupno_popust - sp.kolicina*pro.nabavna_cena, 
           sp.ukupno_popust - (SELECT sp.kolicina*po.nabavna_cena from proizvod_opcija po where po.id_opcija IN (SELECT so.id_opcija FROM stavka_opcija so WHERE so.id_stavka_porudzbina = sp.id) and po.id_proizvod = pro.id)) ), 0) as razlika
           from stavka_porudzbina sp, porudzbina p, proizvod pro
           where sp.id_porudzbina = p.id
           and sp.id_proizvod = pro.id
           and p.status <> 'stornirana'
           and DATE(p.created_at) >= '$datumOd'
           and DATE(p.created_at) < '$datumDo'
           group by p.email
        ");
    }

    public static function dohvatiIzvestajZaSveStavke($datumOd, $datumDo){
        return DB::select("
           select sp.id as id_stavka_porudzbina, sp.ukupno_popust as ukupno, sp.kolicina as komada,
           IF(sp.ima_opcije = 0, 
           sp.ukupno_popust - sp.kolicina*pro.nabavna_cena, 
           sp.ukupno_popust - (SELECT sp.kolicina*po.nabavna_cena from proizvod_opcija po where po.id_opcija IN (SELECT so.id_opcija FROM stavka_opcija so WHERE so.id_stavka_porudzbina = sp.id) and po.id_proizvod = pro.id)) as razlika
           from stavka_porudzbina sp, porudzbina p, proizvod pro
           where sp.id_porudzbina = p.id
           and sp.id_proizvod = pro.id
           and p.status <> 'stornirana'
           and DATE(p.created_at) >= '$datumOd'
           and DATE(p.created_at) < '$datumDo'
        ");
    }

    public static function dohvatiIzvestajZaKupone($datumOd, $datumDo){
        return DB::select("
           select k.id as id_kupon, SUM(sp.cena_popust*sp.kolicina*k.popust) as ukupno_ustedjeno, IFNULL(COUNT(distinct p.id), 0) as porudzbina, IFNULL(SUM(sp.ukupno_popust), 0) as ukupno, IFNULL(SUM(sp.kolicina), 0) as komada,
           IFNULL(SUM(IF(sp.ima_opcije = 0, 
           sp.ukupno_popust - sp.kolicina*pro.nabavna_cena, 
           sp.ukupno_popust - (SELECT sp.kolicina*po.nabavna_cena from proizvod_opcija po where po.id_opcija IN (SELECT so.id_opcija FROM stavka_opcija so WHERE so.id_stavka_porudzbina = sp.id) and po.id_proizvod = pro.id)) ), 0) as razlika
           from stavka_porudzbina sp, porudzbina p, proizvod pro, kupon k
           where sp.id_porudzbina = p.id
           and sp.id_proizvod = pro.id
           and p.status <> 'stornirana'
           and DATE(p.created_at) >= '$datumOd'
           and DATE(p.created_at) < '$datumDo'
           and sp.id_kupon IS NOT NULL
           and sp.id_kupon = k.id
           group by k.id
           
           UNION
           
           select 'Nema kupona' as id_kupon, 0 as ukupno_ustedjeno, IFNULL(COUNT(distinct p.id), 0) as porudzbina, IFNULL(SUM(sp.ukupno_popust), 0) as ukupno, IFNULL(SUM(sp.kolicina), 0) as komada,
           IFNULL(SUM(IF(sp.ima_opcije = 0, 
           sp.ukupno_popust - sp.kolicina*pro.nabavna_cena, 
           sp.ukupno_popust - (SELECT sp.kolicina*po.nabavna_cena from proizvod_opcija po where po.id_opcija IN (SELECT so.id_opcija FROM stavka_opcija so WHERE so.id_stavka_porudzbina = sp.id) and po.id_proizvod = pro.id)) ), 0) as razlika
           from stavka_porudzbina sp, porudzbina p, proizvod pro
           where sp.id_porudzbina = p.id
           and sp.id_proizvod = pro.id
           and p.status <> 'stornirana'
           and DATE(p.created_at) >= '$datumOd'
           and DATE(p.created_at) < '$datumDo'
           and sp.id_kupon IS NULL
           having porudzbina > 0
        ");
    }

    public static function dohvatiNajprodavanijeProizvode(){
        return DB::select("
            select sp.id_proizvod as id_proizvod, IFNULL(SUM(sp.kolicina), 0) as broj_prodatih
            from stavka_porudzbina sp, porudzbina p 
            where sp.id_porudzbina = p.id
            and p.status <> 'stornirana'
            group by sp.id_proizvod
            order by broj_prodatih desc 
            LIMIT 5
        ");
    }

    public function napuni($kolicina, $cena, $cena_popust, $sifra, $id_proizvod, $id_porudzbina, $id_kupon){
        $this->kolicina = $kolicina;
        $this->cena = $cena;
        $this->cena_popust = $cena_popust;
        $this->sifra = $sifra;
        $this->id_proizvod = $id_proizvod;
        $this->id_porudzbina = $id_porudzbina;
        $this->id_kupon = $id_kupon;

        $this->save();
    }


}
